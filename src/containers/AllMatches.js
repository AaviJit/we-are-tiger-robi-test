/* eslint-disable no-underscore-dangle*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import { Image, StyleSheet, Text, View, StatusBar } from "react-native";
import {
  Right,
  Container,
  Button,
  Header,
  Tab,
  Tabs,
  Footer,
  FooterTab,
  Drawer
} from "native-base";
import Icon from "react-native-vector-icons/Ionicons";
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import SideBar from "../components/Sidebar";
import Tab1 from "../components/Completed";
import Tab2 from "../components/OnGoing";
import Tab3 from "../components/UpComing";
import RobirAd from "../resources/icons/robiradd.png";
import HomeIcon from "../resources/icons/home.png";
import CricketIcon from "../resources/icons/cricket_selected.png";
import NewsIcon from "../resources/icons/news.png";
import TigerIcon from "../resources/icons/tiger.png";
import SupporterIcon from "../resources/icons/supporter.png";
import { DrawerChanger } from "../components/DrawerChecker";

const styles = StyleSheet.create({
  tabscontianer: {},
  insideslidetoprow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 0,
    alignItems: "center"
  },
  insideslidebottomrow: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    padding: 0,
    top: 6
  },
  textinsidebottonrow: {
    padding: 0,
    justifyContent: "center",
    color: "black",
    margin: -2,
    lineHeight: 14
  },
  footertabtext: {
    fontFamily: "SansSerifMedium"
  },
  headertext: {
    fontSize: 19,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    fontWeight: "bold",
    justifyContent: "center"
  },
  headerwearetigers: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "90%",
    marginLeft: 1,
    marginRight: 50,
    flex: 1
  },

  headerRightPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    padding: 3,
    marginTop: 3
  },
  headerLeftPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    flex: 1
  },
  ShopStyle: {
    fontSize: 15,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    paddingLeft: 7
  },
  footertabtextselected: {
    fontFamily: "SansSerifMedium",
    color: "green"
  }
});
export default class AllMatches extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#36be6b");
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
    // BackHandler.addEventListener("hardwareBackPress", () => {
    //   this.props.navigation.navigate("HomePage");
    // });
  }
  clearSetintv = () => {
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
  };
  closeDrawer = () => {
    DrawerChanger(false, this.drawer);
    //this.drawer._root.close();
  };

  openDrawer = () => {
    DrawerChanger(true, this.drawer);
    //this.drawer._root.open();
  };

  toHome = () => {
    this.props.navigation.navigate("HomePage");
  };
  toMatches = () => {

  };
  toNews = () => {
    this.props.navigation.navigate("NewsAndVideos");
  };
  toTigers = () => {
    this.props.navigation.navigate("TigersTerritory");
  };
  toRobiShop = () => {
    this.clearSetintv();
    this.props.navigation.navigate("RobiShop");
  };
  toFanZone = () => {
    this.props.navigation.navigate("FanZone");
  };
  toNewsAndVideos = () => {
    this.props.navigation.navigate("NewsAndVideos");
  };
  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={<SideBar nav={this.props.navigation} />}
        onClose={() => this.closeDrawer()}
        style={{ flex: 1 }}
      >
        <Container>
          <Header style={{ backgroundColor: "#36be6b", padding: 4 }}>
            <View style={styles.headerLeftPanel}>
              <Button transparent onPress={this.openDrawer}>
                <Icon name="ios-menu" size={25} color="#ffffff" />
              </Button>
            </View>

            <View style={styles.headerwearetigers}>
              <Text style={styles.headertext}>Matches</Text>
            </View>
            <View style={styles.headerRightPanel}>
              <Right>
                <Button transparent onPress={this.toRobiShop}>
                  <LineIcon name="handbag" size={19} color="#ffffff" />
                  <Text style={styles.ShopStyle}>Shop</Text>
                </Button>
              </Right>
            </View>
          </Header>
          <Container>
            <Tabs
              tabContainerStyle={{
                elevation: 1,
                height: 40,
                backgroundColor: "white",
                paddingLeft: "10%",
                paddingRight: "10%"
              }}
              tabBarUnderlineStyle={{ opacity: 0 }}
              initialPage={0}
            >
              <Tab
                activeTextStyle={{
                  color: "rgb(94, 94, 94)",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                textStyle={{
                  color: "#9b9b9b",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                activeTabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "#23b06a"
                }}
                tabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "white"
                }}
                heading="Completed"
              >
                <Tab1 nav={this.props.navigation} />
              </Tab>
              <Tab
                activeTextStyle={{
                  color: "rgb(94, 94, 94)",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                textStyle={{
                  color: "#9b9b9b",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                activeTabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "#23b06a"
                }}
                tabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "white"
                }}
                heading="On Going"
              >
                <Tab2 nav={this.props.navigation} />
              </Tab>
              <Tab
                activeTextStyle={{
                  color: "rgb(94, 94, 94)",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                textStyle={{
                  color: "#9b9b9b",
                  fontSize: 12,
                  fontFamily: "Roboto"
                }}
                activeTabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "#23b06a"
                }}
                tabStyle={{
                  backgroundColor: "white",
                  width: 30,
                  borderBottomWidth: 3,
                  borderBottomColor: "white"
                }}
                heading="Upcoming"
              >
                <Tab3 nav={this.props.navigation} />
              </Tab>
            </Tabs>
          </Container>
          <View>
            <Image style={{ width: "100%" }} source={RobirAd} />
          </View>

          <Footer
            style={{
              bottom: 0,
              position: "absolute",
              backgroundColor: "#36be6b",
              flex: 0.29
            }}
          >
            <FooterTab style={{ backgroundColor: "#ffffff" }}>
              <Button onPress={this.toHome}>
                <Image
                  style={{
                    paddingTop: 2,
                    height: 22,
                    width: 22
                    // backgroundColor: 'red'
                  }}
                  name="mathches"
                  source={HomeIcon}
                />
                {/* <LineIcon name="home" size={30} color={"green"}  /> */}
                <Text style={styles.footertabtext}>Home</Text>
              </Button>
              <Button onPress={this.toMatches}>
                <Image
                  style={{ paddingTop: 2, height: 22, width: 22 }}
                  name="mathches"
                  source={CricketIcon}
                />
                <Text style={styles.footertabtextselected}>Matches</Text>
              </Button>
              <Button onPress={this.toNews}>
                <Image
                  style={{ paddingTop: 3, height: 22, width: 22 }}
                  source={NewsIcon}
                />
                <Text style={styles.footertabtext}>News</Text>
              </Button>
              <Button onPress={this.toTigers}>
                <Image
                  style={{ paddingTop: 2, height: 22, width: 22 }}
                  source={TigerIcon}
                />
                <Text style={styles.footertabtext}>Tiger&apos;s</Text>
              </Button>
              <Button onPress={this.toFanZone}>
                <Image
                  style={{ height: 22, width: 22 }}
                  source={SupporterIcon}
                />
                <Text style={styles.footertabtext}>Fan Zone</Text>
              </Button>
            </FooterTab>
          </Footer>
        </Container>
      </Drawer>
    );
  }
}

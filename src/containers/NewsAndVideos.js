/* eslint-disable no-underscore-dangle*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, Image, StatusBar, BackHandler } from "react-native";
import {
  Footer,
  FooterTab,
  Container,
  Header,
  Tab,
  Tabs,
  Drawer,
  Button,
  Right
} from "native-base";
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import Icon from "react-native-vector-icons/Ionicons";
import Tab1 from "../components/NewsTab";
import Tab2 from "../components/VideosTab";
import SideBar from "../components/Sidebar";
import HomeIcon from "../resources/icons/home.png";
import CricketIcon from "../resources/icons/cricket.png";
import NewsIcon from "../resources/icons/news_selected.png";
import TigerIcon from "../resources/icons/tiger.png";
import SupporterIcon from "../resources/icons/supporter.png";
import { DrawerChanger } from "../components/DrawerChecker";

const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    flex: 1
  },
  slide2: {
    flex: 1
  },
  slide3: {
    flex: 1
  },
  text: {
    color: "#fff",
    fontSize: 30
  },
  headertext: {
    fontSize: 19,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    fontWeight: "bold",
    justifyContent: "center",
    paddingLeft: 10
  },
  shopIconText: {
    fontSize: 15,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    //fontWeight: "bold",
    paddingLeft: 7
  },
  headerwearetigers: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "60%",
    marginLeft: 1,
    marginRight: 50,
    paddingLeft: 5,
    flex: 1
  },
    headerRightPanel: {
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 1,
        padding: 3,
        marginTop: 3
  },
    headerLeftPanel: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        padding: 10,
        flex: 1
  },
  footertabtext: {
    fontFamily: "SansSerifMedium"
  },
  footertabtextselected: {
    fontFamily: "SansSerifMedium",
    color: "green"
  }
});

export default class NewsAndVideos extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#169757");
    // BackHandler.addEventListener("hardwareBackPress", () => {
    //   this.props.navigation.navigate("HomePage");
    // });
  }
  clearSetintv = () => {
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
  };
  closeDrawer = () => {
    DrawerChanger(false, this.drawer);
    //this.drawer._root.close();
  };

  openDrawer = () => {
    DrawerChanger(true, this.drawer);
    //this.drawer._root.open();
  };

  toHome = () => {
    this.props.navigation.navigate("HomePage");
  };
  toMatches = () => {
    this.props.navigation.navigate("AllMatches");
  };
  toNews = () => {

  };
  toTigers = () => {
    this.props.navigation.navigate("TigersTerritory");
  };
  toRobiShop = () => {
    this.clearSetintv();
    this.props.navigation.navigate("RobiShop");
  };
  toFanZone = () => {
    this.props.navigation.navigate("FanZone");
  };
  toNewsAndVideos = () => {
    this.props.navigation.navigate("NewsAndVideos");
  };

  render() {
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={<SideBar nav={this.props.navigation} />}
        onClose={() => this.closeDrawer()}
        style={{ flex: 1 }}
      >
        <Header style={{ backgroundColor: "#36be6b", padding: 4 }}>
          <View style={styles.headerLeftPanel}>
            <Button transparent onPress={this.openDrawer}>
              <Icon name="ios-menu" size={25} color="#ffffff" />
            </Button>
          </View>
          <View style={styles.headerwearetigers}>
            <Text style={styles.headertext}>NEWS</Text>
          </View>
          <View style={styles.headerRightPanel}>
            <Right>
              <Button transparent onPress={this.toRobiShop}>
                <LineIcon name="handbag" size={18} color="#ffffff" />
                <Text style={styles.shopIconText}>Shop</Text>
              </Button>
            </Right>
          </View>
        </Header>
        <Container>
          <Tabs
            initialPage={0}
            tabContainerStyle={{
              elevation: 0,
              height: 50,
              paddingLeft: "10%",
              backgroundColor: "white",
              paddingRight: "10%"
            }}
            tabBarUnderlineStyle={{
              opacity: 0
            }}
          >
            <Tab
              // tabStyle={{ backgroundColor: 'white' }}
              activeTextStyle={{
                color: "#4a4a4a",
                fontSize: 14,
                fontFamily: "Roboto"
              }}
              textStyle={{
                color: "#9b9b9b",
                fontSize: 14,
                fontFamily: "Roboto"
              }}
              activeTabStyle={{
                backgroundColor: "white",
                width: 30,
                borderBottomWidth: 2,
                borderBottomColor: "#23b06a"
              }}
              tabStyle={{
                backgroundColor: "white",
                width: 30,
                borderBottomWidth: 2,
                borderBottomColor: "white"
              }}
              heading="NEWS"
            >
              <Tab1 nav={this.props.navigation} />
            </Tab>
            <Tab
              activeTextStyle={{
                color: "#4a4a4a",
                fontSize: 14,
                fontFamily: "Roboto"
              }}
              textStyle={{
                color: "#9b9b9b",
                fontSize: 14,
                fontFamily: "Roboto"
              }}
              activeTabStyle={{
                backgroundColor: "white",
                width: 30,
                borderBottomWidth: 2,
                borderBottomColor: "#23b06a"
              }}
              tabStyle={{
                backgroundColor: "white",
                width: 30,
                borderBottomWidth: 2,
                borderBottomColor: "white"
              }}
              heading="VIDEOS"
            >
              <Tab2 nav={this.props.navigation} />
            </Tab>
          </Tabs>
        </Container>
        <Footer
          style={{
            bottom: 0,
            position: "absolute",
            backgroundColor: "#36be6b",
            flex: 0.29
          }}
        >
          <FooterTab style={{ backgroundColor: "#ffffff" }}>
            <Button onPress={this.toHome}>
              <Image
                style={{
                  paddingTop: 2,
                  height: 22,
                  width: 22
                  // backgroundColor: 'red'
                }}
                name="mathches"
                source={HomeIcon}
              />
              {/* <LineIcon name="home" size={30} color={"green"}  /> */}
              <Text style={styles.footertabtext}>Home</Text>
            </Button>
            <Button onPress={this.toMatches}>
              <Image
                style={{ paddingTop: 2, height: 22, width: 22 }}
                name="mathches"
                source={CricketIcon}
              />
              <Text style={styles.footertabtext}>Matches</Text>
            </Button>
            <Button onPress={this.toNews}>
              <Image
                style={{ paddingTop: 3, height: 22, width: 22 }}
                source={NewsIcon}
              />
              <Text style={styles.footertabtextselected}>News</Text>
            </Button>
            <Button onPress={this.toTigers}>
              <Image
                style={{ paddingTop: 2, height: 22, width: 22 }}
                source={TigerIcon}
              />
              <Text style={styles.footertabtext}>Tiger&apos;s</Text>
            </Button>
            <Button onPress={this.toFanZone}>
              <Image style={{ height: 22, width: 22 }} source={SupporterIcon} />
              <Text style={styles.footertabtext}>Fan Zone</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Drawer>
    );
  }
}

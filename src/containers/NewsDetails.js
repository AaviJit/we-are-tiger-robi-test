import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  StatusBar,
  ScrollView
} from "react-native";
import PropTypes from "prop-types";
import axios from "axios";
import Swiper from "react-native-swiper";
//import Icon from "react-native-vector-icons/Ionicons";
import GeneralHeader from "../components/HeaderGeneral";
import BackgroundImage from "../resources/img/ad.png";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
  Text1: {
    fontSize: 18,
    color: "black",
    textAlign: "justify"
  },
  Text2: {
    paddingTop: 2,
    fontSize: 14,
    color: "black",
    paddingLeft: 5,
    textAlign: "justify"
  },
  Text3: {
    paddingTop: 2,
    fontSize: 16,
    color: "black",
    paddingLeft: 5,
    textAlign: "justify"
  },
  Category: {
    height: 16,
    width: 60,
    justifyContent: "center",
    borderRadius: 1,
    backgroundColor: "#3CB371",
    marginLeft: 10
  },
  CategoryText: {
    fontFamily: "Roboto-Light",
    fontSize: 10,
    textAlign: "center",
    color: "#ffffff"
  },
  AdTop: {
    height: 40,
    width: "90%"
  },
  swiperbox: {
    height: 290,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 4
  }
});
export default class NewsDetails extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      summary: "",
      description: "",
      featuredImage: "",
      date: "",
      id: this.props.navigation.state.params.newsid,
      additionalImage: [],
      topBanner: ""
    };
  }
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#037d50");
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);

        this.axiosGetNewsDetails(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/id?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&id=${
            this.state.id
          }`
        );
        axios
          .get(
            `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
              x.access_token
            }&client_id=android-client&client_secret=android-secret&pageNumber=3&position=1`
          )
          .then(banner => {
            console.log(banner);
            this.setState({
              topBanner: banner.data.image.url
            });
          });
      })
      .catch(() => {
        Alert.alert(
          "Cannot connect to internal storage, make sure you have the correct storage rights."
        );
      });
  }
  sliderPicture = url => {
    this.props.navigation.navigate("SliderPage", { url });
  };
  axiosGetNewsDetails = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        console.log(response.data);
        //alert(JSON.stringify(response.data.additionalImage));
        this.setState({
          title: response.data.title,
          summary: response.data.summary,
          description: response.data.description,
          featuredImage: response.data.featruedImage.url,
          date: response.data.createdAt
        });
        response.data.additionalImage.map(x => {
          this.setState({
            additionalImage: [...this.state.additionalImage, x]
          });
          return false;
        });

        // alert(JSON.stringify(this.state.videoObjects));
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.axiosGetNewsDetails(
                `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/news/id?access_token=${
                  res.access_token
                }&client_id=android-client&client_secret=android-secret&id=${
                  this.props.navigation.state.params.newsid
                }`
              );
            })
            .catch(() => {
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.navigation.navigate("LoginPage");
            });
        } else {
          //alert(error.response.status);
          Alert.alert("Sorry, no More videos to display!");
        }
      });
  };
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <ScrollView>
          <View>
            <GeneralHeader
              nav={this.props.navigation}
              back="NewsAndVideos"
              leftIcon="ios-arrow-back"
              HeaderTittle="News Article"
              RightPanelicon1="md-share"
            />
          </View>
          {/* <View style={{flex: 1,width: '100%', height: 1, backgroundColor: '#DCDCDC'}}/> */}
          {this.state.topBanner !== "" ? (
            <View
              style={{
                width: "100%",
                backgroundColor: "white",
                // flex: 1,
                paddingBottom: 2,
                paddingTop: 2,
                // paddingLeft: 15,
                // paddingRight: 15,
                flexDirection: "row",
                justifyContent: "center"
              }}
            >
              <Image
                style={{ width: "80%", height: 60, resizeMode: "cover" }}
                source={{
                  uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
                }}
              />
            </View>
          ) : null}
          <View style={{ marginTop: 10, marginLeft: 10 }}>
            <Text style={styles.Text1}>{this.state.title}</Text>
          </View>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View>
              <TouchableOpacity>
                <View style={styles.Category}>
                  <Text style={styles.CategoryText}>CATEGORY</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{ marginLeft: 7 }}>
              <Text>{this.state.date}</Text>
            </View>
          </View>
          <View>
            <Image
              style={{ height: 200 }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${
                  this.state.featuredImage
                }`
              }}
            />
            <View
              style={{
                marginTop: 5,
                paddingTop: 7,
                paddingBottom: 7
              }}
            >
              <Text style={styles.Text2}>{this.state.summary}</Text>
            </View>
            <View
              style={{
                marginTop: 5,
                paddingTop: 7,
                paddingBottom: 7
              }}
            >
              <Text style={styles.Text3}>{this.state.description}</Text>
              {/* {this.state.additionalImage.map(imageSource => (
                <Image
                  style={{ height: 200 }}
                  source={{
                    uri: `http://206.189.159.149:8080/abc/${imageSource.url}`
                  }}
                />
              ))} */}
            </View>
          </View>
          {this.state.additionalImage.length === 0 ? null : (
            <View style={styles.swiperbox}>
              <Swiper
                dot={
                  <View
                    style={{
                      backgroundColor: "white",
                      width: 10,
                      height: 10,
                      borderRadius: 5,
                      marginLeft: 3,
                      marginRight: 3
                    }}
                  />
                }
                activeDotStyle={{
                  backgroundColor: "rgba(0,0,0,0.3)",
                  width: 10,
                  height: 10,
                  borderRadius: 5,
                  marginLeft: 3,
                  marginRight: 3
                }}
                autoplay
              >
                {this.state.additionalImage.map(imageSource => (
                  <View style={{ flex: 1 }}>
                    <TouchableOpacity onPress={() => this.sliderPicture(imageSource.url)}>
                      <Image
                        style={{ height: 200 }}
                        source={{
                          uri: `http://206.189.159.149:8080/abc/${
                            imageSource.url
                          }`
                        }}
                      />
                    </TouchableOpacity>
                  </View>
                ))}
              </Swiper>
              {this.state.topBanner !== "" ? (
                <View
                  style={{
                    width: "100%",
                    backgroundColor: "white",
                    // flex: 1,
                    paddingBottom: 2,
                    paddingTop: 2,
                    // paddingLeft: 15,
                    // paddingRight: 15,
                    flexDirection: "row",
                    justifyContent: "center"
                  }}
                >
                  <Image
                    style={{ width: "80%", height: 60, resizeMode: "cover" }}
                    source={{
                      uri: `http://206.189.159.149:8080/abc/${
                        this.state.topBanner
                      }`
                    }}
                  />
                </View>
              ) : null}
            </View>
          )}
        </ScrollView>
      </View>
    );
  }
}

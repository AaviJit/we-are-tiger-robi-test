import React, { Component } from "react";
import { WebView, StatusBar } from "react-native";

export default class RobiShop extends Component {
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#037d50");
  }
  render() {
    return (
      <WebView
        source={{ uri: "https://shop.robi.com.bd/" }}
        style={{ marginTop: 20 }}
      />
    );
  }
}

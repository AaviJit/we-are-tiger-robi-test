import React, { Component } from "react";
import { View, Image, TouchableOpacity } from "react-native";
import PropTypes from "prop-types";

import Back from "../resources/icons/close.png";

export default class SliderPage extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      url: this.props.navigation.state.params.url
    };
  }

  back = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: "black" }}>
        <TouchableOpacity onPress={this.back}>
          <Image
            style={{ width: 26, height: 26, marginTop: 10, marginLeft: 10 }}
            source={Back}
          />
        </TouchableOpacity>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            backgroundColor: "black"
          }}
        >
          <Image
            style={{ width: "80%", height: "60%" }}
            source={{
              uri: `http://206.189.159.149:8080/abc/${
                this.state.url
              }`
            }}
          />
        </View>
      </View>
    );
  }
}

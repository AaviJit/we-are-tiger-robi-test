import React from "react";
import {
  Text,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import Accordion from "react-native-collapsible/Accordion";
import Icon from "react-native-vector-icons/Ionicons";
import GeneralHeader from "../components/HeaderGeneral";
import Test from "../resources/img/test.png";

const styles = StyleSheet.create({
  Size: {
    height: 45,
    borderWidth: 0.3,
    borderColor: "#d9d9d9",
    flexDirection: "row"
  },
  SizeText: {
    fontFamily: "Roboto",
    fontSize: 11,
    fontWeight: "900",
    fontStyle: "normal",
    letterSpacing: 0.12,
    textAlign: "left",
    color: "#4a4a4a",
    marginLeft: 20,
    marginTop: 12
  },
  Quantity: {
    height: 45,
    borderWidth: 0.3,
    borderColor: "#d9d9d9",
    flexDirection: "row"
  },
  QuantityText: {
    fontFamily: "Roboto",
    fontSize: 11,
    fontWeight: "900",
    fontStyle: "normal",
    letterSpacing: 0.12,
    textAlign: "left",
    color: "#4a4a4a",
    marginLeft: 20,
    marginTop: 12
  },
  IconDown1: {
    color: "#4a4a4a",
    paddingLeft: 120,
    marginTop: 14
  },
  IconDown2: {
    color: "#4a4a4a",
    paddingLeft: 100,
    marginTop: 14
  },
  Text1: {
    fontFamily: "Roboto",
    fontSize: 10,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#4a4a4a",
    marginTop: 20,
    marginLeft: 18
  },
  Text2: {
    fontFamily: "Roboto",
    fontSize: 27,
    fontWeight: "300",
    fontStyle: "normal",
    textAlign: "left",
    color: "#171616",
    marginTop: 8,
    marginLeft: 18
  },
  Text3: {
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#171616",
    marginTop: 8,
    marginLeft: 18
  },
  Text4: {
    fontFamily: "Roboto",
    fontSize: 10,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#777777",
    marginTop: 8,
    marginBottom: 30,
    marginLeft: 18
  },
  Text5: {
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#656b6f",
    marginTop: 8,
    marginLeft: 30
  },
  Text6: {
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#656b6f",
    marginTop: 8,
    marginLeft: 30
  },
  Text7: {
    fontFamily: "Roboto",
    fontSize: 12,
    fontWeight: "bold",
    fontStyle: "normal",
    textAlign: "left",
    color: "#656b6f",
    marginTop: 8,
    marginLeft: 30,
    marginBottom: 15
  },
  Cart: {
    flex: 1,
    height: 48,
    marginTop: 5,
    marginLeft: 18,
    marginRight: 18,
    justifyContent: "center",
    alignItems: "center",
    marginBottom: 15,
    borderRadius: 22,
    borderWidth: 2,
    borderColor: "#149b58",
    backgroundColor: "#149b58"
  },
  CartText: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    fontWeight: "800",
    textAlign: "center",
    color: "#ffffff"
  }
});
const SECTIONS = [
  {
    title: "Size",
    content: "Lorem ipsum..."
  }
];

const propTypes = {
  navigation: PropTypes.object.isRequired
};

const ProductDetails = props => {
  const renderHeader1 = () => (
    <View style={styles.Size}>
      <Text style={styles.SizeText}>SIZE</Text>
      <Icon name="ios-arrow-down" size={13} style={styles.IconDown1} />
    </View>
  );
  const renderHeader2 = () => (
    <View style={styles.Quantity}>
      <Text style={styles.QuantityText}>QTY</Text>
      <Icon name="ios-arrow-down" size={13} style={styles.IconDown2} />
    </View>
  );
  const renderContent = () => (
    <View style={{}}>
      <Text>aaaaaaaaaaaaaaaaaaaaaaaaaa</Text>
    </View>
  );
  return (
    <ScrollView>
      <View style={{ flex: 1, backgroundColor: "white" }}>
        <View>
          <GeneralHeader
            nav={props.navigation}
            leftIcon="ios-arrow-back"
            back="TigersShop"
            HeaderTittle="Product Details"
            RightPanelicon1="md-cart"
          />
        </View>
        <View style={{ margin: 18 }}>
          <Image style={{ height: 300, width: "100%" }} source={Test} />
        </View>
        <View>
          <Text style={styles.Text1}>tires bien</Text>
          <Text style={styles.Text2}>Bangladesh Cricket Team Jersey</Text>
          <Text style={styles.Text3}>380$</Text>
        </View>
        <View
          style={{
            height: 0.3,
            width: "100%",
            backgroundColor: "#eeeeee",
            marginTop: 25,
            marginBottom: 25
          }}
        />
        <View>
          <Text style={styles.Text4}>
            Abcdefghijklmnopqrstuvwxyzabcdefgijklmnopqrstuvwxyz
          </Text>
          <Text style={styles.Text5}>Black color</Text>
          <Text style={styles.Text6}>Nilon Leather</Text>
          <Text style={styles.Text7}>Made in BD</Text>
        </View>
        <View style={{ flex: 1, marginBottom: 10 }}>
          <View style={{ borderColor: "green", flexDirection: "row" }}>
            <Accordion
              sections={SECTIONS}
              renderHeader={renderHeader1}
              renderContent={renderContent}
              underlayColor="white"
            />
            <Accordion
              sections={SECTIONS}
              renderHeader={renderHeader2}
              renderContent={renderContent}
              underlayColor="white"
              style={{ borderColor: "white" }}
            />
          </View>
        </View>
        <View>
          <TouchableOpacity>
            <View style={styles.Cart}>
              <Text style={styles.CartText}>Add to cart</Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </ScrollView>
  );
};

ProductDetails.propTypes = propTypes;
export default ProductDetails;

/* eslint-disable no-underscore-dangle*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  ScrollView,
  Image,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";

import axios from "axios";
import Swiper from "react-native-swiper";
import { Header, Button, Right, Drawer, Footer, FooterTab } from "native-base";
import Icon from "react-native-vector-icons/Ionicons";
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import SideBar from "../components/Sidebar";
import Cardrender from "../components/Card";
import SeriesIcon from "../resources/icons/weIcon.png";
import HomeIcon from "../resources/icons/home_button.png";
import CricketIcon from "../resources/icons/cricket.png";
import NewsIcon from "../resources/icons/news.png";
import TigerIcon from "../resources/icons/tiger.png";
import SupporterIcon from "../resources/icons/supporter.png";
import Logo from "../resources/icons/bitmap.png";
//import BdFlag from "../resources/icons/bangladesh.jpg";
import { getuserdetails } from "../components/authorizationandrefreshtoken";
import { DrawerChanger } from "../components/DrawerChecker";

const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    flex: 1
  },
  slide2: {
    flex: 1
  },
  slide3: {
    flex: 1
  },
  text: {
    color: "#fff",
    fontSize: 30
  },
  headertext: {
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    padding: 8,
    paddingLeft: 9,
    fontWeight: "bold"
  },
  headerwearetigers: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 2,
    paddingLeft: 8,
    flex: 1
  },
  ShopStyle: {
    fontSize: 15,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    paddingLeft: 7
  },
  headerRightPanel: {
    flexDirection: "row",
    justifyContent: "flex-start",
    flex: 1
  },
  headerLeftPanel: {
    flexDirection: "row",
    width: "20%",
    justifyContent: "space-between",
    padding: 6,
    flex: 1
    // fontWeight: 'bold'
  },
  cardbox: {
    backgroundColor: "#cecece"
  },
  swiperboxLive: {
    height: 190,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 4
    },
  swiperbox: {
    //height: "37.5%",
    height: 270,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 4
  },
  swiperbox2: {
    height: 250,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 4
  },
  swiperbody: { flex: 1 },
  swiperimagecontainer: { flex: 1 },
  swiperslidecontainer: { flex: 1 },
  swiperdottraydotcontainer: {},
  swiperdottray: {},
  swiperdot: {},
  slide: { flex: 1 },
  slidestrech: { flex: 1 },
  labelview: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    padding: 5,
    backgroundColor: "#ffffff"
  },

  seeAll: {
    color: "#36be6b",
    flexDirection: "column",
    paddingRight: "5%",
    fontFamily: "Roboto-Medium"
  },
  lableviewtittle: {
    // fontSize :16,
    fontFamily: "Roboto-Medium",
    fontWeight: "bold",
    //  Color="#4a4a4a",
    fontSize: 15,
    paddingLeft: 12,
    paddingTop: 4
  },
  flagright: {
    width: "100%",
    //paddingTop: 27,
    paddingLeft: "6%",
    //  paddingBottom:5,
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row-reverse",
    margin: 0
    //backgroundColor: "grey"
  },
  logobetweencountries: {
    // flexDirection:'column',
    //position: "relative"
  },
  flagleft: {
    width: "100%",
    //paddingTop: 27,
    paddingLeft: "6%",
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row",
    margin: 0
    //backgroundColor: "blue"
  },
  leftcountryname: {
    fontSize: 26,
    paddingLeft: "5%",
    // paddingTop: 10,
    color: "#2b2b2b",
    alignItems: "flex-end",
    fontFamily: "BigNoodleTitling",
    bottom: 3
    //backgroundColor: "blue"
  },
  rightcountryname: {
    fontSize: 26,
    paddingRight: "5%",
    color: "#2b2b2b",
    alignItems: "flex-start",
    fontFamily: "BigNoodleTitling",
    bottom: 3
    //backgroundColor: "purple"
  },
  scoresleft: {
    fontSize: 17,
    textAlign: "right",
    paddingTop: 8,
    //paddingLeft: 10,
    fontFamily: "Montserrat",
    color: "#2b2b2b"
    //backgroundColor: "yellow"
  },
  scoresright: {
    fontSize: 17,
    textAlign: "left",
    paddingTop: 8,
    paddingRight: "1%",
    fontFamily: "Montserrat",
    color: "#2b2b2b"
    //backgroundColor: "orange"
  },
  countryelement: {
    width: "38%",
    flexDirection: "column"
  },
  slideBackgroundimage: {
    flex: 1
  },
  insideslidetoprow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },
  insideslidebottomrow: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    paddingLeft: "1%",
    paddingRight: "1%",
    //justifyContent: "center"
  },
  textinsidebottonrow: {
    paddingLeft: "1%",
    paddingRight: "1%",
    //paddingTop: "0.5%",
    paddingBottom: 5,
    fontFamily: "Roboto",
    fontSize: 17,
    color: "rgb(55,55,55)"
  },
  textinsidebottonrow2: {
    paddingLeft: "1%",
    paddingRight: "1%",
    //paddingTop: "0.5%",
    paddingBottom: 5,
    fontFamily: "Roboto",
    fontSize: 17,
    color: "rgb(55,55,55)"
  },
  footertabtext: {
    fontFamily: "SansSerifMedium"
  },
  footertabtextselected: {
    fontFamily: "SansSerifMedium",
    color: "green"
  }
});

export default class HomePage extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      match: null,
      upcoming: null,
      live: null

    };
  }
  // componentWillMount() {
  //   StatusBar.setHidden(false);
  //   //StatusBar.setBackgroundColor("#36be6b");
  // }
  componentWillMount() {
    //alert(this.state.isLoggedIn);
    StatusBar.setHidden(false);

    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        this.axiosGetRecentMatchDetails(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/recent/shortCard?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret`
        );
        axios
          .get(
            `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/upcoming?access_token=${
              x.access_token
            }&client_id=android-client&client_secret=android-secret`
          )
          .then(upcoming => {
            // alert(JSON.stringify(upcoming));
            this.setState({
              upcoming
            });
          });

        axios
            .get(
            `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/live?access_token=${
                x.access_token
                }&client_id=android-client&client_secret=android-secret`
        ).then(live => {
          this.setState({
              live
          });
        });

      })
      .catch(() => {
        this.props.navigation.navigate("LoginPage");
      });
  }
  closeDrawer = () => {
    DrawerChanger(false, this.drawer);
    //this.drawer._root.close();
  };

  openDrawer = () => {
    DrawerChanger(true, this.drawer);
    //this.drawer._root.open();
  };

  clearSetintv = () => {
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
  };
  tomatchdetails = key => {
    this.clearSetintv();
    this.props.navigation.navigate("MatchDetails", { key });
  };
  toHome = () => {
    this.clearSetintv();
  };
  toMatches = () => {
    this.clearSetintv();
    this.props.navigation.navigate("AllMatches");
  };
  toNews = () => {
    this.clearSetintv();
    this.props.navigation.navigate("NewsAndVideos");
  };
  toTigers = () => {
    this.clearSetintv();
    this.props.navigation.navigate("TigersTerritory");
  };
  toRobiShop = () => {
    this.clearSetintv();
    this.props.navigation.navigate("RobiShop");
  };
  toFanZone = () => {
    this.clearSetintv();
    this.props.navigation.navigate("FanZone");
  };
  toNewsAndVideos = () => {
    this.clearSetintv();
    this.props.navigation.navigate("NewsAndVideos");
  };
  tomatchdetails = key => {
    this.props.navigation.navigate("MatchDetails", { key });
  };
  upComing = data => {
    this.props.navigation.navigate("UpcomingInfo", { data });
  };
  axiosGetRecentMatchDetails = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        this.setState(
          {
            match: response.data
          },

          () => {
            this.setState({
              loading: false
            });
          }
        );
        // alert(JSON.stringify(this.state.match))
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.setState({}, () => {
                this.axiosGetRecentMatchDetails(
                  `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/recent/shortCard?access_token=${
                    res.access_token
                  }&client_id=android-client&client_secret=android-secret`
                );
              });
            })
            .catch(() => {
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.navigation.navigate("LoginPage");
            });
        }
      });
  };
  render() {
    const { match, upcoming, live} = this.state;
    if (match === null || upcoming === null || live === null) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color="#rgb(54,190,107)" />
        </View>
      );
    }
    return (
      <Drawer
        ref={ref => {
          this.drawer = ref;
        }}
        content={<SideBar nav={this.props.navigation} />}
        onClose={() => this.closeDrawer()}
        style={{ flex: 1 }}
      >
        <Header style={{ backgroundColor: "#36be6b", padding: 4 }}>
          <View style={styles.headerLeftPanel}>
            <Button transparent onPress={this.openDrawer}>
              <Icon name="ios-menu" size={25} color="#ffffff" />
            </Button>
            <View style={styles.headerwearetigers}>
              <Image style={{ height: 25, width: 25, top: 4 }} source={Logo} />

              <Text style={styles.headertext}>WE ARE TIGERS</Text>
            </View>
          </View>

          <View style={styles.headerRightPanel}>
            <Right>
              <Button transparent onPress={this.toRobiShop}>
                <LineIcon name="handbag" size={19} color="#ffffff" />
                <Text style={styles.ShopStyle}>Shop</Text>
              </Button>
            </Right>
          </View>
        </Header>
        <View style={{ flex: 1 }}>
          <ScrollView>
            <View>








              {match.data.length >= 0 && (
                <View>



                    {/*this one for live match testing */}

                    {this.state.live.data.length > 0?(
                        <View>
                    <View style={styles.labelview}>
                        <Text style={styles.lableviewtittle}>Live Match</Text>
                        <TouchableOpacity onPress={this.toMatches}>
                            <Text style={styles.seeAll}>SEE ALL</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.swiperboxLive}>
                        <Swiper
                            dot={
                                <View
                                    style={{
                                        backgroundColor: "rgba(0,0,0,.2)",
                                        width: 10,
                                        height: 10,
                                        borderRadius: 5,
                                        marginLeft: 3,
                                        marginRight: 3,
                                        marginTop: "2%"
                                    }}
                                />
                            }
                            activeDotStyle={{
                                backgroundColor: "#6F6F6F",
                                width: 10,
                                height: 10,
                                borderRadius: 5,
                                marginLeft: 3,
                                marginRight: 3,
                                marginTop: "2.5%"
                            }}
                            showsButtons={false}
                        >
                            <View style={styles.slide1}>
                                <TouchableOpacity
                                    style={{ flex: 1 }}
                                    onPress={() => {
                                        this.tomatchdetails(live.data[0].key);
                                    }}
                                >
                                    <View style={styles.insideslidetoprow}>
                                        <View style={styles.countryelement}>
                                            <View style={styles.flagleft}>
                                                <Image
                                                    style={{ height: 29, width: 41 }}
                                                    source={{ uri: live.data[0].team_a_url }}
                                                />
                                                <Text style={styles.leftcountryname}>
                                                    {live.data[0].team_a}
                                                </Text>
                                            </View>
                                            <Text style={styles.scoresleft}>
                                                {live.data[0].teamAScore}
                                            </Text>
                                        </View>
                                        <View>
                                            <Image
                                                style={{ height: 40, width: 46 }}
                                                source={SeriesIcon}
                                            />
                                        </View>
                                        <View style={styles.countryelement}>
                                            <View style={styles.flagright}>
                                                <Image
                                                    style={{ height: 29, width: 41 }}
                                                    source={{ uri: live.data[0].team_b_url }}
                                                />
                                                <Text style={styles.rightcountryname}>
                                                    {live.data[0].team_b}
                                                </Text>
                                            </View>
                                            <Text style={styles.scoresright}>
                                                {live.data[0].teamBScore}
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={styles.insideslidebottomrow}>
                                        <Text style={styles.textinsidebottonrow}>
                                            {live.data[0].name}
                                        </Text>
                                        <Text
                                            style={{
                                                textAlign: "center",
                                                fontSize: 17,
                                                fontFamily: "Roboto",
                                                fontWeight: "400",
                                                color: "#000000"
                                            }}
                                        >
                                            {live.data[0].related_name}
                                        </Text>
                                        {/* fontSize: 19, fontcolor: 'Black' */}
                                        <Text style={styles.textinsidebottonrow2}>
                                            {live.data[0].venue}
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>


                        </Swiper>
                    </View>
                            {this.state.live.data.length > 1 ?
                                <View style={styles.swiperboxLive}>
                                    <Swiper
                                        dot={
                                            <View
                                                style={{
                                                    backgroundColor: "rgba(0,0,0,.2)",
                                                    width: 10,
                                                    height: 10,
                                                    borderRadius: 5,
                                                    marginLeft: 3,
                                                    marginRight: 3,
                                                    marginTop: "2%"
                                                }}
                                            />
                                        }
                                        activeDotStyle={{
                                            backgroundColor: "#6F6F6F",
                                            width: 10,
                                            height: 10,
                                            borderRadius: 5,
                                            marginLeft: 3,
                                            marginRight: 3,
                                            marginTop: "2.5%"
                                        }}
                                        showsButtons={false}
                                    >
                                        <View style={styles.slide1}>
                                            <TouchableOpacity
                                                style={{ flex: 1 }}
                                                onPress={() => {
                                                    this.tomatchdetails(live.data[1].key);
                                                }}
                                            >
                                                <View style={styles.insideslidetoprow}>
                                                    <View style={styles.countryelement}>
                                                        <View style={styles.flagleft}>
                                                            <Image
                                                                style={{ height: 29, width: 41 }}
                                                                source={{ uri: live.data[1].team_a_url }}
                                                            />
                                                            <Text style={styles.leftcountryname}>
                                                                {live.data[1].team_a}
                                                            </Text>
                                                        </View>
                                                        <Text style={styles.scoresleft}>
                                                            {live.data[1].teamAScore}
                                                        </Text>
                                                    </View>
                                                    <View>
                                                        <Image
                                                            style={{ height: 40, width: 46 }}
                                                            source={SeriesIcon}
                                                        />
                                                    </View>
                                                    <View style={styles.countryelement}>
                                                        <View style={styles.flagright}>
                                                            <Image
                                                                style={{ height: 29, width: 41 }}
                                                                source={{ uri: live.data[1].team_b_url }}
                                                            />
                                                            <Text style={styles.rightcountryname}>
                                                                {live.data[1].team_b}
                                                            </Text>
                                                        </View>
                                                        <Text style={styles.scoresright}>
                                                            {live.data[1].teamBScore}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={styles.insideslidebottomrow}>
                                                    <Text style={styles.textinsidebottonrow}>
                                                        {live.data[1].name}
                                                    </Text>
                                                    <Text
                                                        style={{
                                                            textAlign: "center",
                                                            fontSize: 17,
                                                            fontFamily: "Roboto",
                                                            fontWeight: "400",
                                                            color: "#000000"
                                                        }}
                                                    >
                                                        {live.data[1].related_name}
                                                    </Text>
                                                    {/* fontSize: 19, fontcolor: 'Black' */}
                                                    <Text style={styles.textinsidebottonrow2}>
                                                        {live.data[1].venue}
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </Swiper>
                                </View>
                            :null}
                        </View>
                        ):null
                    }






                  {/*this one for completed match */}
                  <View style={styles.labelview}>
                    <Text style={styles.lableviewtittle}>Completed match</Text>
                    <TouchableOpacity onPress={this.toMatches}>
                      <Text style={styles.seeAll}>SEE ALL</Text>
                    </TouchableOpacity>
                  </View>

                  <View style={styles.swiperbox}>
                    <Swiper
                      dot={
                        <View
                          style={{
                            backgroundColor: "rgba(0,0,0,.2)",
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginLeft: 3,
                            marginRight: 3,
                            marginTop: "2%"
                          }}
                        />
                      }
                      activeDotStyle={{
                        backgroundColor: "#6F6F6F",
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginLeft: 3,
                        marginRight: 3,
                        marginTop: "2.5%"
                      }}
                      showsButtons={false}
                    >
                      <View style={styles.slide1}>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => {
                            this.tomatchdetails(match.data[0].key);
                          }}
                        >
                          <View style={styles.insideslidetoprow}>
                            <View style={styles.countryelement}>
                              <View style={styles.flagleft}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[0].team_a_url }}
                                />
                                <Text style={styles.leftcountryname}>
                                  {match.data[0].team_a}
                                </Text>
                              </View>
                              <Text style={styles.scoresleft}>
                                {match.data[0].teamAScore}
                              </Text>
                            </View>
                            <View>
                              <Image
                                style={{ height: 40, width: 46 }}
                                source={SeriesIcon}
                              />
                            </View>
                            <View style={styles.countryelement}>
                              <View style={styles.flagright}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[0].team_b_url }}
                                />
                                <Text style={styles.rightcountryname}>
                                  {match.data[0].team_b}
                                </Text>
                              </View>
                              <Text style={styles.scoresright}>
                                {match.data[0].teamBScore}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.insideslidebottomrow}>
                            <Text style={styles.textinsidebottonrow}>
                              {match.data[0].name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                fontWeight: "400",
                                color: "#000000"
                              }}
                            >
                              {match.data[0].msgs_result}
                            </Text>
                            {/* fontSize: 19, fontcolor: 'Black' */}
                            <Text style={styles.textinsidebottonrow2}>
                              {match.data[0].venue}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.slide2}>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => {
                            this.tomatchdetails(match.data[1].key);
                          }}
                        >
                          <View style={styles.insideslidetoprow}>
                            <View style={styles.countryelement}>
                              <View style={styles.flagleft}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[1].team_a_url }}
                                />
                                <Text style={styles.leftcountryname}>
                                  {match.data[1].team_a}
                                </Text>
                              </View>

                              <Text style={styles.scoresleft}>
                                {match.data[1].teamAScore}
                              </Text>
                            </View>
                            <View style={styles.logobetweencountries}>
                              <Image
                                style={{ height: 40, width: 46 }}
                                source={SeriesIcon}
                              />
                            </View>
                            <View style={styles.countryelement}>
                              <View style={styles.flagright}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[1].team_b_url }}
                                />
                                <Text style={styles.rightcountryname}>
                                  {match.data[1].team_b}
                                </Text>
                              </View>
                              <Text style={styles.scoresright}>
                                {match.data[1].teamBScore}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.insideslidebottomrow}>
                            <Text style={styles.textinsidebottonrow}>
                              {match.data[1].name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                fontWeight: "400",
                                color: "#000000"
                              }}
                            >
                              {match.data[1].msgs_result}
                            </Text>
                            {/* fontSize: 19, fontcolor: 'Black' */}
                            <Text style={styles.textinsidebottonrow2}>
                              {match.data[1].venue}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.slide3}>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => {
                            this.tomatchdetails(match.data[2].key);
                          }}
                        >
                          <View style={styles.insideslidetoprow}>
                            <View style={styles.countryelement}>
                              <View style={styles.flagleft}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[2].team_a_url }}
                                />
                                <Text style={styles.leftcountryname}>
                                  {match.data[2].team_a}
                                </Text>
                              </View>
                              <Text style={styles.scoresleft}>
                                {match.data[2].teamAScore}
                              </Text>
                            </View>
                            <View style={styles.logobetweencountries}>
                              <Image
                                style={{ height: 40, width: 46 }}
                                source={SeriesIcon}
                              />
                            </View>
                            <View style={styles.countryelement}>
                              <View style={styles.flagright}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: match.data[2].team_b_url }}
                                />
                                <Text style={styles.rightcountryname}>
                                  {match.data[2].team_b}
                                </Text>
                              </View>
                              <Text style={styles.scoresright}>
                                {match.data[2].teamBScore}
                              </Text>
                            </View>
                          </View>
                          <View style={styles.insideslidebottomrow}>
                            <Text style={styles.textinsidebottonrow}>
                              {match.data[2].name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                fontWeight: "400",
                                color: "#000000"
                              }}
                            >
                              {match.data[2].msgs_result}
                            </Text>
                            {/* fontSize: 19, fontcolor: 'Black' */}
                            <Text style={styles.textinsidebottonrow2}>
                              {match.data[2].venue}
                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </Swiper>
                  </View>


                    {/*this one for upcomming match*/}
                  <View style={styles.labelview}>
                    <Text style={styles.lableviewtittle}>Upcoming match</Text>
                    <TouchableOpacity onPress={this.toMatches}>
                      <Text style={styles.seeAll}>SEE ALL</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.swiperbox2}>
                    <Swiper
                      dot={
                        <View
                          style={{
                            backgroundColor: "rgba(0,0,0,.2)",
                            width: 10,
                            height: 10,
                            borderRadius: 5,
                            marginLeft: 3,
                            marginRight: 3,
                            marginTop: "2%"
                          }}
                        />
                      }
                      activeDotStyle={{
                        backgroundColor: "#6F6F6F",
                        width: 10,
                        height: 10,
                        borderRadius: 5,
                        marginLeft: 3,
                        marginRight: 3,
                        marginTop: "2.5%"
                      }}
                      showsButtons={false}
                    >
                      <View style={styles.slide1}>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => this.upComing(upcoming.data[0])}
                        >
                          <View style={styles.insideslidetoprow}>
                            <View style={styles.countryelement}>
                              <View style={styles.flagleft}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: upcoming.data[0].team_a_url }}
                                />
                                <Text style={styles.leftcountryname}>
                                  {upcoming.data[0].team_a}
                                </Text>
                              </View>
                            </View>
                            <View style={{ marginTop: 30 }}>
                              <Image
                                style={{ height: 40, width: 46 }}
                                source={SeriesIcon}
                              />
                            </View>
                            <View style={styles.countryelement}>
                              <View style={styles.flagright}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: upcoming.data[0].team_b_url }}
                                />
                                <Text style={styles.rightcountryname}>
                                  {upcoming.data[0].team_b}
                                </Text>
                              </View>
                            </View>
                          </View>
                          <View style={styles.insideslidebottomrow}>
                            <Text style={styles.textinsidebottonrow}>
                              {upcoming.data[0].name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                //fontWeight: "bold",
                                color: "#000000"
                              }}
                            >
                                {upcoming.data[0].related_name}
                            </Text>

                              <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                //fontWeight: "400",
                                //color: "#000000",
                                color: "rgb(55,55,55)"
                              }}
                            >
                              {upcoming.data[0].start_date.str}, {upcoming.data[0].venue}
                             {/* {upcoming.data[0].date}, {upcoming.data[0].time}, {upcoming.data[0].venue}*/}

                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                      <View style={styles.slide2}>
                        <TouchableOpacity
                          style={{ flex: 1 }}
                          onPress={() => this.upComing(upcoming.data[1])}
                        >
                          <View style={styles.insideslidetoprow}>
                            <View style={styles.countryelement}>
                              <View style={styles.flagleft}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: upcoming.data[1].team_a_url }}
                                />
                                <Text style={styles.leftcountryname}>
                                  {upcoming.data[1].team_a}
                                </Text>
                              </View>
                            </View>
                            <View style={{ marginTop: 30 }}>
                              <Image
                                style={{ height: 40, width: 46 }}
                                source={SeriesIcon}
                              />
                            </View>
                            <View style={styles.countryelement}>
                              <View style={styles.flagright}>
                                <Image
                                  style={{ height: 29, width: 41 }}
                                  source={{ uri: upcoming.data[1].team_b_url }}
                                />
                                <Text style={styles.rightcountryname}>
                                  {upcoming.data[1].team_b}
                                </Text>
                              </View>
                            </View>
                          </View>
                          <View style={styles.insideslidebottomrow}>
                            <Text style={styles.textinsidebottonrow}>
                              {upcoming.data[1].name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                //fontWeight: "400",
                                color: "#000000"
                              }}
                            >
                                {upcoming.data[1].related_name}
                            </Text>
                            <Text
                              style={{
                                textAlign: "center",
                                fontSize: 17,
                                fontFamily: "Roboto",
                                //fontWeight: "400",
                                //color: "#000000",
                                color: "rgb(55,55,55)"
                              }}
                            >
                              {upcoming.data[1].start_date.str},  {upcoming.data[1].venue}
                                {/*{upcoming.data[1].date}, {upcoming.data[1].time}, {upcoming.data[1].venue}*/}

                            </Text>
                          </View>
                        </TouchableOpacity>
                      </View>
                    </Swiper>
                  </View>
                </View>
              )}

              <View style={styles.labelview}>
                <Text style={styles.lableviewtittle}>Top Updates</Text>
                <TouchableOpacity onPress={this.toNews}>
                  <Text style={styles.seeAll}>SEE ALL</Text>
                </TouchableOpacity>
              </View>
              <View style={styles.cardbox}>
                <Cardrender
                  nav={this.props.navigation}
                  banner="{this.state.bannerImage}"
                />
              </View>
            </View>
          </ScrollView>

          <Footer
            style={{
              bottom: 0,
              position: "absolute",
              backgroundColor: "#36be6b",
              flex: 0.29
            }}
          >
            <FooterTab style={{ backgroundColor: "#ffffff" }}>
              <Button onPress={this.toHome}>
                <Image
                  style={{
                    paddingTop: 2,
                    height: 22,
                    width: 22
                    // backgroundColor: 'red'
                  }}
                  name="mathches"
                  source={HomeIcon}
                />
                {/* <LineIcon name="home" size={30} color={"green"}  /> */}
                <Text style={styles.footertabtextselected}>Home</Text>
              </Button>
              <Button onPress={this.toMatches}>
                <Image
                  style={{ paddingTop: 2, height: 22, width: 22 }}
                  name="mathches"
                  source={CricketIcon}
                />
                <Text style={styles.footertabtext}>Matches</Text>
              </Button>
              <Button onPress={this.toNews}>
                <Image
                  style={{ paddingTop: 3, height: 22, width: 22 }}
                  source={NewsIcon}
                />
                <Text style={styles.footertabtext}>News</Text>
              </Button>
              <Button onPress={this.toTigers}>
                <Image
                  style={{ paddingTop: 2, height: 22, width: 22 }}
                  source={TigerIcon}
                />
                <Text style={styles.footertabtext}>Tiger&apos;s</Text>
              </Button>
              <Button onPress={this.toFanZone}>
                <Image
                  style={{ height: 22, width: 22 }}
                  source={SupporterIcon}
                />
                <Text style={styles.footertabtext}>Fan Zone</Text>
              </Button>
            </FooterTab>
          </Footer>
        </View>
      </Drawer>
    );
  }
}

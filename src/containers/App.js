import React, { Component } from "react";
import PropTypes from "prop-types";
import { BackHandler, AsyncStorage, NetInfo, Alert } from "react-native";
import {
  NavigationActions,
  addNavigationHelpers
} from "react-navigation/src/react-navigation";
import { connect } from "react-redux";
import AppNavigator from "../navigator";
import { DrawerChecker, DrawerChanger } from "../components/DrawerChecker";

const newLocal = "My Alert Msg";
class AppWithNavigationState extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    nav: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      delay: true
    };
  }

  componentDidMount() {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === "none") {
        Alert.alert("No internet connection", "Please check your internet.");
      } else {
        AsyncStorage.getItem("user").then(() => {
          this.setState({
            delay: false
          });
          this.props.dispatch(
            NavigationActions.navigate({ routeName: "HomePage" })
          );
        });
      }
    });
    NetInfo.addEventListener("connectionChange", this.handleConnectivityChange);
    this.backHandler = BackHandler.addEventListener("hardwareBackPress", () => {
      const drawerOpen = DrawerChecker("drawer");
      if (drawerOpen) {
        DrawerChanger(false);
        return true;
      }
      if (
        this.props.nav.routes[this.props.nav.routes.length - 1].routeName ===
          "LoginPage" ||
        this.props.nav.routes[this.props.nav.routes.length - 1].routeName ===
          "HomePage" ||
        this.props.nav.routes[this.props.nav.routes.length - 1].routeName ===
          "LogOut"
      ) {
        // Works on both iOS and Android
        Alert.alert(
          "Exit We are Tigers",
          "Do you want to exit?",
          [
            {
              text: "Ok",
              onPress: () => BackHandler.exitApp()
            },
            {
              text: "Cancel",
              onPress: () => console.log("Cancel Pressed"),
              style: "cancel"
            }
          ],
          { cancelable: false }
        );
      } else {
        this.props.dispatch(NavigationActions.back());
      }
      return true;
    });

    AsyncStorage.getItem("user").then(() => {
      this.setState({
        delay: false
      });
      this.props.dispatch(
        NavigationActions.navigate({ routeName: "HomePage" })
      );
    });
  }

  componentWillUnmount() {
    this.backHandler.remove();
  }

  handleConnectivityChange = () => {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === "none") {
        Alert.alert("No internet connection", "Please check your internet.");
      } else {
        AsyncStorage.getItem("user").then(() => {
          if (
            this.props.nav.routes[this.props.nav.routes.length - 1]
              .routeName === "MatchDetails" ||
            this.props.nav.routes[this.props.nav.routes.length - 1]
              .routeName === "NewsDetails"
          ) {
          } else {
            this.props.dispatch(
              NavigationActions.navigate({
                routeName: this.props.nav.routes[
                  this.props.nav.routes.length - 1
                ].routeName
              })
            );
          }
        });
      }
    });
  };
  render() {
    const { dispatch, nav } = this.props;
    if (this.state.delay) {
      return null;
    }
    return (
      <AppNavigator
        navigation={addNavigationHelpers({ dispatch, state: nav })}
      />
    );
  }
}
// wraps dispatch to create nicer functions to call within our component
const mapDispatchToProps = dispatch => ({
  dispatch,
  startup: () => dispatch()
});

const mapStateToProps = state => ({
  nav: state.nav
});

export default connect(mapStateToProps, mapDispatchToProps)(
  AppWithNavigationState
);

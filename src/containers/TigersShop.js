/* eslint-disable no-console*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  Image,
  ScrollView,
  StyleSheet,
  View,
  TouchableOpacity,
  StatusBar
} from "react-native";

import Swiper from "react-native-swiper";

// import Carousel from 'react-native-snap-carousel';
// import SwipeableViews from 'react-swipeable-views-native';
import CarouselPager from "react-native-carousel-pager";
import Icon from "react-native-vector-icons/Ionicons";
import Bitmap1 from "../resources/img/bitmap1.jpg";
import Bitmap from "../resources/img/bitmap.jpg";
import Bg from "../resources/img/bg.jpg";
import Test from "../resources/icons/bitmap.png";
import Rect from "../resources/img/rectangle_11.png";
import Rect1 from "../resources/img/rectangle_11_copy_2.png";

import GeneralHeader from "../components/HeaderGeneral";

const styles = StyleSheet.create({
  Text1: {
    fontSize: 14,
    fontFamily: "Roboto",
    marginLeft: 8,
    color: "#171616",
    marginTop: 5
  },
  Text2: {
    fontSize: 10,
    fontFamily: "Roboto",
    marginLeft: 8,
    color: "#787878",
    marginTop: 1
  },
  backbutton: {
    marginTop: 16
  },
  swiperbox: {
    flex: 1,
    height: 170,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 5,
    marginTop: 4
  },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB"
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5"
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9"
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold"
  },
  CarouselHeading1: {
    width: "50%",
    fontSize: 15,
    color: "#787878",
    paddingLeft: 13
  },
  ViewAll: {
    fontFamily: "Roboto",
    color: "#36be6b",
    fontSize: 12,
    fontWeight: "500",
    fontStyle: "normal",
    letterSpacing: 0,
    paddingRight: 13
  },
  PaymentText: {
    marginTop: 25,
    paddingLeft: 5,
    color: "black",
    alignItems: "center",
    justifyContent: "center"
  },
  CarText1: {
    fontFamily: "Roboto",
    fontSize: 12,
    textAlign: "center",
    alignItems: "center",
    color: "#787878"
  },
  CarText2: {
    fontFamily: "Roboto",
    fontSize: 12,
    textAlign: "center",
    alignItems: "center",
    color: "#36be6b"
  }
});
export default class TigersShop extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#037d50");
  }
  toPlayersWallpaper = () => {
    this.props.navigation.navigate("PlayersWallpaper");
  };
  toProducts = () => {
    this.props.navigation.navigate("Products");
  };
  toShippingInformation = () => {
    this.props.navigation.navigate("ShippingInformation");
  };
  toProductDetails = () => {
    this.props.navigation.navigate("ProductDetails");
  };
  render() {
    return (
      <ScrollView>
        <View style={{ flex: 1, backgroundColor: "white" }}>
          <GeneralHeader
            nav={this.props.navigation}
            leftIcon="ios-arrow-back"
            back="HomePage"
            HeaderTittle="Tigers Shop"
            RightPanelicon1="md-cart"
          />
          <View style={styles.swiperbox}>
            <Swiper
              dot={
                <View
                  style={{
                    backgroundColor: "#36be6b",
                    width: 4.5,
                    height: 4.5,
                    borderRadius: 4,
                    marginLeft: -135,
                    marginRight: 140,
                    paddingBottom: -200
                  }}
                />
              }
              activeDot={
                <View
                  style={{
                    backgroundColor: "#ffffff",
                    width: 4.5,
                    height: 4.5,
                    borderRadius: 4,
                    marginLeft: -135,
                    marginRight: 140,
                    paddingBottom: -200
                  }}
                />
              }
              activeDotColor="#ffffff"
              showsButtons={false}
            >
              <View style={styles.slide1}>
                {/* <Image style={styles.img} source ={require('../resources/img/bg.png')}/>
                  <View>
                    <TouchableOpacity>
                      <View style={styles.login}>
                        <Text style={styles.loginText} >Log in</Text>
                      </View>
                    </TouchableOpacity>
                  </View> */}
                <Image
                  style={{ height: "100%", width: "100%" }}
                  // source={{uri: this.state.featuredImage}}
                  source={Bitmap1}
                />
              </View>
              <View style={styles.slide2}>
                <Image
                  style={{ height: "100%", width: "100%" }}
                  source={Bitmap}
                />
              </View>
              <View style={styles.slide3}>
                <Image style={{ height: "100%", width: "100%" }} source={Bg} />
              </View>
            </Swiper>
            <View
              style={{ width: "100%", height: 3, backgroundColor: "#DCDCDC" }}
            />
          </View>
          <View style={{ height: "80%", flex: 1, marginBottom: 10 }}>
            <View
              style={{ flexDirection: "row", marginTop: 10, marginBottom: 10 }}
            >
              <Image
                style={{ marginLeft: 15, height: 50, width: 60 }}
                source={Test}
              />
              <View>
                <Text style={styles.Text1}>We Are Tiger&apos;s Community</Text>
                <Text style={styles.Text2}>
                  Join and get exciting offers and packs
                </Text>
              </View>
              <View>
                <TouchableOpacity>
                  <View style={styles.backbutton}>
                    <Icon
                      name="md-arrow-forward"
                      size={16}
                      color="#d7d7d7"
                      style={{
                        height: 20,
                        width: 21,
                        borderColor: "#d7d7d7",
                        borderWidth: 1,
                        borderRadius: 22,
                        paddingLeft: 5,
                        paddingTop: 2,
                        marginLeft: 40
                      }}
                    />
                  </View>
                </TouchableOpacity>
              </View>
            </View>
            <View
              style={{
                width: "100%",
                height: 3,
                backgroundColor: "#DCDCDC",
                marginBottom: 5
              }}
            />
            <View
              style={{ marginTop: 10, marginBottom: 10, flexDirection: "row" }}
            >
              <Text style={styles.CarouselHeading1}>
                Player&apos;s Wallpaper
              </Text>
              <TouchableOpacity
                onPress={this.toPlayersWallpaper}
                style={{ width: "50%", flexDirection: "row-reverse" }}
              >
                <Text style={styles.ViewAll}>VIEW ALL</Text>
              </TouchableOpacity>
            </View>
            <View style={{ marginBottom: 10, flex: 1 }}>
              <CarouselPager
                ref={ref => {
                  this.carousel1 = ref;
                }}
                deltaDelay={10}
                horizontal
                initialPage={0}
                containerPadding={15}
                pageStyle={{
                  backgroundColor: "white",
                  height: 120,
                  width: 130
                }}
                blurredZoom={0}
                blurredOpacity={1}
                animationDuration={0}
              >
                <View key="page0">
                  {/* {
                    this.state.newsObjects.map((data) => {
                      return (
                        <View>
                          <Text>{data.image.id}</Text>
                        </View>
                    );
                  })
                  } */}
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect1} />
                  </View>
                </View>
                <View key="page1">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect1} />
                  </View>
                </View>
                <View key="page2">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect1} />
                  </View>
                </View>
                <View key="page3">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect1} />
                  </View>
                </View>
                <View key="page4">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect1} />
                  </View>
                </View>
                <TouchableOpacity onPress={() => this.carousel1.goToPage(0)}>
                  <View key="page5">
                    <View style={{ height: 120, width: 150 }}>
                      <Image source={Rect1} />
                    </View>
                  </View>
                </TouchableOpacity>
              </CarouselPager>
            </View>
            <View
              style={{
                width: "100%",
                height: 3,
                backgroundColor: "#DCDCDC",
                marginTop: 10,
                marginBottom: 5
              }}
            />
            <View
              style={{ marginTop: 10, marginBottom: 10, flexDirection: "row" }}
            >
              <Text style={styles.CarouselHeading1}>Hot Products</Text>
              <TouchableOpacity
                onPress={this.toProducts}
                style={{ width: "50%", flexDirection: "row-reverse" }}
              >
                <Text style={styles.ViewAll}>BROWSE ALL</Text>
              </TouchableOpacity>
            </View>
            <View style={{ flex: 1 }}>
              <CarouselPager
                ref={ref => {
                  this.carousel2 = ref;
                }}
                deltaDelay={10}
                initialPage={0}
                containerPadding={15}
                pageStyle={{
                  backgroundColor: "white",
                  height: 130,
                  width: 120
                }}
                blurredZoom={0}
                blurredOpacity={1}
                animationDuration={0}
              >
                <ScrollView key="page0">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect} />
                  </View>

                  <View style={{ backgroundColor: "#ffffff" }}>
                    <Text style={styles.CarText1}>Signed Cricket Bat</Text>
                    <Text style={styles.CarText2}>100 $52.49</Text>
                  </View>
                </ScrollView>
                <View key="page1">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect} />
                  </View>

                  <View style={{ backgroundColor: "#ffffff" }}>
                    <Text style={styles.CarText1}>Signed Cricket Bat</Text>
                    <Text style={styles.CarText2}>100 $52.49</Text>
                  </View>
                </View>
                <View key="page2">
                  <View style={{ height: 90, width: 150 }}>
                    <Image source={Rect} />
                  </View>
                  <View style={{ backgroundColor: "#ffffff" }}>
                    <Text style={styles.CarText1}>Signed Cricket Bat</Text>
                    <Text style={styles.CarText2}>100 $52.49</Text>
                  </View>
                </View>
                <TouchableOpacity onPress={() => this.carousel2.goToPage(0)}>
                  <View key="page3">
                    <View style={{ height: 90, width: 150 }}>
                      <Image source={Rect} />
                    </View>
                    <View style={{ backgroundColor: "#ffffff" }}>
                      <Text style={styles.CarText1}>Signed Cricket Bat</Text>
                      <Text style={styles.CarText2}>100 $52.49</Text>
                    </View>
                  </View>
                </TouchableOpacity>
              </CarouselPager>
            </View>
            <View
              style={{
                width: "100%",
                height: 3,
                backgroundColor: "#DCDCDC",
                marginTop: 10
              }}
            />
            <View style={{ flex: 1 }}>
              <Image style={{ height: 100, width: "100%" }} source={Bitmap} />
            </View>
            <View
              style={{ width: "100%", height: 3, backgroundColor: "#DCDCDC" }}
            />
            <View
              style={{ width: "100%", height: 1, backgroundColor: "#DCDCDC" }}
            />
            <View style={{ flex: 1 }}>
              <Image style={{ width: "100%" }} source={Bitmap} />
            </View>
            <View
              style={{ width: "100%", height: 3, backgroundColor: "#DCDCDC" }}
            />
            <TouchableOpacity onPress={this.toShippingInformation}>
              <View
                style={{
                  marginBottom: 2,
                  marginLeft: 100,
                  height: 60,
                  width: "100%"
                }}
              >
                <Text style={styles.PaymentText}>Payment and Registration</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

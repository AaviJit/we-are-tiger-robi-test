import React, { Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TextInput,
  TouchableOpacity
} from "react-native";
import PropTypes from "prop-types";
import CarouselPager from "react-native-carousel-pager";
import GeneralHeader from "../components/HeaderGeneral";
import Test from "../resources/img/test.png";

const styles = StyleSheet.create({
  Text1: {
    fontSize: 10,
    color: "#777777",
    fontWeight: "900",
    marginTop: 20,
    marginLeft: 10
  },
  TextInputs: {
    width: "90%",
    marginTop: 7,
    marginLeft: 10
  },
  Text2: {
    color: "#4a4a4a"
  },
  ProcToPay: {
    height: 45,
    marginTop: 10,
    marginBottom: 10,
    justifyContent: "center",
    marginRight: 10,
    marginLeft: 10,
    borderRadius: 22,
    backgroundColor: "#149b58"
  },
  ProcToPayText: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    fontWeight: "800",
    textAlign: "center",
    color: "#ffffff"
  },
  Text3: {
    marginLeft: 30,
    marginTop: 25,
    color: "#4a4a4a",
    fontSize: 10
  },
  Text4: {
    marginLeft: 30,
    color: "#4a4a4a",
    fontSize: 10
  }
});
export default class ShippingInformation extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  UponPagechange() {
    this.carousel.goToPage(1);
  }
  render() {
    return (
      <ScrollView style={{ backgroundColor: "#ffffff" }}>
        <View>
          <GeneralHeader
            nav={this.props.navigation}
            leftIcon="ios-arrow-back"
            back="TigersShop"
            HeaderTittle="Shipping Information"
          />
        </View>
        <View>
          <Text style={styles.Text1}>SHIPPING INFORMATION</Text>
        </View>
        <View
          style={{
            height: 2,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 15,
            marginBottom: 5
          }}
        />
        <View style={styles.TextInputs}>
          <TextInput
            style={{
              height: 40,
              width: "103%",
              textAlign: "left",
              borderBottomWidth: 2,
              borderColor: "#eeeeee",
              color: "#2e332f"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Firstname"
            placeholderTextColor="#C0C0C0"
          />
          <TextInput
            style={{
              height: 40,
              width: "103%",
              textAlign: "left",
              borderBottomWidth: 2,
              borderColor: "#eeeeee",
              color: "#2e332f"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Lastname"
            placeholderTextColor="#C0C0C0"
          />
          <TextInput
            style={{
              height: 40,
              width: "103%",
              textAlign: "left",
              borderBottomWidth: 2,
              borderColor: "#eeeeee",
              color: "#2e332f"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Phone number"
            placeholderTextColor="#C0C0C0"
          />
          <TextInput
            style={{
              height: 40,
              width: "103%",
              textAlign: "left",
              borderBottomWidth: 2,
              borderColor: "#eeeeee",
              color: "#2e332f"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Address"
            placeholderTextColor="#C0C0C0"
          />
          <View style={{ flexDirection: "row" }}>
            <TextInput
              style={{
                height: 40,
                width: "30%",
                textAlign: "left",
                borderBottomWidth: 2,
                borderColor: "#eeeeee",
                color: "#2e332f"
              }}
              underlineColorAndroid="transparent"
              selectionColor="#b39ddb"
              placeholder="Zip"
              placeholderTextColor="#C0C0C0"
            />
            <TextInput
              style={{
                height: 40,
                width: "65%",
                marginLeft: 10,
                textAlign: "left",
                borderBottomWidth: 2,
                borderColor: "#eeeeee",
                color: "#2e332f"
              }}
              underlineColorAndroid="transparent"
              selectionColor="#b39ddb"
              placeholder="City"
              placeholderTextColor="#C0C0C0"
            />
          </View>
        </View>
        <View
          style={{
            height: 1,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 15,
            marginBottom: 10
          }}
        />
        <View style={{ marginLeft: 10 }}>
          <Text
            style={{
              color: "#777777",
              fontFamily: "Roboto",
              fontWeight: "900",
              fontSize: 10
            }}
          >
            Payment TYPE
          </Text>
        </View>
        <View
          style={{
            height: 2,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 10
          }}
        />
        <View style={{ flex: 1, marginTop: 10 }}>
          <CarouselPager
            ref={this.carosel}
            initialPage={0}
            containerPadding={15}
            pageStyle={{
              backgroundColor: "#ffffff",
              height: 90,
              width: 140,
              borderWidth: 1.5,
              borderRadius: 10,
              borderColor: "#d9d9d9"
            }}
            blurredZoom={0}
            animationDuration={0}
            blurredOpacity={1}
            OnPagechange={this.UponPagechange}
          >
            <View key="page0">
              <Text style={styles.Text3}>CASH ON</Text>
              <Text style={styles.Text4}>DELIVERY</Text>
            </View>
            <View key="page1">
              <Image
                style={{ height: "100%", width: "100%", borderRadius: 10 }}
                source={Test}
              />
            </View>
            <View key="page2">
              <Image
                style={{ height: "100%", width: "100%", borderRadius: 10 }}
                source={Test}
              />
            </View>
          </CarouselPager>
        </View>
        <View
          style={{
            height: 1.5,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 6
          }}
        />
        <View style={{ marginLeft: 10, flexDirection: "row" }}>
          <Text
            style={{
              color: "#171616",
              fontFamily: "Roboto",
              fontWeight: "900",
              fontSize: 14,
              marginTop: 10
            }}
          >
            DELIVERY
          </Text>
          <Text
            style={{
              color: "#4a4a4a",
              fontFamily: "Roboto",
              fontWeight: "900",
              fontSize: 14,
              marginTop: 10,
              marginLeft: 270,
              marginRight: 10
            }}
          >
            20$
          </Text>
        </View>
        <View
          style={{
            height: 1.5,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 15
          }}
        />
        <View style={{ marginLeft: 10, flexDirection: "row" }}>
          <Text
            style={{
              color: "#171616",
              fontFamily: "Roboto",
              fontWeight: "900",
              fontSize: 14,
              marginTop: 10
            }}
          >
            TOTAL PRICE
          </Text>
          <Text
            style={{
              color: "#4a4a4a",
              fontFamily: "Roboto",
              fontWeight: "900",
              fontSize: 14,
              marginTop: 10,
              marginLeft: 242,
              marginRight: 10
            }}
          >
            2043$
          </Text>
        </View>
        <View
          style={{
            height: 1.5,
            width: "100%",
            backgroundColor: "#d9d9d9",
            marginTop: 15
          }}
        />
        <View>
          <TouchableOpacity>
            <View style={styles.ProcToPay}>
              <Text style={styles.ProcToPayText}>Proceed to payment</Text>
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

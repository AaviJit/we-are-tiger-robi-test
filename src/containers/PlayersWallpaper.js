import React from "react";
import PropTypes from "prop-types";
import { ScrollView, View } from "react-native";
import { Container, Tab, Tabs } from "native-base";
import GeneralHeader from "../components/HeaderGeneral";
import Tab1 from "../components/Hot";
import Tab2 from "../components/New";
import Tab3 from "../components/Popular";

const propTypes = {
  navigation: PropTypes.object.isRequired
};

const PlayersWallpaper = props => (
  <ScrollView>
    <View style={{ flex: 1, backgroundColor: "white" }}>
      <Container>
        <View>
          <GeneralHeader
            nav={props.navigation}
            leftIcon="ios-arrow-back"
            back="TigersShop"
            HeaderTittle="Players Wallpaper"
            RightPanelicon1="md-cart"
          />
        </View>
        <Tabs
          tabBarUnderlineStyle={{
            backgroundColor: "#23b06a",
            height: 2,
            width: 100,
            marginLeft: 15
          }}
          initialPage={0}
        >
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{ fontSize: 12, fontWeight: "normal", color: "#9b9b9b" }}
            activeTabStyle={{ backgroundColor: "white" }}
            tabStyle={{ backgroundColor: "white" }}
            heading="HOT"
          >
            <Tab1 />
          </Tab>
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{ fontSize: 12, fontWeight: "normal", color: "#9b9b9b" }}
            activeTabStyle={{ backgroundColor: "white" }}
            tabStyle={{ backgroundColor: "white" }}
            heading="NEW"
          >
            <Tab2 />
          </Tab>
          <Tab
            activeTextStyle={{ fontSize: 12, color: "#4a4a4a" }}
            textStyle={{ fontSize: 12, fontWeight: "normal", color: "#9b9b9b" }}
            activeTabStyle={{ backgroundColor: "white" }}
            tabStyle={{ backgroundColor: "white" }}
            heading="POPULAR"
          >
            <Tab3 />
          </Tab>
        </Tabs>
      </Container>
    </View>
  </ScrollView>
);
PlayersWallpaper.propTypes = propTypes;
export default PlayersWallpaper;

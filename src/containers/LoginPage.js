/* eslint-disable no-console*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
  AsyncStorage,
  KeyboardAvoidingView,
  Alert,
  StatusBar,
  Keyboard,
  TouchableWithoutFeedback,
  Dimensions,
  NetInfo
} from "react-native";
import axios from "axios";
import Icon from "react-native-vector-icons/Ionicons";
import backgroundImage from "../components/Images/newBackGroundImage.png";
import Logo from "../../assets/icon.png";
import FBSDK, {
  AccessToken,
  LoginManager,
  GraphRequestManager,
  GraphRequest
} from "react-native-fbsdk";

const styles = StyleSheet.create({
  text1: {
    fontSize: 20,
    fontFamily: "Lato-Regular",
    marginLeft: 10,
    color: "#ffffff",
    marginTop: 23,
    backgroundColor: "transparent"
  },
  backbutton: {
    marginLeft: 20,
    marginTop: 25,
    backgroundColor: "transparent"
  },
  text2: {
    fontFamily: "Roboto-Light",
    marginTop: "20%",
    fontSize: 28,
    textAlign: "center",
    // margin: 5,
    color: "#ffffff",
    backgroundColor: "transparent"
    //width :
  },
  text3: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    marginTop: "8%",
    color: "#ffffff",
    textAlign: "center",
    marginBottom: 18,
    backgroundColor: "transparent"
  },
  text4: {
    fontFamily: "Roboto-Light",
    fontSize: 12,
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text5: {
    fontFamily: "Roboto-Light",
    fontSize: 12,
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text6: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    marginTop: 10,
    color: "rgba(255,255,255,0.8)",
    backgroundColor: "transparent"
  },
  text7: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text8: {
    marginTop: 20,
    marginLeft: 50,
    marginRight: 50,
    backgroundColor: "transparent"
  },
  img: {
    flex: 1,
    position: "absolute",
    width: Dimensions.get("window").width,
    height: Dimensions.get("window").height,
    resizeMode: "stretch"
  },
  facebook: {
    height: 40,
    marginTop: "12%",
    marginLeft: "32%",
    marginRight: "32%",
    borderRadius: 22,
    backgroundColor: "#3e549c",
    marginBottom: 15,
    justifyContent: "center"
  },
  GOOGLE: {
    height: 40,
    marginTop: 10,
    marginLeft: 90,
    marginRight: 90,
    borderRadius: 22,
    backgroundColor: "#da4e29",
    justifyContent: "center"
  },
  login: {
    height: 45,
    marginTop: "20%",
    justifyContent: "center",
    marginRight: "32%",
    marginLeft: "32%",
    borderRadius: 22,
    borderWidth: 1,
    borderColor: "white"
  },
  loginText: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    color: "#ffffff",
    fontWeight: "500",
    backgroundColor: "transparent"
  }
});

export default class LoginPage extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: "",
      token: "",
      hidePassword: true
    };
  }
  componentWillMount() {
    StatusBar.setHidden(true);
  }
  hidePassword = () => {
    if(this.state.hidePassword) {
      this.setState({
        hidePassword: false
      });
    } else {
      this.setState({
        hidePassword: true
      });
    }
  }
  toDiscover = () => {
    this.props.navigation.navigate("Counter");
  };
  toRegisterPage = () => {
    this.props.navigation.navigate("RegisterPage");
  };

  responseInfoCallback = (err, result) => {
    console.log(result.id, this.state.token);
    axios
      .post(
        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/sso/facebook/login?accessToken=${
          this.state.token
        }&userId=${result.id}`
      )
      .then(response => {
        console.log(response);
        AsyncStorage.setItem("user", JSON.stringify(response.data));
        this.props.navigation.navigate("HomePage");
      })
      .catch(error => {
        console.log(error);
        Alert.alert("Internal Error");
      });
  };

  toFb = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithReadPermissions(["public_profile"]).then(
      result => {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            this.setState({
              token: data.accessToken
            });
            const infoRequest = new GraphRequest(
              "/me",
              {
                accessToken: data.accessToken,
                parameters: {
                  fields: {
                    string: "id"
                  }
                }
              },
              this.responseInfoCallback
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      error => {
        console.log(`Login fail with error: ${error}`);
      }
    );
  };
  toGoogle = () => {
    Alert.alert("Hey there!", "This feature is yet to come!");
  };
  toProfile = () => {
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === "none") {
        Alert.alert("No internet connection", "Please check your internet.");
      } else {
        axios
          .post(
            `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/oauth/token?username=${
              this.state.username
            }&password=${
              this.state.password
            }&grant_type=password&client_id=android-client&client_secret=android-secret`
          )
          .then(response => {
            // alert(JSON.stringify(response)+'login');
            if (this.state.username === "" || this.state.password === "") {
              Keyboard.dismiss();
              Alert.alert(
                "Hey there!",
                "Please fill out your credentials to login"
              );
            } else {
              console.log(response);
              AsyncStorage.setItem("user", JSON.stringify(response.data));
              this.props.navigation.navigate("HomePage");
              Keyboard.dismiss();
            }
          })
          .catch(() => {
            Alert.alert("Login Failed!", "please check your credentials.");
          });
      }
    });
  };

  render() {
    return (
      <KeyboardAvoidingView style={{ flex: 1 }} behavior="padding">
        <Image style={styles.img} source={backgroundImage} />
        <View
          style={{
            alignItems: "center",
            paddingTop: "25%",
            paddingBottom: "10%"
          }}
        >
          <Image style={{ height: 80, width: 90 }} source={Logo} />
        </View>

        <View style={styles.text8}>
          <TextInput
            style={{
              height: 40,
              width: "100%",
              textAlign: "center",
              borderBottomWidth: 1,
              borderColor: "rgba(255,255,255,.2)",
              color: "#C0C0C0",
              fontWeight: "600"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Email or Username"
            placeholderTextColor="#C0C0C0"
            onChangeText={username => this.setState({ username })}
          />

          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 1,
              borderColor: "#9B9B9B"
            }}
          >
            <TextInput
              style={{
                height: 40,
                width: "85%",
                textAlign: "center",
                color: "#C0C0C0",
                fontWeight: "600"
              }}
              underlineColorAndroid="transparent"
              selectionColor="#b39ddb"
              placeholder="Password"
              placeholderTextColor="#C0C0C0"
              onChangeText={password => this.setState({ password })}
              secureTextEntry={this.state.hidePassword}
            />
            <TouchableOpacity onPress={this.hidePassword}>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#9B9B9B",
                  textAlign: "center",
                  marginTop: 8
                }}
              >
                Show
              </Text>
            </TouchableOpacity>
          </View>
        </View>
        <View>
          <TouchableWithoutFeedback onPress={this.toProfile}>
            <View style={styles.login}>
              <Text style={styles.loginText}>Sign In</Text>
            </View>
          </TouchableWithoutFeedback>
        </View>
        <TouchableOpacity onPress={this.toFb}>
          <View style={styles.facebook}>
            <Text style={styles.text4}>FACEBOOK</Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.toRegisterPage}>
          <Text style={styles.text6}>Don&apos;t have an account?</Text>
        </TouchableOpacity>
      </KeyboardAvoidingView>
    );
  }
}

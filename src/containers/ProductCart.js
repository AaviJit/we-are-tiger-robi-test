import React, { Component } from "react";
import {
  Image,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ScrollView
} from "react-native";
import PropTypes from "prop-types";
import { Button } from "native-base";
import Swipeable from "react-native-swipeable";
import Customheader from "../components/HeaderGeneral2";
import CartImage from "../resources/icons/jersy.jpg";

const styles = StyleSheet.create({
  Swipeablestyle: {
    width: "100%",
    height: 140,
    backgroundColor: "white",
    borderBottomWidth: 1,
    borderBottomColor: "#dcdcdc",
    flexDirection: "row",
    justifyContent: "flex-start"
  },
  imageContainer: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    height: "100%",
    width: "30%"
    // backgroundColor: 'green'
  },
  textContainer: {
    width: "70%",
    height: "100%",
    // backgroundColor: 'grey',
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center"
  },
  textandinfo: {
    // backgroundColor:'red',
    top: 8,
    right: 10,
    width: "70%",
    height: "55%"
  },
  textandincrementer: {
    flexDirection: "row",
    justifyContent: "space-between",
    // backgroundColor: 'red',
    height: "100%",
    width: "70%"
  },
  iteminfo: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "63%",
    height: "25%"
    // backgroundColor: 'green'
  },
  textstyle: {
    fontSize: 16,
    fontFamily: "Roboto",
    fontWeight: "bold",
    color: "#4a4a4a"
  },
  infotextstyle: {
    fontSize: 13,
    fontFamily: "Roboto",
    fontWeight: "bold",
    color: "#777777"
  },
  incrementerdiv: {
    // backgroundColor:'green',
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "flex-end",
    height: "100%",
    width: "10%",
    marginRight: 10
  },
  incrementer: {
    // backgroundColor:'grey',
    width: "70%",
    height: "50%"
  },
  buttondiv: {
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#d0021b",
    height: "100%",
    width: "30%"
  }
});

const leftContent = <Text>Pull to activate</Text>;

const rightButtons = [
  <TouchableHighlight>
    <View style={styles.buttondiv}>
      {/* <LineIcon name='close' size={35} color='black' /> */}
      <Text
        style={{
          color: "white",
          backgroundColor: "#d0021b",
          fontSize: 24,
          fontFamily: "Roboto",
          fontWeight: "100"
        }}
      >
        X
      </Text>
    </View>
  </TouchableHighlight>
];

// function MyListItem() {
//   return (
//     <Swipeable leftContent={leftContent} rightButtons={rightButtons}>
//       <Text>My swipeable content</Text>
//     </Swipeable>
//   );
// }

export default class ProductCart extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      isSwiping: false
    };
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Customheader
          leftIcon="arrow-left"
          HeaderTittle="Cart"
          nav={this.props.navigation}
          back="HomePage"
        />
        <ScrollView scrollEnabled={!this.state.isSwiping}>
          <View style={{ flex: 1 }}>
            <Swipeable
              onSwipeStart={() => this.setState({ isSwiping: true })}
              onSwipeRelease={() => this.setState({ isSwiping: false })}
              leftContent={leftContent}
              rightButtons={rightButtons}
            >
              <View style={styles.Swipeablestyle}>
                <View style={styles.imageContainer}>
                  <Image
                    style={{ height: 100, width: 100, top: 4 }}
                    source={CartImage}
                  />
                </View>
                <View style={styles.textandincrementer}>
                  <View style={styles.textContainer}>
                    <View style={styles.textandinfo}>
                      <Text style={styles.textstyle}>
                        Bangladesh Cricket Team Jersey
                      </Text>
                      <View style={styles.iteminfo}>
                        <Text style={styles.infotextstyle}>One Size</Text>
                        <Text style={styles.infotextstyle}>380$</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.incrementerdiv}>
                    <View style={styles.incrementer}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#d9d9d9",
                          marginBottom: 3
                        }}
                      >
                        +
                      </Text>
                      <Text style={{ fontSize: 17, color: "black" }}>1</Text>
                      <Text style={{ fontSize: 26, color: "#d9d9d9" }}>-</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Swipeable>
            <Swipeable
              onSwipeStart={() => this.setState({ isSwiping: true })}
              onSwipeRelease={() => this.setState({ isSwiping: false })}
              leftContent={leftContent}
              rightButtons={rightButtons}
            >
              <View style={styles.Swipeablestyle}>
                <View style={styles.imageContainer}>
                  <Image
                    style={{ height: 100, width: 100, top: 4 }}
                    source={CartImage}
                  />
                </View>
                <View style={styles.textandincrementer}>
                  <View style={styles.textContainer}>
                    <View style={styles.textandinfo}>
                      <Text style={styles.textstyle}>
                        Bangladesh Cricket Team Jersey
                      </Text>
                      <View style={styles.iteminfo}>
                        <Text style={styles.infotextstyle}>One Size</Text>
                        <Text style={styles.infotextstyle}>380$</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.incrementerdiv}>
                    <View style={styles.incrementer}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#d9d9d9",
                          marginBottom: 3
                        }}
                      >
                        +
                      </Text>
                      <Text style={{ fontSize: 17, color: "black" }}>1</Text>
                      <Text style={{ fontSize: 26, color: "#d9d9d9" }}>-</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Swipeable>
            <Swipeable
              onSwipeStart={() => this.setState({ isSwiping: true })}
              onSwipeRelease={() => this.setState({ isSwiping: false })}
              leftContent={leftContent}
              rightButtons={rightButtons}
            >
              <View style={styles.Swipeablestyle}>
                <View style={styles.imageContainer}>
                  <Image
                    style={{ height: 100, width: 100, top: 4 }}
                    source={CartImage}
                  />
                </View>
                <View style={styles.textandincrementer}>
                  <View style={styles.textContainer}>
                    <View style={styles.textandinfo}>
                      <Text style={styles.textstyle}>
                        Bangladesh Cricket Team Jersey
                      </Text>
                      <View style={styles.iteminfo}>
                        <Text style={styles.infotextstyle}>One Size</Text>
                        <Text style={styles.infotextstyle}>380$</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.incrementerdiv}>
                    <View style={styles.incrementer}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#d9d9d9",
                          marginBottom: 3
                        }}
                      >
                        +
                      </Text>
                      <Text style={{ fontSize: 17, color: "black" }}>1</Text>
                      <Text style={{ fontSize: 26, color: "#d9d9d9" }}>-</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Swipeable>
            <Swipeable
              onSwipeStart={() => this.setState({ isSwiping: true })}
              onSwipeRelease={() => this.setState({ isSwiping: false })}
              leftContent={leftContent}
              rightButtons={rightButtons}
            >
              <View style={styles.Swipeablestyle}>
                <View style={styles.imageContainer}>
                  <Image
                    style={{ height: 100, width: 100, top: 4 }}
                    source={CartImage}
                  />
                </View>
                <View style={styles.textandincrementer}>
                  <View style={styles.textContainer}>
                    <View style={styles.textandinfo}>
                      <Text style={styles.textstyle}>
                        Bangladesh Cricket Team Jersey
                      </Text>
                      <View style={styles.iteminfo}>
                        <Text style={styles.infotextstyle}>One Size</Text>
                        <Text style={styles.infotextstyle}>380$</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.incrementerdiv}>
                    <View style={styles.incrementer}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#d9d9d9",
                          marginBottom: 3
                        }}
                      >
                        +
                      </Text>
                      <Text style={{ fontSize: 17, color: "black" }}>1</Text>
                      <Text style={{ fontSize: 26, color: "#d9d9d9" }}>-</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Swipeable>
            <Swipeable
              onSwipeStart={() => this.setState({ isSwiping: true })}
              onSwipeRelease={() => this.setState({ isSwiping: false })}
              leftContent={leftContent}
              rightButtons={rightButtons}
            >
              <View style={styles.Swipeablestyle}>
                <View style={styles.imageContainer}>
                  <Image
                    style={{ height: 100, width: 100, top: 4 }}
                    source={CartImage}
                  />
                </View>
                <View style={styles.textandincrementer}>
                  <View style={styles.textContainer}>
                    <View style={styles.textandinfo}>
                      <Text style={styles.textstyle}>
                        Bangladesh Cricket Team Jersey
                      </Text>
                      <View style={styles.iteminfo}>
                        <Text style={styles.infotextstyle}>One Size</Text>
                        <Text style={styles.infotextstyle}>380$</Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.incrementerdiv}>
                    <View style={styles.incrementer}>
                      <Text
                        style={{
                          fontSize: 20,
                          color: "#d9d9d9",
                          marginBottom: 3
                        }}
                      >
                        +
                      </Text>
                      <Text style={{ fontSize: 17, color: "black" }}>1</Text>
                      <Text style={{ fontSize: 26, color: "#d9d9d9" }}>-</Text>
                    </View>
                  </View>
                </View>
              </View>
            </Swipeable>

            <View
              style={{
                height: 50,
                width: "100%",
                flexDirection: "row",
                justifyContent: "space-between",
                backgroundColor: "#f5f5f5",
                alignItems: "center",
                padding: 14
              }}
            >
              <Text
                style={{
                  fontFamily: "Roboto",
                  fontWeight: "bold",
                  color: "#4a4a4a"
                }}
              >
                SUBTOTAL
              </Text>
              <Text
                style={{
                  fontFamily: "Roboto",
                  fontWeight: "bold",
                  color: "grey"
                }}
              >
                2023$
              </Text>
            </View>
            <View
              style={{
                height: 140,
                width: "100%",
                flexDirection: "column",
                backgroundColor: "#ffffff",
                justifyContent: "center",
                alignContent: "center",
                padding: 14
              }}
            >
              <Button
                style={{
                  margin: 8,
                  width: "95%",
                  flexDirection: "column",
                  justifyContent: "center",
                  backgroundColor: "#149b58"
                }}
                rounded
                success
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "Roboto",
                    fontWeight: "bold",
                    color: "white"
                  }}
                >
                  Proceed to checkout
                </Text>
              </Button>
              <Button
                style={{
                  margin: 8,
                  width: "95%",
                  flexDirection: "column",
                  justifyContent: "center",
                  backgroundColor: "white",
                  borderWidth: 1,
                  borderColor: "#bababa",
                  borderStyle: "solid"
                }}
                rounded
                success
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontFamily: "Roboto",
                    fontWeight: "bold",
                    color: "black"
                  }}
                >
                  Continue shopping
                </Text>
              </Button>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

/* eslint-disable no-console*/
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  AsyncStorage,
  Keyboard,
  NetInfo
} from "react-native";
import axios from "axios";
import Icon from "react-native-vector-icons/Ionicons";
import backgroundImage from "../components/Images/backgroundImage.png";
import FBSDK, {
  AccessToken,
  LoginManager,
  GraphRequestManager,
  GraphRequest
} from "react-native-fbsdk";

const styles = StyleSheet.create({
  text1: {
    fontSize: 20,
    fontFamily: "Lato-Regular",
    marginLeft: 10,
    color: "#ffffff",
    marginTop: 23,
    backgroundColor: "transparent"
  },
  text3: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    marginTop: "15%",
    color: "#ffffff",
    textAlign: "center",
    marginBottom: 18,
    backgroundColor: "transparent"
  },
  text4: {
    fontFamily: "Roboto-Light",
    fontSize: 12,
    textAlign: "center",
    margin: 10,
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text5: {
    fontFamily: "Roboto-Light",
    fontSize: 12,
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text6: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    marginTop: 20,
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text7: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  },
  text8: {
    marginLeft: 50,
    marginRight: 50,
    backgroundColor: "transparent"
  },
  img: {
    top: -100,
    flex: 1,
    position: "absolute",
    width: "100%",
    height: "150%",
    resizeMode: "stretch"
  },
  facebook: {
    height: 40,
    marginLeft: 90,
    marginRight: 90,
    borderRadius: 22,
    backgroundColor: "#225b99",
    marginTop: 10,
    justifyContent: "center"
  },
  backbutton: {
    marginLeft: 20,
    marginTop: 25,
    backgroundColor: "transparent"
  },
  GOOGLE: {
    height: 40,
    marginTop: 10,
    marginLeft: 90,
    marginRight: 90,
    borderRadius: 22,
    backgroundColor: "#da4e29",
    justifyContent: "center"
  },
  login: {
    height: 45,
    marginTop: 20,
    justifyContent: "center",
    marginRight: "35%",
    marginLeft: "35%",
    borderRadius: 22,
    borderWidth: 2,
    borderColor: "white"
  },
  loginText: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    color: "#ffffff",
    backgroundColor: "transparent"
  }
});

export default class RegisterPage extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      phoneNo: "",
      firstName: "",
      lastName: "",
      password: "",
      confirmPassword: "",
      hidePassword: true,
      hideConfirmPassword: true
    };
  }
  hidePassword = () => {
    if(this.state.hidePassword) {
      this.setState({
        hidePassword: false
      });
    } else {
      this.setState({
        hidePassword: true
      });
    }
  }
  hideConfirmPassword = () => {
    if(this.state.hideConfirmPassword) {
      this.setState({
        hideConfirmPassword: false
      });
    } else {
      this.setState({
        hideConfirmPassword: true
      });
    }
  }
  toDiscover = () => {
    this.props.navigation.navigate("Counter");
  };
  toFacebook = () => {
    this.props.navigation.navigate("");
  };
  toGoogleplus = () => {
    this.props.navigation.navigate("");
  };
  responseInfoCallback = (err, result) => {
    console.log(result.id, this.state.token);
    axios
      .post(
        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/sso/facebook/login?accessToken=${
          this.state.token
        }&userId=${result.id}`
      )
      .then(response => {
        console.log(response);
        AsyncStorage.setItem("user", JSON.stringify(response.data));
        this.props.navigation.navigate("HomePage");
      })
      .catch(error => {
        console.log(error);
        Alert.alert("Internal Error");
      });
  };

  toFb = () => {
    // Attempt a login using the Facebook login dialog asking for default permissions.
    LoginManager.logInWithReadPermissions(["public_profile"]).then(
      result => {
        if (result.isCancelled) {
          console.log("Login cancelled");
        } else {
          AccessToken.getCurrentAccessToken().then(data => {
            this.setState({
              token: data.accessToken
            });
            const infoRequest = new GraphRequest(
              "/me",
              {
                accessToken: data.accessToken,
                parameters: {
                  fields: {
                    string: "id"
                  }
                }
              },
              this.responseInfoCallback
            );
            // Start the graph request.
            new GraphRequestManager().addRequest(infoRequest).start();
          });
        }
      },
      error => {
        console.log(`Login fail with error: ${error}`);
      }
    );
  };

  toLoginPageNavigation = () => {
    this.props.navigation.navigate("LoginPage");
  };
  RegisterFun = () => {
    let empty = false;
    let navToLoginPage = true;
    console.log(
      `email:${this.state.email}firstName:${this.state.firstName}lastName:${
        this.state.lastName
      }password:${this.state.password}phoneNo:${this.state.phoneNo}`
    );
    NetInfo.getConnectionInfo().then(connectionInfo => {
      if (connectionInfo.type === "none") {
        Alert.alert("No internet connection", "Please check your internet.");
      } else {
        axios
      .post(
        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/registration/?email=${
          this.state.email
        }&firstName=${this.state.firstName}&lastName=${
          this.state.lastName
        }&password=${this.state.password}&phoneNumber=${this.state.phoneNo}`
      )
      .then(res => {
        console.log(res);
        const reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(this.state.email) === false) {
          navToLoginPage = false;
          Alert.alert("Hey there!", "please use valid email id");
        }
        if (this.state.confirmPassword !== this.state.password) {
          navToLoginPage = false;
          Alert.alert("Hey there!", "Check your password and try again");
        }
        if (navToLoginPage) {
          Alert.alert(
            "Hey there!",
            "You have successfully registered to We Are Tigers!"
          );
          this.toLoginPage();
        }
      })
      .catch(error => {
        if (
          this.state.email === null ||
          this.state.phoneNo === "" ||
          this.state.firstName === "" ||
          this.state.lastName === "" ||
          this.state.password === "" ||
          this.state.email === " " ||
          this.state.phoneNo === " " ||
          this.state.firstName === " " ||
          this.state.lastName === " " ||
          this.state.password === " "
        ) {
          navToLoginPage = false;
          empty = true;
        }
        //console.log(error.response);
        if (error.response.status === 400) {
          Alert.alert("Hello there!", "please fill out all the fields correctly");
        }
        if (error.response.status === 409) {
          if(empty === true) {
            Alert.alert(
              "Hello there!",
              "please fill out all the fields"
            );
          } else {
            Alert.alert(
              "Hello there!",
              "There exist an user with this credentials"
            );
          }
        }
      });
      }
    });

  };
  toLoginPage = () => {
    console.log(`${this.state.email + this.state.password}`);
    axios
      .post(
        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/oauth/token?username=${
          this.state.email
        }&password=${
          this.state.password
        }&grant_type=password&client_id=android-client&client_secret=android-secret`
      )
      .then(response => {
        // alert(JSON.stringify(response)+'login');
        if (this.state.email === "" || this.state.password === "") {
          Keyboard.dismiss();
          Alert.alert(
            "Hey there!",
            "Please fill out your credentials to login"
          );
        } else {
          console.log(response);
          AsyncStorage.setItem("user", JSON.stringify(response.data));
          this.props.navigation.navigate("HomePage");
          Keyboard.dismiss();
        }
      })
      .catch(() => {
        Alert.alert("Login Failed!", "please check your credentials.");
      });
  };
  render() {
    return (
      <View style={{ flex: 1, marginTop: 60 }}>
        <Image style={styles.img} source={backgroundImage} />
        <Text style={styles.text3}>You can register with-</Text>
        <TouchableOpacity onPress={this.toFb}>
          <View style={styles.facebook}>
            <Text style={styles.text4}>FACEBOOK</Text>
          </View>
        </TouchableOpacity>
        <View>
          <Text style={styles.text6}>or you can register</Text>
          <Text style={styles.text7}>manually -</Text>
        </View>
        <View style={styles.text8}>
          <TextInput
            style={{
              height: 35,
              width: "100%",
              borderBottomWidth: 2,
              borderColor: "#9B9B9B",
              color: "#ffffff"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Username"
            placeholderTextColor="#9B9B9B"
            onChangeText={firstName => this.setState({ firstName })}
          />
          <TextInput
            style={{
              height: 35,
              width: "100%",
              borderBottomWidth: 2,
              borderColor: "#9B9B9B",
              color: "#ffffff"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Phone Number"
            placeholderTextColor="#9B9B9B"
            keyboardType="phone-pad"
            onChangeText={phoneNo => this.setState({ phoneNo })}
          />
          <TextInput
            style={{
              height: 35,
              width: "100%",
              borderBottomWidth: 2,
              borderColor: "#9B9B9B",
              color: "#ffffff"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Email"
            placeholderTextColor="#9B9B9B"
            onChangeText={email => this.setState({ email })}
          />
          <TextInput
            style={{
              height: 35,
              width: "100%",
              borderBottomWidth: 2,
              borderColor: "#9B9B9B",
              color: "#ffffff"
            }}
            underlineColorAndroid="transparent"
            selectionColor="#b39ddb"
            placeholder="Name"
            placeholderTextColor="#9B9B9B"
            onChangeText={lastName => this.setState({ lastName })}
          />
          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 2,
              borderColor: "#9B9B9B"
            }}
          >
            <TextInput
              style={{
                height: 35,
                width: "85%",
                color: "#ffffff"
              }}
              underlineColorAndroid="transparent"
              selectionColor="#b39ddb"
              placeholder="Password"
              placeholderTextColor="#9B9B9B"
              secureTextEntry={this.state.hidePassword}
              onChangeText={password => this.setState({ password })}
            />
            <TouchableOpacity onPress={this.hidePassword}>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#9B9B9B",
                  textAlign: "center",
                  marginTop: 8
                }}
              >
                Show
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              flexDirection: "row",
              borderBottomWidth: 3,
              borderColor: "#9B9B9B"
            }}
          >
            <TextInput
              style={{
                height: 35,
                width: "85%",
                color: "#ffffff"
              }}
              underlineColorAndroid="transparent"
              selectionColor="#b39ddb"
              placeholder="Confirm Password"
              placeholderTextColor="#9B9B9B"
              secureTextEntry={this.state.hideConfirmPassword}
              onChangeText={confirmPassword =>
                this.setState({ confirmPassword })
              }
            />
            <TouchableOpacity onPress={this.hideConfirmPassword}>
              <Text
                style={{
                  fontWeight: "bold",
                  color: "#9B9B9B",
                  textAlign: "center",
                  marginTop: 8
                }}
              >
                Show
              </Text>
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.login}>
          <TouchableOpacity onPress={this.RegisterFun}>
            <Text style={styles.loginText}>Register</Text>
          </TouchableOpacity>
        </View>
        <TouchableOpacity onPress={this.toLoginPageNavigation}>
          <View>
            <Text style={styles.text6}>Or click here to login</Text>
          </View>
        </TouchableOpacity>

        </View>
    );
  }
}

/* eslint-disable no-console*/
import React, { Component } from "react";
import { Text, View, StyleSheet, StatusBar } from "react-native";
import PropTypes from "prop-types";
import { Switch } from "react-native-switch";
import Customheader from "../components/HeaderGeneral2";
import { DrawerChecker, DrawerChanger } from "../components/DrawerChecker";

const styles = StyleSheet.create({
  label: {
    width: "100%",
    height: "9%",
    backgroundColor: "#f1f1f1",
    flexDirection: "column",
    justifyContent: "center",
    paddingHorizontal: "8%"
  },
  division: {
    width: "100%",
    height: "8%",
    backgroundColor: "white",
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    // alignItems: "center",
    paddingHorizontal: "8%",
    borderBottomWidth: 1,
    borderBottomColor: "#f1f1f1"
  },
  divisionText: {
    fontFamily: "Roboto-Regular",
    fontSize: 16,
    color: "#2e332f"
  },
  footings: {
    width: "100%",
    backgroundColor: "#f1f1f1",
    justifyContent: "center",
    alignItems: "center",
    paddingTop: 20,
    paddingBottom: 20
  }
});

export default class Settings extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#36be6b");
    const drawerOpen = DrawerChecker("drawer");
    if (drawerOpen) {
      DrawerChanger(false);
      return true;
    }
  }
  render() {
    return (
      <View style={{ flex: 1 }}>
        <Customheader
          leftIcon="arrow-left"
          HeaderTittle="Settings"
          nav={this.props.navigation}
          back="HomePage"
          RightPanelIcon1=""
        />
        <View style={styles.label}>
          <Text
            style={{
              fontFamily: "Roboto-Black",
              lineHeight: 15,
              fontSize: 13,
              color: "#4a4a4a",
              fontWeight: "bold"
            }}
          >
            SETTINGS
          </Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Edit Profile</Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Change Password</Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Send push notifications</Text>
          <Switch
            value
            onValueChange={val => console.log(val)}
            disabled={false}
            circleSize={26}
            barHeight={30}
            circleBorderWidth={0}
            backgroundActive="#23b06a"
            backgroundInactive="gray"
            circleActiveColor="white"
            circleInActiveColor="white"
            changeValueImmediately
            // custom component to render inside the Switch circle (Text, Image, etc.)
            // if rendering inside circle, change state immediately or wait for animation to complete
            innerCircleStyle={{
              alignItems: "center",
              justifyContent: "center"
            }} // style for inner animated circle for what you (may) be rendering inside the circle
            outerCircleStyle={{}} // style for outer animated circle
            renderActiveText={false}
            renderInActiveText={false}
            switchLeftPx={3} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
            switchRightPx={3} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
            switchWidthMultiplier={1.9}
          />
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Refresh automatically</Text>
          <Switch
            value
            onValueChange={val => console.log(val)}
            disabled={false}
            circleSize={26}
            barHeight={30}
            circleBorderWidth={0}
            backgroundActive="#23b06a"
            backgroundInactive="gray"
            circleActiveColor="white"
            circleInActiveColor="white"
            changeValueImmediately
            // custom component to render inside the Switch circle (Text, Image, etc.)
            // if rendering inside circle, change state immediately or wait for animation to complete
            innerCircleStyle={{
              alignItems: "center",
              justifyContent: "center"
            }} // style for inner animated circle for what you (may) be rendering inside the circle
            outerCircleStyle={{}} // style for outer animated circle
            renderActiveText={false}
            renderInActiveText={false}
            switchLeftPx={3} // denominator for logic when sliding to TRUE position. Higher number = more space from RIGHT of the circle to END of the slider
            switchRightPx={3} // denominator for logic when sliding to FALSE position. Higher number = more space from LEFT of the circle to BEGINNING of the slider
            switchWidthMultiplier={1.9}
          />
        </View>
        <View style={styles.label}>
          <Text
            style={{
              fontFamily: "Roboto-Black",
              lineHeight: 15,
              fontSize: 13,
              color: "#4a4a4a",
              fontWeight: "bold"
            }}
          >
            SUPPORT
          </Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>About We Are Tigers</Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Support</Text>
        </View>
        <View style={styles.division}>
          <Text style={styles.divisionText}>Find WIC</Text>
        </View>
        <View style={styles.footings}>
          <Text
            style={{
              fontFamily: "Roboto-Regular",
              color: "#2e332f",
              fontWeight: "400"
            }}
          >
            Powered by Robi Axiata
          </Text>
        </View>
      </View>
    );
  }
}

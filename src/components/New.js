import React from "react";
import {
  View,
  StyleSheet,
  FlatList,
  ScrollView,
  Image,
  Dimensions
} from "react-native";
import image from "../resources/img/test.png";

const styles = StyleSheet.create({
  list: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: 1,
    marginRight: 1
  }
});

const New = () => {
  const renderItem = () => (
    <View
      style={{
        flex: 1,
        margin: 1,
        width: (Dimensions.get("window").width - 10) / 3,
        height: (Dimensions.get("window").width - 10) / 3,
        backgroundColor: "#CCC"
      }}
    >
      <Image style={{ height: "100%", width: "100%" }} source={image} />
    </View>
  );
  return (
    <ScrollView>
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={styles.list}
          data={[
            { id: 1 },
            { id: 2 },
            { id: 3 },
            { id: 4 },
            { id: 5 },
            { id: 6 },
            { id: 7 },
            { id: 8 },
            { id: 9 },
            { id: 10 },
            { id: 11 }
          ]}
          keyExtractor={item => item.id}
          showsHorizontalScrollIndicator={false}
          renderItem={renderItem}
        />
      </View>
    </ScrollView>
  );
};
export default New;

import React from "react";
import { Text, View } from "react-native";
import Carousel from "react-native-snap-carousel";

const MyCarousel = () => {
  const renderItem = item => (
    <View>
      <Text>{item.title}</Text>
    </View>
  );

  return (
    <Carousel
      ref={c => {
        this.carousel = c;
      }}
      data={this.state.entries}
      renderItem={renderItem}
    />
  );
};

export default MyCarousel;

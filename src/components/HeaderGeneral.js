import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/Ionicons";

const styles = StyleSheet.create({
  headertext: {
    fontSize: 17,
    fontWeight: "800",
    fontFamily: "Roboto",
    textAlign: "center",
    color: "#ffffff"
  },
  headerwearetigers: {
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },

  headerRightPanel: {
    width: "20%",
    flexDirection: "row-reverse",
    alignItems: "center",
    paddingLeft: 18
  },
  headerLeftPanel: {
    justifyContent: "center",
    width: "20%",
    paddingLeft: 18
  }
});

export default class HeaderGeneralComponent extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired,
    back: PropTypes.string,
    leftIcon: PropTypes.string,
    RightPanelicon1: PropTypes.string,
    HeaderTittle: PropTypes.string
  };
  static defaultProps = {
    back: "HomePage",
    leftIcon: "",
    RightPanelicon1: "",
    HeaderTittle: ""
  };

  // generateicon = ()=>{
  //   return(

  //   );

  toBack = () => {
    // alert(JSON.stringify(this.props));
    this.props.nav.navigate(this.props.back);
  };

  render() {
    return (
      <View
        style={{
          flexDirection: "row",
          backgroundColor: "#36be6b",
          height: 60.6
        }}
      >
        <View style={styles.headerLeftPanel}>
          <TouchableOpacity
            onPress={() => {
              this.toBack();
            }}
          >
            <Icon name={this.props.leftIcon} size={25} color="#ffffff" />
          </TouchableOpacity>
        </View>
        <View style={styles.headerwearetigers}>
          <Text style={styles.headertext}>{this.props.HeaderTittle}</Text>
        </View>

        <View style={styles.headerRightPanel}>
          <TouchableOpacity>
            <Icon name={this.props.RightPanelicon1} size={20} color="#ffffff" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

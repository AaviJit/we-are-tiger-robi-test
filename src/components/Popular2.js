import React from "react";
import { View, StyleSheet, FlatList, ScrollView } from "react-native";

const styles = StyleSheet.create({
  list: {
    flexDirection: "row",
    flexWrap: "wrap",
    marginLeft: 1,
    marginRight: 1
  }
});

const Popular = () => {
  const renderItem = () => (
    <View
      style={{
        flex: 1,
        margin: 7,
        width: 165,
        height: 170,
        backgroundColor: "#CCC"
      }}
    />
  );
  return (
    <ScrollView>
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={styles.list}
          data={[{ key: "1" }, { key: "2" }, { key: "3" }, { key: "4" }]}
          renderItem={renderItem}
        />
      </View>
    </ScrollView>
  );
};
export default Popular;

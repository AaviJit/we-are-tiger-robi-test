import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  ListView,
  StyleSheet,
  View,
  AsyncStorage,
  Alert,
  WebView,
  Platform,
  ActivityIndicator,
  Image
} from "react-native";
import axios from "axios";
import InfiniteScroll from "react-native-infinite-scroll";
//import Thumbnail from "react-native-thumbnail-video";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
  container: {
    // paddingBottom: 60,
  },
  carditem: {
    width: "100%",
    backgroundColor: "white",
    // flex: 1,
    paddingBottom: 2,
    paddingTop: 2,
    paddingLeft: 15,
    paddingRight: 15,
    flexDirection: "row",
    justifyContent: "space-around"
  },
  cardImage: {
    height: 200,
    width: "100%"
  },
  cardImageVideos: {
    height: 100,
    width: 150
  },
  exploremore: {
    width: "100%",
    backgroundColor: "white",
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#ffffff",
    borderBottomColor: "#ffffff"
  },
  textandvideolength: {
    flexDirection: "column",
    flexWrap: "wrap"
  },
  WebViewContainer: {
    marginTop: Platform.OS === "ios" ? 20 : 0
  }
});

export default class VideosTab1 extends Component {
  static propTypes = {
    navigation: PropTypes.object.isRequired
  };
  // pagination_no = 0;
  constructor(props) {
    super(props);
    this.dataSource = new ListView.DataSource({
      rowHasChanged: (r1, r2) => r1 !== r2
    });
    this.state = {
      pageNo: 0,
      access_token: "",
      loadingstate: true,
      videoObjects: [],
      loading: true,
      topBanner: ""
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        this.setState(
          {
            access_token: x.access_token
          },
          () => {
            this.axiosGetVideoContents(
              `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secretid&id=0`
            );
          }
        );
        axios
              .get(
                `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
                  x.access_token
                }&client_id=android-client&client_secret=android-secret&pageNumber=6&position=1`
              )
              .then(banner => {
                console.log(banner);
                this.setState({
                  topBanner: banner.data.image.url
                });
              });
      })
      .catch(() => {});
  }
  loadMorePage = () => {
    this.setState({
      loading: false
    });
    this.setState({
      pageNo: this.state.pageNo + 1
    });
    this.axiosGetVideoContents(
      `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/?access_token=${
        this.state.access_token
      }&client_id=android-client&client_secret=android-secret&id=${
        this.state.pageNo
      }`
    );
  };

  axiosGetVideoContents = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        this.setState({
          loading: false
        });
        //Alert.alert(JSON.stringify(response.data.content));
        console.log(JSON.stringify(response.data.content));
        response.data.content.map(
          video => {
            this.setState({
              videoObjects: [...this.state.videoObjects, video]
            });
            return null;
          },
          () => {
            this.setState({
              loadingstate: false
            });
          }
        );
        // alert(JSON.stringify(this.state.videoObjects));
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.axiosGetVideoContents(
                `http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/video/page?access_token=${
                  res.access_token
                }&client_id=android-client&client_secret=android-secret&id=${
                  this.state.pageNo
                }`
              );
            })
            .catch(() => {
              this.setState({
                loading: false
              });
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.navigation.navigate("LoginPage");
            });
        } else {
          this.setState({
            loading: false
          });
          //alert(error.response.status);
          Alert.alert("Sorry, no More videos to display!");
        }
      });
  };

  render() {
    const rows = this.dataSource.cloneWithRows(this.state.videoObjects || []);
    return (
      <View style={styles.container}>
        {this.state.topBanner !== "" ? (
          <View
            style={{
              width: "100%",
              backgroundColor: "white",
              // flex: 1,
              paddingBottom: 2,
              paddingTop: 2,
              // paddingLeft: 15,
              // paddingRight: 15,
              flexDirection: "row",
              justifyContent: "center"
            }}
          >
            <Image
              style={{ width: "80%", height: 60, resizeMode: "cover" }}
              source={{
                uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
              }}
            />
          </View>
        ) : null}
        <InfiniteScroll
          horizontal={false} //true - if you want in horizontal
          onLoadMoreAsync={this.loadMorePage}
          distanceFromEnd={1}
        >
          <View style={styles.news}>
            <ListView
              enableEmptySections
              dataSource={rows}
              renderRow={data => (
                // <View style={styles.carditem}>
                //   <Thumbnail
                //     url={`http://159.89.195.154:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/${
                //       data.url
                //     }`}
                //   />
                // </View>
                <View style={{ height: 200 }}>
                  <WebView
                    ref={(ref) => { this.videoPlayer = ref;}}
                    style={styles.WebViewContainer}
                    javaScriptEnabled
                    domStorageEnabled
                    source={{
                      uri: `https://www.youtube.com/embed/${data.url}`
                    }}
                  />
                </View>
              )}
            />
            {this.state.loading && (
              <ActivityIndicator size="large" color="#rgb(54,190,107)" />
            )}
          </View>
        </InfiniteScroll>
      </View>
    );
  }
}

import React, { Component } from "react";
import PropTypes from "prop-types";
import {
    ScrollView,
    Image,
    StyleSheet,
    Text,
    View,
    AsyncStorage,
    Alert,
    ActivityIndicator
} from "react-native";
import axios from "axios";
// import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import SeriesIcon from "../resources/icons/weIcon.png";
import BannerImage from "../resources/icons/group_3.png";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
    Row11: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "#138b50",
        width: "36%",
        paddingLeft: 13
    },
    Row11bold: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "black",
        width: "36%",
        paddingLeft: 13
    },

    playersScore: {
        flexDirection: "row",
        justifyContent: "flex-end",
        width: "60%",
        height: "100%",
        // justifyContent: 'center',
        marginRight: 12
        //backgroundColor: "red"
    },
    rowElementContainer: {
        flexDirection: "row",
        marginHorizontal: 4,
        width: "16%",
        justifyContent: "flex-end"
        //backgroundColor: "green"
    },
    scoredisplaycontainer: {
        width: "100%",
        height: 30,
        backgroundColor: "white",
        borderTopWidth: 1,
        borderTopColor: "#dcdcdc",
        flexDirection: "column",
        justifyContent: "center"
    },
    scoredisplaycontainerBowlerComment: {
        height: "30%",
        width: "100%",
        backgroundColor: "blue",
        borderTopWidth: 1,
        borderTopColor: "#dcdcdc",
        flexDirection: "column",
        justifyContent: "center"
    },
    scoredisplaycontaineronnocolor: {
        width: "100%",
        height: 30,
        backgroundColor: "#f9f9f9",
        borderTopWidth: 1,
        borderTopColor: "#dcdcdc",
        flexDirection: "column",
        justifyContent: "center"
    },
    rowElementText: {
        fontFamily: "Roboto",
        fontSize: 12,
        width: "12%",
        textAlign: "center"
    },
    rowElementTextbold: {
        fontFamily: "Roboto",
        fontSize: 12,
        color: "black",
        width: "12%",
        textAlign: "center"
    },
    rowElementTextboldSR: {
        fontFamily: "Roboto",
        fontSize: 12,
        color: "black",
        width: "20%"
    },
    rowElementBollingText: {
        fontFamily: "Roboto",
        fontSize: 12,
        width: "10%",
        textAlign: "center"
    },
    rowElementBollingEText: {
        fontFamily: "Roboto",
        fontSize: 12,
        width: "9%",
        textAlign: "center"
    },
    Highlights: {
        width: "100%",
        height: 20,
        marginTop: 10
    },
    RunPerBall: {
        width: "100%",
        height: 40,
        paddingVertical: 8,
        borderTopWidth: 1,
        borderTopColor: "#dcdcdc"
    },
    Circle: {
        marginLeft: 6.6,
        borderWidth: 1,
        borderColor: "rgba(0,0,0,0.2)",
        alignItems: "center",
        justifyContent: "center",
        width: 25,
        height: 25,
        backgroundColor: "#332e2c",
        borderRadius: 100
    },
    CircleText: {
        fontFamily: "Roboto-Light",
        fontSize: 15,
        textAlign: "center",
        color: "white"
    },
    wrapper: {},
    slide1: {
        flex: 1
    },
    slide2: {
        flex: 1
    },
    slide3: {
        flex: 1
    },
    text: {
        color: "#fff",
        fontSize: 30
    },
    headertext: {
        fontSize: 13,
        fontFamily: "Montserrat-Medium",
        color: "#ffffff",
        padding: 8,
        paddingLeft: 9
    },
    headerwearetigers: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 2,
        paddingLeft: 8,
        flex: 1
    },

    headerRightPanel: {
        flexDirection: "row",
        justifyContent: "space-between",
        flex: 1,
        padding: 3
    },
    headerLeftPanel: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 6,
        flex: 1
    },
    cardbox: {
        backgroundColor: "#cecece"
    },
    swiperbox: {
        flex: 1,
        width: "100%",
        backgroundColor: "white",
        borderBottomWidth: 6,
        borderBottomColor: "#dcdcdc"
        // flex: 1,
        // width: "100%",
        // marginBottom: 4
    },
    slide: { flex: 1 },
    slidestrech: { flex: 1 },
    labelview: {
        flexDirection: "row",
        alignItems: "flex-end",
        justifyContent: "space-between",
        padding: 5,
        backgroundColor: "#ffffff"
    },

    seeAll: {
        color: "#36be6b",
        flexDirection: "column",
        paddingRight: 10,
        fontFamily: "Roboto-Medium"
    },
    lableviewtittle: {
        // fontSize :16,
        fontFamily: "Roboto-Medium",
        // fontWeight="bold",
        //  Color="#4a4a4a",
        fontSize: 16,
        paddingLeft: 12,
        paddingTop: 4
    },
    flagright: {
        paddingTop: 27,
        paddingLeft: "6%",
        //  paddingBottom:5,
        borderBottomColor: "#36be6b",
        borderBottomWidth: 5,
        flexDirection: "row-reverse",
        margin: 0
        //backgroundColor: "green"
    },
    logobetweencountries: {
        // flexDirection:'column',
        position: "relative"
    },
    flagleft: {
        paddingTop: 27,
        paddingLeft: "6%",
        borderBottomColor: "#36be6b",
        borderBottomWidth: 5,
        flexDirection: "row",
        margin: 0
        //backgroundColor: "red"
    },
    leftcountryname: {
        fontSize: 28,
        paddingLeft: 2,
        // paddingTop: 10,
        color: "#2b2b2b",
        alignItems: "flex-end",
        fontFamily: "BigNoodleTitling",
        bottom: 5
        //backgroundColor: "blue"
    },
    rightcountryname: {
        fontSize: 28,
        paddingRight: 2,
        // paddingTop: 10,
        color: "#2b2b2b",
        alignItems: "flex-start",
        fontFamily: "BigNoodleTitling",
        bottom: 5
        //backgroundColor: "purple"
    },
    scoresleft: {
        fontSize: 17,
        textAlign: "right",
        paddingTop: 8,
        fontFamily: "Montserrat",
        color: "#2b2b2b"
        //backgroundColor: "yellow"
    },
    scoresright: {
        fontSize: 17,
        textAlign: "left",
        paddingTop: 8,
        fontFamily: "Montserrat",
        color: "#2b2b2b"
        //backgroundColor: "orange"
    },
    countryelement: {
        width: "42%",
        flexDirection: "column"
    },
    slideBackgroundimage: {
        flex: 1
    },
    insideslidetoprow: {
        flex: 1,
        flexDirection: "row",
        justifyContent: "space-between",
        paddingBottom: 10,
        alignItems: "center",
        marginTop: 10
    },
    insideslidebottomrow: {
        flex: 1,
        justifyContent: "flex-start",
        padding: 0,
        marginTop: 5,
        backgroundColor: "red"
    },
    textpartBottomrow: {
        flex: 1,
        flexDirection: "column",
        //justifyContent: "center",
        //alignItems: "center",
        height: "100%",
        width: "100%",
        paddingLeft: 13,
        paddingRight: 13,
        paddingTop: 0,
        paddingBottom: 15
        //backgroundColor: "blue"
    },
    textinsidebottonrow: {
        marginTop: 5,
        fontFamily: "Roboto",
        fontSize: 15,
        textAlign: "center",
        color: "#9b9b9b"
    },
    footertabtext: {
        fontFamily: "SansSerifMedium"
    },

    infoRowTop: {
        flexDirection: "row",
        paddingLeft: 13,
        marginTop: 8,
        marginBottom: 8,
        //backgroundColor: "#f7f7f7"
    },
    info: {
        flexDirection: "row",
        alignItems: "flex-start",
        padding: 3,
        paddingHorizontal: 6,
        //backgroundColor: "blue",
        //backgroundColor: "#f7f7f7"
    },
    infoLabel: {
        fontFamily: "Roboto",
        fontSize: 13,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "black"
    },
    infoRightLabel: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "black",
        width: "80%",
        fontWeight: "bold"
    },
    lastBallText: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "black"
    },
    infoLeftLabel: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "black",
        fontWeight: "bold"
    },
    infoRightLabel2: {
        fontFamily: "Roboto",
        fontSize: 13,
        color: "black"
    },
    infoLabelvalue: {
        fontFamily: "Roboto",
        fontSize: 13
    },
    rowElementBollingETextbold: {
        fontFamily: "Roboto",
        fontSize: 12,
        color: "black",
        width: "9%",
        textAlign: "center"
    },
    matchinfoview: {
        flexDirection: "row",
        flexWrap: "wrap",
        //  ailgnItems:'center',
        // height:'9%',
        alignContent: "space-between",
        paddingHorizontal: 12,
        alignItems: "center",
        borderWidth: 1,
        borderTopColor: "#f7f7f7",
        borderBottomColor: "#f7f7f7",
        backgroundColor: "white"
        // paddingVertical:4 ,
        // backgroundColor:'green'
    },
    matchinfoLabel: {
        width: "34%",
        fontFamily: "Roboto",
        fontSize: 13,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",
        color: "black",
        paddingLeft: 10
        // backgroundColor:'green'
    },
    matchinfoLabelvalue: {
        width: "100%",
        marginLeft: 8,
        fontFamily: "Roboto",
        fontSize: 13,
        fontWeight: "normal",
        fontStyle: "normal",
        letterSpacing: 0,
        textAlign: "left",

        //   backgroundColor:'red',
        padding: 2
    },
    matchlblinfocontainer: {
        flexDirection: "row",
        // flexWrap: 'wrap',
        alignContent: "flex-start",
        alignItems: "center",
        paddingHorizontal: 4,
        width: "65%",
        paddingVertical: 4,
        height: "100%",
        // backgroundColor:'red',
        borderLeftWidth: 1,
        borderLeftColor: "#f7f7f7"
    },
    flagimagesmall: {
        width: 30,
        height: 15,
        paddingVertical: 4,
        marginLeft: 8,
        paddingHorizontal: 2
    },
    rowElementBollingTextbold: {
        fontFamily: "Roboto",
        fontSize: 12,
        color: "black",
        width: "10%",
        textAlign: "center"
    },
});

export default class Summary extends Component {
    static propTypes = {
        nav: PropTypes.object.isRequired
    };
    constructor(props) {
        super(props);
        this.state = {
            matchObject: {},
            loading: true,
            teamABallingInnings1: [],
            teamABallingInnings2: [],
            teamABattingInnings1: [],
            teamABattingInnings2: [],
            teamBBallingInnings1: [],
            teamBBallingInnings2: [],
            teamBBattingInnings1: [],
            teamBBattingInnings2: [],
            recentBatsman: [],
            recentBowler: [],
            topBanner: "",
            bottomBanner: ""
        };
    }
    // componentWillMount() {
    //   StatusBar.setHidden(false);
    //   StatusBar.setBackgroundColor("#36be6b");
    // }
    componentDidMount() {
        //alert(JSON.stringify(this.props.nav.state.params.key));
        AsyncStorage.getItem("user")
            .then(response => {
                //alert(JSON.stringify(response))
                const x = JSON.parse(response);
                this.setState({}, () => {
                    this.axiosGetRecentMatchDetails(
                        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
                            x.access_token
                            }&client_id=android-client&client_secret=android-secret&matchKey=${
                            this.props.nav.state.params.key
                            }`
                    );
                });
                axios
                    .get(
                        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
                            x.access_token
                            }&client_id=android-client&client_secret=android-secret&pageNumber=2&position=1`
                    )
                    .then(banner => {
                        console.log(banner);
                        this.setState({
                            topBanner: banner.data.image.url
                        });
                    });
                setInterval(() => {
                    console.log("fetching latest match summary info");
                    this.axiosGetRecentMatchDetails(
                        `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
                            x.access_token
                            }&client_id=android-client&client_secret=android-secret&matchKey=${
                            this.props.nav.state.params.key
                            }`
                    );
                }, 10000);
            })
            .catch(() => {
                console.log("device might have lack of permission.")
            });
    }
    axiosGetRecentMatchDetails = async urlvariable => {
        axios
            .get(urlvariable)
            .then(response => {
                console.log(response.data)
                // ................. TEAM A ...............
                const teamABallingInnings1 = [];
                const teamABallingInnings2 = [];
                const teamABattingInnings1 = [];
                const teamABattingInnings2 = [];
                const teamBBallingInnings1 = [];
                const teamBBallingInnings2 = [];
                const teamBBattingInnings1 = [];
                const teamBBattingInnings2 = [];
                const recentBatsman = [];
                const recentBowler = [];


                let indexRecentBat =1;
                for(let i=0;i<2;i++)
                {
                    const newBatsman ={
                        name : response.data[`latest_batsman_${[indexRecentBat]}_name`],
                        run : response.data[`latest_batsman_${[indexRecentBat]}_runs`],
                        balls : response.data[`latest_batsman_${[indexRecentBat]}_balls`],
                        fours : response.data[`latest_batsman_${[indexRecentBat]}_fours`],
                        sixes : response.data[`latest_batsman_${[indexRecentBat]}_sixes`],
                        strikerate : response.data[`latest_batsman_${[indexRecentBat]}_strike_rate`]
                    }
                    if(newBatsman.name !== null)
                    {
                        recentBatsman.push(newBatsman);
                    }
                    indexRecentBat++;
                }


                let indexRecentBowl = 1;
                for(let i=0;i<2;i++)
                {
                    const newBowler ={
                        name:  response.data[`latest_bowler_${[indexRecentBowl]}_name`],
                        over : response.data[`latest_bowler_${[indexRecentBowl]}_over`],
                        maiden: response.data[`latest_bowler_${[indexRecentBowl]}_maiden`],
                        run : response.data[`latest_bowler_${[indexRecentBowl]}_run`],
                        wicket :response.data[`latest_bowler_${[indexRecentBowl]}_wicket`],
                        eco :response.data[`latest_bowler_${[indexRecentBowl]}_eco`]
                    }
                    if(newBowler.name !== null)
                    {
                        recentBowler.push(newBowler);
                    }
                    indexRecentBowl++;
                }

                this.setState({
                    recentBatsman,
                    recentBowler,
                    loading: false
                });



                let indexTABI1 = 1;
                /* for (let i = 0; i < 99; i += 9) {*/
                for (let i = 0; i < 9; i++){
                    const individualPlayers = {
                        balls:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_balls`
                                ],
                        overs:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_overs`
                                ],
                        dots:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_dots`
                                ],
                        economy:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_economy`
                                ],
                        extras:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_extras`
                                ],
                        maiden_overs:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_maiden_overs`
                                ],
                        name:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_name`
                                ],
                        runs:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_runs`
                                ],
                        wickets:
                            response.data[
                                `bowling_a_1_player_${[indexTABI1]}_wickets`
                                ]
                    };
                    teamABallingInnings1.push(individualPlayers);
                    indexTABI1++;
                }

                let indexTABI2 = 1;
                /*for (let i = 99; i < 198; i += 9) {*/
                for (let i = 0; i < 9; i++) {
                    const individualPlayers = {
                        balls:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_balls`
                                ],
                        overs:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_overs`
                                ],
                        dots:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_dots`
                                ],
                        economy:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_economy`
                                ],
                        extras:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_extras`
                                ],
                        maiden_overs:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_maiden_overs`
                                ],
                        name:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_name`
                                ],
                        runs:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_runs`
                                ],
                        wickets:
                            response.data[
                                `bowling_a_2_player_${[indexTABI2]}_wickets`
                                ]
                    };
                    teamABallingInnings2.push(individualPlayers);
                    indexTABI2++;
                }

                let indexTABatI1 = 1;
                /*for (let i = 396; i < 539; i += 13) {*/
                for (let i = 0; i < 11; i++) {
                    const individualPlayerinn1 = {
                        name:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_full_name`
                                ],
                        balls:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_balls`
                                ],
                        fours:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_fours`
                                ],
                        comment:
                            response.data[
                                `innings_A_1_batting_player_${[
                                    indexTABatI1
                                ]}_innings_1_out_comment`
                                ],
                        runs:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_runs`
                                ],
                        sixes:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_sixes`
                                ],
                        sr:
                            response.data[
                                `innings_A_1_batting_player_${[
                                    indexTABatI1
                                ]}_innings_1_strike_rate`
                                ]
                    };
                    const individualPlayerinn2 = {
                        name:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_full_name`
                                ],
                        balls:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_balls`
                                ],
                        fours:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_fours`
                                ],
                        comment:
                            response.data[
                                `innings_A_1_batting_player_${[
                                    indexTABatI1
                                ]}_innings_2_out_comment`
                                ],
                        runs:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_runs`
                                ],
                        sixes:
                            response.data[
                                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_sixes`
                                ],
                        sr:
                            response.data[
                                `innings_A_1_batting_player_${[
                                    indexTABatI1
                                ]}__innings_2_strike_rate`
                                ]
                    };
                    teamABattingInnings1.push(individualPlayerinn1);
                    teamABattingInnings2.push(individualPlayerinn2);
                    indexTABatI1++;
                }
                this.setState({
                    teamABallingInnings1,
                    teamABallingInnings2,
                    teamABattingInnings1,
                    teamABattingInnings2,
                    loading: false
                });

                // ................. TEAM B ...............

                let indexTABIB1 = 1;
                /* for (let i = 198; i < 297; i += 9) {*/
                for (let i = 0; i < 9; i++) {
                    const individualPlayers = {
                        balls: response.data[`bowling_b_1_player_${[indexTABIB1]}_balls`],
                        overs: response.data[`bowling_b_1_player_${[indexTABIB1]}_overs`],
                        dots: response.data[`bowling_b_1_player_${[indexTABIB1]}_dots`],
                        economy:
                            response.data[`bowling_b_1_player_${[indexTABIB1]}_economy`],
                        extras: response.data[`bowling_b_1_player_${[indexTABIB1]}_extras`],
                        maiden_overs:
                            response.data[`bowling_b_1_player_${[indexTABIB1]}_maiden_overs`],
                        name: response.data[`bowling_b_1_player_${[indexTABIB1]}_name`],
                        runs: response.data[`bowling_b_1_player_${[indexTABIB1]}_runs`],
                        wickets:
                            response.data[`bowling_b_1_player_${[indexTABIB1]}_wickets`]
                    };
                    teamBBallingInnings1.push(individualPlayers);
                    indexTABIB1++;
                }

                let indexTABIB2 = 1;
                /* for (let i = 297; i < 396; i += 9) {*/
                for (let i = 0; i < 9; i++) {
                    const individualPlayers = {
                        balls: response.data[`bowling_b_2_player_${[indexTABIB2]}_balls`],
                        overs: response.data[`bowling_b_2_player_${[indexTABIB2]}_overs`],
                        dots: response.data[`bowling_b_2_player_${[indexTABIB2]}_dots`],
                        economy:
                            response.data[`bowling_b_2_player_${[indexTABIB2]}_economy`],
                        extras: response.data[`bowling_b_2_player_${[indexTABIB2]}_extras`],
                        maiden_overs:
                            response.data[`bowling_b_2_player_${[indexTABIB2]}_maiden_overs`],
                        name: response.data[`bowling_b_2_player_${[indexTABIB2]}_name`],
                        runs: response.data[`bowling_b_2_player_${[indexTABIB2]}_runs`],
                        wickets:
                            response.data[`bowling_b_2_player_${[indexTABIB2]}_wickets`]
                    };
                    teamBBallingInnings2.push(individualPlayers);
                    indexTABIB2++;
                }

                let indexTABatIB1 = 1;
                /*for (let i = 577; i < 720; i += 13) {*/
                for (let i = 0; i < 11; i++) {
                    const individualPlayerinn1B = {
                        name:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_full_name`
                                ],
                        balls:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_balls`
                                ],
                        fours:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_fours`
                                ],
                        comment:
                            response.data[
                                `innings_B_1_batting_player_${[
                                    indexTABatI1
                                ]}_innings_1_out_comment`
                                ],
                        runs:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_runs`
                                ],
                        sixes:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_sixes`
                                ],
                        sr:
                            response.data[
                                `innings_B_1_batting_player_${[
                                    indexTABatIB1
                                ]}_innings_1_strike_rate`
                                ]
                    };
                    const individualPlayerinn2B = {
                        name:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_full_name`
                                ],
                        balls:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_balls`
                                ],
                        fours:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_fours`
                                ],
                        comment:
                            response.data[
                                `innings_B_1_batting_player_${[
                                    indexTABatIB1
                                ]}_innings_2_out_comment`
                                ],
                        runs:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_runs`
                                ],
                        sixes:
                            response.data[
                                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_sixes`
                                ],
                        sr:
                            response.data[
                                `innings_B_1_batting_player_${[
                                    indexTABatIB1
                                ]}__innings_2_strike_rate`
                                ]
                    };
                    teamBBattingInnings1.push(individualPlayerinn1B);
                    teamBBattingInnings2.push(individualPlayerinn2B);
                    indexTABatIB1++;
                }
                this.setState({
                    teamBBallingInnings1,
                    teamBBallingInnings2,
                    teamBBattingInnings1,
                    teamBBattingInnings2,
                    loading: false
                });

                console.log(this.state.teamABallingInnings1);
                console.log(this.state.teamBBallingInnings1);

                console.log(this.state.teamABattingInnings1);
                console.log(this.state.teamABattingInnings2);
                console.log(this.state.teamBBattingInnings1);
                console.log(this.state.teamBBattingInnings2);
                this.setState({
                    matchObject: response.data,
                    loading: false
                });
                //alert(JSON.stringify(this.state.matchObject.man_of_match));
            })
            .catch(error => {
                if (error.response.status === 401) {
                    getuserdetails()
                        .then(res => {
                            this.setState({}, () => {
                                this.axiosGetRecentMatchDetails(
                                    `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
                                        res.access_token
                                        }&client_id=android-client&client_secret=android-secret&matchKey=iplt20_2018_final`
                                );
                            });
                        })
                        .catch(() => {
                            this.setState({
                                loading: false
                            });
                            Alert.alert(
                                "you are being logged out for unavilability, Please log in again!"
                            );
                            this.props.nav.navigate("LoginPage");
                        });
                } else {
                    this.setState({
                        loading: false
                    });
                    Alert.alert("internal Error");
                }
            });
        // console.log(this.state.matchObject)
    };

    render() {
        if (this.state.loading) {
            return (
                <View
                    style={{
                        flex: 1,
                        backgroundColor: "white",
                        alignItems: "center",
                        justifyContent: "center"
                    }}
                >
                    <ActivityIndicator size="large" color="#rgb(54,190,107)" />
                </View>
            );
        }
        return (
            <View style={{ flex: 1 }}>
                <ScrollView>
                    <View>
                        <View style={styles.swiperbox}>
                            <View style={styles.insideslidetoprow}>
                                <View style={styles.countryelement}>
                                    <View style={styles.flagleft}>
                                        <Image
                                            style={{ height: 29, width: 41 }}
                                            source={{ uri: this.state.matchObject.team_a_url }}
                                        />
                                        <Text style={styles.leftcountryname}>
                                            {this.state.matchObject.team_a}
                                        </Text>
                                    </View>
                                    <Text style={styles.scoresleft}>
                                        {this.state.matchObject.innings_A_1_runs}/{
                                        this.state.matchObject.innings_A_1_wickets
                                    }
                                    </Text>
                                    {this.state.matchObject.format === "test" ? (
                                        <Text style={styles.scoresleft}>
                                            {this.state.matchObject.innings_A_2_runs}/{
                                            this.state.matchObject.innings_A_2_wickets
                                        }
                                        </Text>
                                    ) : null}
                                </View>
                                <View style={styles.logobetweencountries}>
                                    <Image
                                        style={{ height: 40, width: 46 }}
                                        source={SeriesIcon}
                                    />
                                </View>
                                <View style={styles.countryelement}>
                                    <View style={styles.flagright}>
                                        <Image
                                            style={{ height: 29, width: 41 }}
                                            source={{ uri: this.state.matchObject.team_b_url }}
                                        />
                                        <Text style={styles.rightcountryname}>
                                            {this.state.matchObject.team_b}
                                        </Text>
                                    </View>
                                    <Text style={styles.scoresright}>
                                        {this.state.matchObject.innings_B_1_runs}/{
                                        this.state.matchObject.innings_B_1_wickets
                                    }
                                    </Text>
                                    {this.state.matchObject.format === "test" ? (
                                        <Text style={styles.scoresright}>
                                            {this.state.matchObject.innings_B_2_runs}/{
                                            this.state.matchObject.innings_B_2_wickets
                                        }
                                        </Text>
                                    ) : null}
                                </View>
                            </View>

                            <View style={styles.textpartBottomrow}>
                                <Text style={styles.textinsidebottonrow}>
                                    {this.state.matchObject.name}
                                </Text>
                                <Text
                                    style={{
                                        fontSize: 17,
                                        fontFamily: "Roboto",
                                        fontWeight: "normal",
                                        textAlign: "center",
                                        color: "#000000"
                                    }}
                                >
                                    {this.state.matchObject.msgs_result}
                                </Text>
                                <Text style={styles.textinsidebottonrow}>
                                    {/* {this.state.matchObject.related_name}, */}
                                    {this.state.matchObject.venue}
                                </Text>
                            </View>
                        </View>
                    </View>




                  {/*  <View>*/}






                        {this.state.matchObject.status === "3" ?
                            /*this block for showing live summury */
                            (<View>
                                    <View style={styles.infoRowTop}>
                                <Text style={styles.infoRightLabel}>
                                    {this.state.matchObject.current_batting_team}
                                </Text>
                                <Text style={styles.infoLeftLabel}>
                                    CRR: {""}
                                    <Text style={styles.infoLabelvalue}>
                                        {this.state.matchObject.current_runrate}
                                    </Text>
                                </Text>
                            </View>

                            <View
                            style={{
                            flexDirection: "row",
                            paddingTop: 6,
                            paddingBottom: 6,
                            borderTopWidth: 1,
                            borderColor: "#dcdcdc"
                        }}
                            >
                            <Text style={styles.Row11bold}>BATSMEN</Text>
                            <Text style={styles.rowElementTextbold}>R</Text>
                            <Text style={styles.rowElementTextbold}>B</Text>
                            <Text style={styles.rowElementTextbold}>4S</Text>
                            <Text style={styles.rowElementTextbold}>6S</Text>
                            <Text style={styles.rowElementTextbold}>SR</Text>
                            </View>

                                    {this.state.recentBatsman.map((stat, index) => (
                                        <View>
                                             <View
                                                style={{
                                                    flexDirection: "row",
                                                    paddingTop: 6,
                                                    paddingBottom: 6,
                                                    borderTopWidth: 1,
                                                    borderColor: "#dcdcdc"
                                                }}
                                            >
                                                <Text style={styles.Row11}>{stat.name}</Text>
                                                <Text style={styles.rowElementText}>{stat.run}</Text>
                                                <Text style={styles.rowElementText}>{stat.balls}</Text>
                                                <Text style={styles.rowElementText}>{stat.fours}</Text>
                                                <Text style={styles.rowElementText}>{stat.sixes}</Text>
                                                <Text style={styles.rowElementText}>{stat.strikerate}</Text>
                                            </View>
                                        </View>
                                    ))}

                                    <View
                                        style={{
                                            flexDirection: "row",
                                            paddingTop: 6,
                                            paddingBottom: 6,
                                            borderTopWidth: 1,
                                            borderColor: "#dcdcdc"
                                        }}
                                    >
                                        <Text style={styles.Row11bold}>BOWLERS</Text>
                                        <Text style={styles.rowElementBollingTextbold}>O</Text>
                                        <Text style={styles.rowElementBollingTextbold}>M</Text>
                                        <Text style={styles.rowElementBollingTextbold}>R</Text>
                                        <Text style={styles.rowElementBollingTextbold}>W</Text>
                                        <Text style={styles.rowElementBollingETextbold}>E</Text>
                                       {/* <Text style={styles.rowElementBollingTextbold}>Ext</Text>*/}
                                    </View>

                                    {this.state.recentBowler.map((stat, index) => (
                                        <View>
                                            <View>
                                                <View
                                                    style={{
                                                        flexDirection: "row",
                                                        paddingTop: 6,
                                                        paddingEnd: 6,
                                                        borderTopWidth: 1,
                                                        borderColor: "#dcdcdc"
                                                    }}
                                                >
                                                    <Text style={styles.Row11}>{stat.name}</Text>
                                                    <Text style={styles.rowElementBollingText}>{stat.over}</Text>
                                                    <Text style={styles.rowElementBollingText}>{stat.maiden}</Text>
                                                    <Text style={styles.rowElementBollingText}>{stat.run}</Text>
                                                    <Text style={styles.rowElementBollingText}>{stat.wicket}</Text>
                                                    <Text style={styles.rowElementBollingEText}>{stat.eco}</Text>
                                                    {/*<Text style={styles.rowElementBollingText}>{stat.extras}</Text>*/}
                                                </View>
                                            </View>
                                        </View>
                                    ))}
                                </View>
                            ):(
                            /*this block for showing summury of completed match*/
                            <View>



                                <View style={styles.infoRowTop}>
                                    <Text style={styles.infoRightLabel}>
                                        {this.state.matchObject.team_a_full_name}
                                    </Text>
                                    <Text style={styles.infoLeftLabel}>
                                        CRR: {""}
                                        <Text style={styles.infoLabelvalue}>
                                            {` ${this.state.matchObject.innings_A_1_run_rate}`}
                                        </Text>
                                    </Text>
                                </View>

                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingTop: 6,
                                        paddingBottom: 6,
                                        borderTopWidth: 1,
                                        borderColor: "#dcdcdc"
                                    }}
                                >
                                    <Text style={styles.Row11bold}>BATSMEN</Text>
                                    <Text style={styles.rowElementTextbold}>R</Text>
                                    <Text style={styles.rowElementTextbold}>B</Text>
                                    <Text style={styles.rowElementTextbold}>4S</Text>
                                    <Text style={styles.rowElementTextbold}>6S</Text>
                                    <Text style={styles.rowElementTextbold}>SR</Text>
                                </View>

                                {this.state.teamABattingInnings1.sort((a, b) => b.runs - a.runs).map((stat, index) => (
                                    <View>
                                        {index <2 ? <View
                                            style={{
                                                flexDirection: "row",
                                                paddingTop: 6,
                                                paddingBottom: 6,
                                                borderTopWidth: 1,
                                                borderColor: "#dcdcdc"
                                            }}
                                        >
                                            <Text style={styles.Row11}>{stat.name}</Text>
                                            <Text style={styles.rowElementText}>{stat.runs}</Text>
                                            <Text style={styles.rowElementText}>{stat.balls}</Text>
                                            <Text style={styles.rowElementText}>{stat.fours}</Text>
                                            <Text style={styles.rowElementText}>{stat.sixes}</Text>
                                            <Text style={styles.rowElementText}>{stat.sr}</Text>
                                        </View>: null}

                                    </View>
                                ))}

                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingTop: 6,
                                        paddingBottom: 6,
                                        borderTopWidth: 1,
                                        borderColor: "#dcdcdc"
                                    }}
                                >
                                    <Text style={styles.Row11bold}>BOWLERS</Text>
                                    <Text style={styles.rowElementBollingTextbold}>O</Text>
                                    <Text style={styles.rowElementBollingTextbold}>M</Text>
                                    <Text style={styles.rowElementBollingTextbold}>R</Text>
                                    <Text style={styles.rowElementBollingTextbold}>W</Text>
                                    <Text style={styles.rowElementBollingETextbold}>E</Text>
                                    {/*<Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
                                    <Text style={styles.rowElementBollingTextbold}>Ext</Text>
                                </View>

                                {this.state.teamBBallingInnings1.sort((a, b) => b.wickets - a.wickets).map((stat, index) => (
                                    <View>
                                        {/*{stat.balls !== null ? (*/}
                                        <View>
                                            {index < 2 ? <View
                                                style={{
                                                    flexDirection: "row",
                                                    paddingTop: 6,
                                                    paddingEnd: 6,
                                                    borderTopWidth: 1,
                                                    borderColor: "#dcdcdc"
                                                }}
                                            >
                                                <Text style={styles.Row11}>{stat.name}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.maiden_overs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
                                                <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
                                                {/*<Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
                                                <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
                                            </View>: null }

                                        </View>
                                        {/* ) : null}*/}
                                    </View>
                                ))}

                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingTop: 6,
                                        paddingBottom: 6,
                                        borderTopWidth: 1,
                                        borderBottomWidth: 6,
                                        borderColor: "#dcdcdc"
                                    }}
                                >
                                    {this.state.topBanner !== "" ? (
                                        <View
                                            style={{
                                                width: "100%",
                                                backgroundColor: "white",
                                                // flex: 1,
                                                paddingBottom: 2,
                                                paddingTop: 2,
                                                // paddingLeft: 15,
                                                // paddingRight: 15,
                                                flexDirection: "row",
                                                justifyContent: "center"
                                            }}
                                        >
                                            <Image
                                                style={{ width: "80%", height: 60, resizeMode: "cover" }}
                                                source={{
                                                    uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
                                                }}
                                            />
                                        </View>
                                    ) : null}
                                    <Text style={styles.Row11bold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                    <Text style={styles.rowElementBollingETextbold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                    <Text style={styles.rowElementBollingTextbold}></Text>
                                </View>
                                <View style={styles.infoRowTop}>
                                    <Text style={styles.infoRightLabel}>
                                        {this.state.matchObject.team_b_full_name}
                                    </Text>
                                    <Text style={styles.infoLeftLabel}>
                                        CRR:
                                        <Text style={styles.infoLabelvalue}>
                                            {` ${this.state.matchObject.innings_B_1_run_rate}`}
                                        </Text>

                                    </Text>
                                </View>
                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingTop: 6,
                                        paddingBottom: 6,
                                        borderTopWidth: 1,
                                        borderColor: "#dcdcdc"
                                    }}
                                >
                                    <Text style={styles.Row11bold}>BATSMEN</Text>
                                    <Text style={styles.rowElementTextbold}>R</Text>
                                    <Text style={styles.rowElementTextbold}>B</Text>
                                    <Text style={styles.rowElementTextbold}>4S</Text>
                                    <Text style={styles.rowElementTextbold}>6S</Text>
                                    <Text style={styles.rowElementTextbold}>SR</Text>
                                </View>
                                {this.state.teamBBattingInnings1.sort((a, b) => b.runs - a.runs).map((stat, index) => (
                                    <View>
                                        {index <2 ? <View
                                            style={{
                                                flexDirection: "row",
                                                paddingTop: 6,
                                                paddingBottom: 6,
                                                borderTopWidth: 1,
                                                borderColor: "#dcdcdc"
                                            }}
                                        >
                                            <Text style={styles.Row11}>{stat.name}</Text>
                                            <Text style={styles.rowElementText}>{stat.runs}</Text>
                                            <Text style={styles.rowElementText}>{stat.balls}</Text>
                                            <Text style={styles.rowElementText}>{stat.fours}</Text>
                                            <Text style={styles.rowElementText}>{stat.sixes}</Text>
                                            <Text style={styles.rowElementText}>{stat.sr}</Text>
                                        </View>: null}

                                    </View>
                                ))}

                                <View
                                    style={{
                                        flexDirection: "row",
                                        paddingTop: 6,
                                        paddingBottom: 6,
                                        borderTopWidth: 1,
                                        borderColor: "#dcdcdc"
                                    }}
                                >
                                    <Text style={styles.Row11bold}>BOWLERS</Text>
                                    <Text style={styles.rowElementBollingTextbold}>O</Text>
                                    <Text style={styles.rowElementBollingTextbold}>M</Text>
                                    <Text style={styles.rowElementBollingTextbold}>R</Text>
                                    <Text style={styles.rowElementBollingTextbold}>W</Text>
                                    <Text style={styles.rowElementBollingETextbold}>E</Text>
                                    {/*<Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
                                    <Text style={styles.rowElementBollingTextbold}>Ext</Text>
                                </View>
                                {this.state.teamABallingInnings1.sort((a, b) => b.wickets - a.wickets).map((stat, index) => (
                                    <View>
                                        {/*{stat.balls !== null ? (*/}
                                        <View>
                                            {index < 2 ? <View
                                                style={{
                                                    flexDirection: "row",
                                                    paddingTop: 6,
                                                    paddingEnd: 6,
                                                    borderTopWidth: 1,
                                                    borderColor: "#dcdcdc"
                                                }}
                                            >
                                                <Text style={styles.Row11}>{stat.name}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.maiden_overs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
                                                <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
                                                <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
                                                {/*<Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
                                                <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
                                            </View>: null }

                                        </View>
                                        {/* ) : null}*/}
                                    </View>
                                ))}





                                    {/*block end for showing summary of completed match*/}
                            </View>
                            )
                        }













                    <View style={{ width: "100%", height: "0.1%", backgroundColor: "#DCDCDC" }} />

                        {this.state.topBanner !== "" ? (
                            <View
                                style={{
                                    width: "100%",
                                    backgroundColor: "white",
                                    // flex: 1,
                                    paddingBottom: "2%",
                                    paddingTop: "2%",
                                    // paddingLeft: 15,
                                    // paddingRight: 15,
                                    flexDirection: "row",
                                    justifyContent: "center"
                                }}
                            >
                                <Image
                                    style={{ width: "80%", height: 60, resizeMode: "cover" }}
                                    source={{
                                        uri: `http://206.189.159.149:8080/abc/${this.state.topBanner}`
                                    }}
                                />
                            </View>
                        ) : null}


                    {/*</View>*/}

                    <View
                        style={{ width: "100%", height: 6, backgroundColor: "#DCDCDC" }}
                    />
                    <View
                        style={{
                            width: "100%",
                            paddingTop: 10,
                            backgroundColor: "#f7f7f7"
                        }}
                    >
                        <Text
                            style={{
                                paddingLeft: 18,
                                fontFamily: "Roboto",
                                fontWeight: "500",
                                color: "#4a4a4a",
                                fontSize: 16,
                                paddingTop: 16,
                                paddingBottom: 8
                            }}
                        >
                            Match Info
                        </Text>
                        <View style={styles.matchinfoview}>
                            <Text style={styles.matchinfoLabel}>Series</Text>
                            {this.state.matchObject.short_name === null ? (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                                </View>
                            ) : (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>
                                        {this.state.matchObject.short_name}
                                    </Text>
                                </View>
                            )}
                        </View>
                        <View style={styles.matchinfoview}>
                            <Text style={styles.matchinfoLabel}>Match Detail</Text>
                            {this.state.matchObject.related_name === null ? (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                                </View>
                            ) : (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>
                                        {this.state.matchObject.related_name}
                                    </Text>
                                </View>
                            )}
                        </View>
                        <View style={styles.matchinfoview}>
                            <Text style={styles.matchinfoLabel}>Toss</Text>
                            {this.state.matchObject.toss_str === null ? (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                                </View>
                            ) : (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>
                                        {this.state.matchObject.toss_str}
                                    </Text>
                                </View>
                            )}
                        </View>


                       {/* <View style={styles.matchinfoview}>
                            <Text style={styles.matchinfoLabel}>Man Of The Match</Text>
                            {this.state.matchObject.man_of_match === null ? (
                                <View style={styles.matchlblinfocontainer}>
                                    <Image style={styles.flagimagesmall} />
                                    <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                                </View>
                            ) : (
                                <View style={styles.matchlblinfocontainer}>
                                    <Image style={styles.flagimagesmall} />
                                    <Text style={styles.matchinfoLabelvalue}>
                                        {this.state.matchObject.man_of_match}
                                    </Text>
                                </View>
                            )}
                        </View>*/}


                        <View style={styles.matchinfoview}>
                            <Text style={styles.matchinfoLabel}>Series Result</Text>
                            {this.state.matchObject.msgs_result === null ? (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                                </View>
                            ) : (
                                <View style={styles.matchlblinfocontainer}>
                                    <Text style={styles.matchinfoLabelvalue}>
                                        {this.state.matchObject.msgs_result}
                                    </Text>
                                </View>
                            )}
                        </View>
                        <View>
                            <Image style={{ width: "100%" }} source={BannerImage} />
                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

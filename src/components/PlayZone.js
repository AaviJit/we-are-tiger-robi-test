import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  View,
  StyleSheet,
  ScrollView,
  Image,
  TouchableOpacity,
  Alert,
  AsyncStorage
} from "react-native";
import Ad from "../resources/img/ad.png";
import Image1 from "../resources/img/group_7.png";
import axios from "axios";

// import PropTypes from 'prop-types';
const styles = StyleSheet.create({
  Image1View: {
    borderWidth: 1,
    borderColor: "#ffffff",
    borderRadius: 2,
    backgroundColor: "#FF0000",
    height: 27,
    width: 80,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 120,
    marginLeft: 50
  },
  Image1Text: {
    // justifyContent: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "rgba(255, 255, 255, 0.87)"
  },
  Image2View: {
    borderWidth: 1,
    borderColor: "#ffffff",
    borderRadius: 2,
    backgroundColor: "#FF0000",
    height: 27,
    width: 80,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 60,
    marginLeft: 220
  },
  Image2Text: {
    // justifyContent: "center",
    fontSize: 14,
    fontWeight: "bold",
    color: "rgba(255, 255, 255, 0.87)"
  }
});
export default class FanZone extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      bottomBanner: ""
    };
  }

  componentWillMount() {
    AsyncStorage.getItem("user").then(response => {
      //alert(JSON.stringify(response))
      const x = JSON.parse(response);
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=11&position=1`
        )
        .then(banner => {
          console.log(banner);
          this.setState({
            bottomBanner: banner.data.image.url
          });
        });
    });
  }
  clearSetintv = () => {
    console.log("clearing...");
    for (let i = 1; i < 99999; i++) {
      clearInterval(i);
    }
  };
  toTigerShop = () => {
    this.clearSetintv();
    this.props.nav.navigate("RobiShop");
  };
  render() {
    return (
      <View
        style={{
          backgroundColor: "#ffffff",
          justifyContent: "center",
          alignItems: "center",
          marginTop: 10
        }}
      >
        <ScrollView>
          <View style={{ flex: 1 }}>
            <Image source={Image1}>
              <TouchableOpacity onPress={this.toTigerShop}>
                <View style={styles.Image1View}>
                  <Text style={styles.Image1Text}>Play Now</Text>
                </View>
              </TouchableOpacity>
            </Image>
          </View>
          <View
            style={{ height: 2, width: "100%", backgroundColor: "#DCDCDC" }}
          />
          <View style={{ flex: 1 }}>
            <Image source={Image1}>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert("Hello!", "This feature is yet to come");
                }}
              >
                <View style={styles.Image2View}>
                  <Text style={styles.Image2Text}>Play Now</Text>
                </View>
              </TouchableOpacity>
            </Image>
          </View>
          <View style={{ flex: 1 }}>
            <Image source={Image1}>
              <TouchableOpacity
                onPress={() => {
                  Alert.alert("Hello!", "This feature is yet to come");
                }}
              >
                <View style={styles.Image1View}>
                  <Text style={styles.Image1Text}>Play Now</Text>
                </View>
              </TouchableOpacity>
            </Image>
          </View>
        </ScrollView>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            paddingTop: 10,
            backgroundColor: "white"
          }}
        >
          <Image
            style={{ width: "80%", height: 60, resizeMode: "cover" }}
            source={{
              uri: `http://206.189.159.149:8080/abc/${this.state.bottomBanner}`
            }}
          />
        </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";
import axios from "axios";
// import Icon from 'react-native-vector-icons/Ionicons';
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import RobiAd from "../resources/icons/robiradd.png";
import SeriesIcon from "../resources/icons/weIcon.png";
import { getuserdetails } from "../components/authorizationandrefreshtoken";

const styles = StyleSheet.create({
  wrapper: {},
  slide1: {
    flex: 1
  },
  slide2: {
    flex: 1
  },
  slide3: {
    flex: 1
  },
  text: {
    color: "#fff",
    fontSize: 30
  },
  headertext: {
    fontSize: 14,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    padding: 8,
    paddingLeft: 9,
    fontWeight: "bold"
  },
  headerwearetigers: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 2,
    paddingLeft: 8,
    flex: 1
  },
  ShopStyle: {
    fontSize: 15,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    paddingLeft: 7
  },
  headerRightPanel: {
    flexDirection: "row",
    justifyContent: "flex-start",
    flex: 1
  },
  headerLeftPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 6,
    flex: 1
    // fontWeight: 'bold'
  },
  cardbox: {
    backgroundColor: "#cecece"
  },
  swiperbox: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
    marginBottom: 4
  },
  swiperbody: { flex: 1 },
  swiperimagecontainer: { flex: 1 },
  swiperslidecontainer: { flex: 1 },
  swiperdottraydotcontainer: {},
  swiperdottray: {},
  swiperdot: {},
  labelview: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    padding: 5,
    backgroundColor: "#ffffff"
  },

  seeAll: {
    color: "#36be6b",
    flexDirection: "column",
    paddingRight: 10,
    fontFamily: "Roboto-Medium"
  },
  lableviewtittle: {
    // fontSize :16,
    fontFamily: "Roboto-Medium",
    // fontWeight="bold",
    //  Color="#4a4a4a",
    fontSize: 16,
    paddingLeft: 12,
    paddingTop: 4
  },
  flagright: {
    width: "100%",
    paddingTop: 27,
    paddingLeft: "6%",
    //  paddingBottom:5,
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row-reverse",
    margin: 0
    //backgroundColor: "grey"
  },
  logobetweencountries: {
    // flexDirection:'column',
    position: "relative"
  },
  flagleft: {
    width: "100%",
    paddingTop: 27,
    paddingLeft: "6%",
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row",
    margin: 0
    //backgroundColor: "blue"
  },
  leftcountryname: {
    fontSize: 28,
    paddingLeft: 7,
    // paddingTop: 10,
    color: "#2b2b2b",
    alignItems: "flex-end",
    fontFamily: "BigNoodleTitling",
    bottom: 5
  },
  rightcountryname: {
    fontSize: 28,
    paddingRight: 7,
    // paddingTop: 10,
    color: "#2b2b2b",
    alignItems: "flex-start",
    fontFamily: "BigNoodleTitling",
    bottom: 5
  },
  scoresleft: {
    flex: 1,
    fontSize: 17,
    paddingLeft: 92,
    paddingTop: 8,
    fontFamily: "Montserrat",
    color: "#2b2b2b"
    //backgroundColor: "blue"
  },
  scoresright: {
    flex: 1,
    fontSize: 17,
    paddingLeft: 0,
    paddingTop: 8,
    fontFamily: "Montserrat",
    color: "#2b2b2b"
    //backgroundColor: "blue"
  },
  countryelement: {
    flexDirection: "column"
    //backgroundColor: "green"
  },
  slideBackgroundimage: {
    flex: 1
  },
  insideslidetoprow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    paddingBottom: 10,
    alignItems: "center",
    marginTop: 10,
    /*paddingLeft: "1%",
    paddingRight: "1%"*/
    // backgroundColor: "black"
  },
  insideslidebottomrow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
    padding: 0,
    marginTop: 5,
    /*paddingLeft: "1%",
    paddingRight: "1%"*/
    //backgroundColor: "red"
  },
  textpartBottomrow: {
    flexDirection: "column",
    justifyContent: "flex-end",
    height: "100%",
    width: "100%",
    paddingLeft: 12,
    paddingTop: 0,
    paddingBottom: 15
    //backgroundColor: "yellow"
  },
  bellbox: {
    height: "100%",
    width: "30%",
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingTop: 80,
    paddingRight: 10
  },
  textinsidebottonrow: {
    fontFamily: "Roboto",
    fontSize: 17,
    justifyContent: "center",
    margin: 1,
    marginBottom: 5,
    color: "#9b9b9b",
      textAlign: "center"
  }
});

export default class Upcoming extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      matchObject: [],
      loading: true,
      topBanner: "",
      bottomBanner: ""
    };
  }
  componentWillMount() {
    // StatusBar.setHidden(false);
    // StatusBar.setBackgroundColor("#36be6b");
  }

  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response))
        const x = JSON.parse(response);
        this.axiosGetRecentMatchDetails(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/upcoming?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret`
        );
        axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=1&position=1`
        )
        .then(banner => {
          console.log(banner);
          this.setState({
            topBanner: banner.data.image.url
          });
        });
      axios
        .get(
          `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
            x.access_token
          }&client_id=android-client&client_secret=android-secret&pageNumber=1&position=2`
        )
        .then(banner => {
          this.setState({
            bottomBanner: banner.data.image.url
          });
        });
      })
      .catch(() => {
        Alert.alert(
          "Cannot connect to internal storage, make sure you have the correct storage rights."
        );
      });
  }
  /*tomatchdetails = (time, name, description) => {
    Alert.alert(`${name}`, `${description}`);
    // this.props.nav.navigate("MatchDetails");
  };*/
  axiosGetRecentMatchDetails = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        this.setState({
          loading: false
        });
        response.data.map(
          match => {
            this.setState({
              matchObject: [...this.state.matchObject, match]
            });
            return null;
          },
          () => {
            this.setState({
              // loadingstate: false
            });
          }
        );
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              this.setState({}, () => {
                this.axiosGetRecentMatchDetails(
                  `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/upcoming?access_token=${
                    res.access_token
                  }&client_id=android-client&client_secret=android-secret`
                );
              });
            })
            .catch(() => {
              this.setState({
                loading: false
              });
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.nav.navigate("LoginPage");
            });
        } else {
          this.setState({
            loading: false
          });
        }
      });
  };
  render() {
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color="#rgb(54,190,107)" />
        </View>
      );
    }
    return (
      <ScrollView>
        {this.state.matchObject.map((match, index) => (
          <View>

              {/*{index<1?*/}


                    <View>
          {/*  <TouchableOpacity
              /*onPress={() =>
                this.tomatchdetails(match.start_date.str, match.short_name, match.description)
              }*/
            /*>*/}
              <View style={styles.swiperbox}>
                <View style={styles.insideslidetoprow}>
                  <View style={styles.countryelement}>
                    <View style={styles.flagleft}>
                      <Image
                        style={{ height: 29, width: 41 }}
                        source={{ uri: match.team_a_url }}
                      />
                      <Text style={styles.leftcountryname}>{match.team_a}</Text>
                    </View>
                  </View>
                  <View style={styles.logobetweencountries}>
                    <Image
                      style={{ height: 40, width: 46 }}
                      source={SeriesIcon}
                    />
                  </View>
                  <View style={styles.countryelement}>
                    <View style={styles.flagright}>
                      <Image
                        style={{ height: 29, width: 41 }}
                        source={{ uri: match.team_b_url }}
                      />
                      <Text style={styles.rightcountryname}>
                        {match.team_b}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={styles.insideslidebottomrow}>
                  <View style={styles.textpartBottomrow}>

                    <Text
                      style={{
                        fontSize: 17,
                        fontFamily: "Roboto",
                        fontWeight: "400",
                        color: "#000000",
                          textAlign: "center"
                      }}
                    >
                        {match.related_name}
                    </Text>
                      <Text style={styles.textinsidebottonrow}>{match.start_date.str}, {match.venue}</Text>
                  </View>
                </View>
              </View>
           {/* </TouchableOpacity>*/}
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                paddingTop: 10,
                backgroundColor: "white"
              }}
            >
              <Image
                style={{ width: "80%", height: 60, resizeMode: "cover" }}
                source={{
                  uri: `http://206.189.159.149:8080/abc/${
                    this.state.topBanner
                  }`
                }}
              />
            </View>
            </View>

              {
                  /*   :*/

                  /*


                             <View>
                             <TouchableOpacity
                               onPress={() =>
                                 this.tomatchdetails(match.start_date.str, match.short_name, match.description)
                               }
                             >
                               <View style={styles.swiperbox}>
                                 <View style={styles.insideslidetoprow}>
                                   <View style={styles.countryelement}>
                                     <View style={styles.flagleft}>
                                       <Image
                                         style={{ height: 29, width: 41 }}
                                         source={{ uri: match.team_a_url }}
                                       />
                                       <Text style={styles.leftcountryname}>{match.team_a}</Text>
                                     </View>
                                   </View>
                                   <View style={styles.logobetweencountries}>
                                     <Image
                                       style={{ height: 40, width: 46 }}
                                       source={SeriesIcon}
                                     />
                                   </View>
                                   <View style={styles.countryelement}>
                                     <View style={styles.flagright}>
                                       <Image
                                         style={{ height: 29, width: 41 }}
                                         source={{ uri: match.team_b_url }}
                                       />
                                       <Text style={styles.rightcountryname}>
                                         {match.team_b}
                                       </Text>
                                     </View>
                                   </View>
                                 </View>
                                 <View style={styles.insideslidebottomrow}>
                                   <View style={styles.textpartBottomrow}>
                                     <Text style={styles.textinsidebottonrow}>{match.name}</Text>
                                     <Text
                                       style={{
                                         fontSize: 17,
                                         fontFamily: "Roboto",
                                         fontWeight: "400",
                                         color: "#000000",
                                           textAlign: "center"
                                       }}
                                     >
                                       {match.start_date.str}
                                     </Text>
                                     <Text style={styles.textinsidebottonrow}>
                                       {match.related_name}, {match.venue}
                                     </Text>
                                   </View>
                                    <View style={styles.bellbox}>
                                     <LineIcon name="bell" size={25} color="green" />
                                   </View>
                                 </View>
                               </View>
                             </TouchableOpacity>
                             </View>


                  */
                  /*  }*/
              }
            <View
              style={{ width: "100%", height: 3, backgroundColor: "#DCDCDC" }}
            />
          </View>
        ))}
        <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                paddingTop: 10,
                backgroundColor: "white"
              }}
            >
              <Image
                style={{ width: "80%", height: 60, resizeMode: "cover" }}
                source={{
                  uri: `http://206.189.159.149:8080/abc/${
                    this.state.bottomBanner
                  }`
                }}
              />
            </View>
      </ScrollView>
    );
  }
}

import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Image,
  StyleSheet,
  Text,
  View,
  ScrollView,
  StatusBar,
  AsyncStorage,
  Alert,
  ActivityIndicator
} from "react-native";
import axios from "axios";
import Accordion from "react-native-collapsible/Accordion";
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import RobiAd from "../resources/icons/robiradd.png";
import { getuserdetails } from "../components/authorizationandrefreshtoken";
//import BdFlag from "../resources/icons/bangladesh.jpg";

//Fall of wickets renderer
const listStringArray = data => {
  const length = data.length;
  return data.map((item, index) => (
    <Text>
      <Text style={{ marginHorizontal: 2 }}>{item}</Text>
      {index != length - 1 ? <Text> | </Text> : null}
    </Text>
  ));
};

const styles = StyleSheet.create({
  Row11: {
    // fontSize: 11,
    //   fontFamily: 'Lato-Regular',
    //   textDecorationLine: 'underline',
    fontFamily: "Roboto",
    fontSize: 13,
    color: "#138b50",
    width: "36%",
    paddingLeft: 13
  },
  Row11bold: {
    fontFamily: "Roboto",
    fontSize: 13,
    color: "black",
    width: "36%",
    paddingLeft: 13
  },
  playersScore: {
    flexDirection: "row",
    justifyContent: "flex-end",
    width: "60%",
    height: "100%",
    // justifyContent: 'center',
    marginRight: 12
    // backgroundColor:'red'
  },
  rowElementContainer: {
    flexDirection: "row",
    marginHorizontal: 4,
    width: "16%",
    justifyContent: "flex-end"
    // backgroundColor:'green',
  },
  rowElementContainerb: {
    flexDirection: "row",
    marginHorizontal: 1,
    width: "11%",
    justifyContent: "flex-end"
    // backgroundColor:'green',
  },
  rowElementContainerbecon: {
    flexDirection: "row",
    marginHorizontal: 1,
    width: "12%",
    justifyContent: "flex-end"
    // backgroundColor:'green',
  },
  scoredisplaycontainer: {
    flex: 1,
    width: "100%",
    backgroundColor: "white",
    borderTopWidth: 1,
    borderTopColor: "#dcdcdc",
    justifyContent: "center"
  },
  scoredisplaycontaineronnocolor: {
    width: "100%",
    height: 38,
    backgroundColor: "#f9f9f9",
    borderTopWidth: 1,
    borderTopColor: "#dcdcdc",
    flexDirection: "column",
    justifyContent: "center"
  },
  rowElementText: {
    fontFamily: "Roboto",
    fontSize: 12,
    width: "12%",
    textAlign: "center"
  },
  rowElementTextbold: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: "black",
    width: "12%",
    textAlign: "center"
  },
  rowElementBollingTextbold: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: "black",
    width: "10%",
    textAlign: "center"
  },
  rowElementBollingETextbold: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: "black",
    width: "9%",
    textAlign: "center"
  },
  rowElementBollingText: {
    fontFamily: "Roboto",
    fontSize: 12,
    width: "10%",
    textAlign: "center"
  },
  rowElementBollingEText: {
    fontFamily: "Roboto",
    fontSize: 12,
    width: "9%",
    textAlign: "center"
  },
  totalText: {
    fontFamily: "Roboto",
    fontSize: 12,
    color: "black",
    width: "38%"
  },
  Highlights: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderBottomColor: "#dcdcdc",
    borderTopColor: "#dcdcdc"
  },
  RunPerBall: {
    width: "100%",
    height: 40,
    paddingVertical: 8,
    borderTopWidth: 1,
    borderTopColor: "#dcdcdc"
  },
  Circle: {
    marginLeft: 6.6,
    borderWidth: 1,
    borderColor: "rgba(0,0,0,0.2)",
    alignItems: "center",
    justifyContent: "center",
    width: 25,
    height: 25,
    backgroundColor: "#332e2c",
    borderRadius: 100
  },
  CircleText: {
    fontFamily: "Roboto-Light",
    fontSize: 15,
    textAlign: "center",
    color: "white"
  },
  wrapper: {},
  slide1: {
    flex: 1
  },
  slide2: {
    flex: 1
  },
  slide3: {
    flex: 1
  },
  text: {
    color: "#fff",
    fontSize: 30
  },
  headertext: {
    fontSize: 13,
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    padding: 8,
    paddingLeft: 9
  },
  headerwearetigers: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 2,
    paddingLeft: 8,
    flex: 1
  },

  headerRightPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    padding: 3
  },
  headerLeftPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 6,
    flex: 1
  },
  cardbox: {
    backgroundColor: "#cecece"
  },
  swiperbox: {
    height: 250,
    width: "100%",
    backgroundColor: "white",
    borderBottomWidth: 6,
    borderBottomColor: "#dcdcdc"
  },
  slide: { flex: 1 },
  slidestrech: { flex: 1 },
  labelview: {
    flexDirection: "row",
    alignItems: "flex-end",
    justifyContent: "space-between",
    padding: 5,
    backgroundColor: "#ffffff"
  },

  seeAll: {
    color: "#36be6b",
    flexDirection: "column",
    paddingRight: 10,
    fontFamily: "Roboto-Medium"
  },
  lableviewtittle: {
    // fontSize :16,
    fontFamily: "Roboto-Medium",
    // fontWeight="bold",
    //  Color="#4a4a4a",
    fontSize: 16,
    paddingLeft: 12,
    paddingTop: 4
  },
  flagright: {
    flex: 1,
    paddingTop: 27,
    paddingLeft: 3,
    //  paddingBottom:5,
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row-reverse",
    margin: 0
  },
  logobetweencountries: {
    // flexDirection:'column',
    position: "relative"
  },
  flagleft: {
    flex: 1,
    paddingTop: 27,
    paddingLeft: 3,
    borderBottomColor: "#36be6b",
    borderBottomWidth: 5,
    flexDirection: "row",
    margin: 0
  },
  leftcountryname: {
    fontSize: 28,
    paddingLeft: 3,
    paddingTop: 10,
    color: "black",
    alignItems: "flex-end",
    fontFamily: "BigNoodleTitling",
    bottom: 8
  },
  rightcountryname: {
    fontSize: 28,
    paddingRight: 3,
    paddingTop: 10,
    color: "black",
    alignItems: "flex-start",
    fontFamily: "BigNoodleTitling",
    bottom: 8
  },
  scoresleft: {
    paddingLeft: 58,
    paddingTop: 5,
    fontFamily: "Montserrat-Medium",
    color: "black"
  },
  scoresright: {
    paddingLeft: 0,
    paddingTop: 5,
    fontFamily: "Montserrat-Medium",
    color: "black"
  },
  countryelement: {
    width: "36%",
    height: "90%",
    flexDirection: "column",
    padding: 0
  },
  slideBackgroundimage: {
    flex: 1
  },
  insideslidetoprow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "space-between",
    padding: 0,
    alignItems: "center"
  },
  insideslidebottomrow: {
    flex: 1,
    flexDirection: "column",
    alignItems: "center",
    padding: 0,
    top: 6
  },
  textinsidebottonrow: {
    padding: 0,
    justifyContent: "center",
    color: "black",
    margin: -2,
    lineHeight: 14
  },
  footertabtext: {
    fontFamily: "SansSerifMedium"
  },

  infoRowTop: {
    flexDirection: "row",
    alignItems: "flex-start",
    marginTop: 5,
    paddingLeft: 13,
    paddingTop: 8,
    paddingBottom: 8
  },
  info: {
    flexDirection: "row",
    alignItems: "flex-start",
    padding: 3,
    paddingHorizontal: 6
  },
  infoLabel: {
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "black"
  },
  infoLabelvalue: {
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    paddingHorizontal: 3
  },
  matchinfoview: {
    flexDirection: "row",
    flexWrap: "wrap",
    //  ailgnItems:'center',
    // height:'9%',
    alignContent: "space-between",
    paddingHorizontal: 12,
    alignItems: "center",
    borderWidth: 1,
    borderTopColor: "#f7f7f7",
    borderBottomColor: "#f7f7f7",
    backgroundColor: "white"
    // paddingVertical:4 ,
    // backgroundColor:'green'
  },
  matchinfoLabel: {
    width: "34%",
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "black",
    paddingLeft: 10
    // backgroundColor:'green'
  },
  matchinfoLabelvalue: {
    width: "100%",
    marginLeft: 8,
    fontFamily: "Roboto",
    fontSize: 13,
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",

    //   backgroundColor:'red',
    padding: 2
  },
  matchlblinfocontainer: {
    flexDirection: "row",
    // flexWrap: 'wrap',
    alignContent: "flex-start",
    alignItems: "center",
    paddingHorizontal: 4,
    width: "65%",
    paddingVertical: 4,
    height: "100%",
    // backgroundColor:'red',
    borderLeftWidth: 1,
    borderLeftColor: "#f7f7f7"
  },
  flagimagesmall: {
    width: 30,
    height: 19,
    marginLeft: 8,
    paddingHorizontal: 6,
    marginTop: 3,
    marginRight: 3
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    width: "100%",
    paddingVertical: 10,
    paddingHorizontal: 18,
      backgroundColor: "#f7f7f7"
  },
  headerTextnew: {
    fontFamily: "Roboto",
    fontSize: 13,
    fontStyle: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: "black",
    fontWeight: "600",
  }
});

const SECTIONS1 = [
  {
    title: "Bangladesh First Innings",
    content: "Lorem ipsum..."
  }
];
const SECTIONS2 = [
  {
    title: "Bangladesh First Innings",
    content: "Lorem ipsum..."
  }
];
export default class Scorecard extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);

    this.state = {
      matchObject: {},
      loading: true,
      teamABallingInnings1: [],
      teamABallingInnings2: [],
      teamABattingInnings1: [],
      teamABattingInnings2: [],
      teamBBallingInnings1: [],
      teamBBallingInnings2: [],
      teamBBattingInnings1: [],
      teamBBattingInnings2: [],
      topBanner: "",
      bottomBanner: ""
      // team_a: "",
      // team_b: ""
    };
  }
  componentWillMount() {
    StatusBar.setHidden(false);
    StatusBar.setBackgroundColor("#36be6b");
  }
  componentDidMount() {
    AsyncStorage.getItem("user")
      .then(response => {
        //alert(JSON.stringify(response.data.innings_A_1_bowling_order));
        const x = JSON.parse(response);
        this.setState({}, () => {
          this.axiosGetRecentMatchDetails(
            `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
              x.access_token
            }&client_id=android-client&client_secret=android-secret&matchKey=${
              this.props.nav.state.params.key
            }`
          );
          axios
            .get(
              `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/banner/specific?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secret&pageNumber=4&position=1`
            )
            .then(banner => {
              console.log(banner);
              this.setState({
                topBanner: banner.data.image.url
              });
            });
          setInterval(() => {
            console.log("fetching latest match scorecard info");
            this.axiosGetRecentMatchDetails(
              `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
                x.access_token
              }&client_id=android-client&client_secret=android-secret&matchKey=${
                this.props.nav.state.params.key
              }`
            );
          }, 10000);
        });
      })
      .catch(() => {
        Alert.alert(
          "Cannot connect to internal storage, make sure you have the correct storage rights."
        );
      });
  }

  axiosGetRecentMatchDetails = async urlvariable => {
    axios
      .get(urlvariable)
      .then(response => {
        // ................. TEAM A ...............
        const teamABallingInnings1 = [];
        const teamABallingInnings2 = [];
        const teamABattingInnings1 = [];
        const teamABattingInnings2 = [];
        const teamBBallingInnings1 = [];
        const teamBBallingInnings2 = [];
        const teamBBattingInnings1 = [];
        const teamBBattingInnings2 = [];
        console.log("TEST TEST", response.data.bowling_a_1_player_1_name);
        let indexTABI1 = 1;

        /*for (let i = 0; i < 99; i += 9) {*/

        for (let i = 0; i < 10; i++) {
          const individualPlayers = {
            balls: response.data[`bowling_a_1_player_${[indexTABI1]}_balls`],
            overs: response.data[`bowling_a_1_player_${[indexTABI1]}_overs`],
            dots: response.data[`bowling_a_1_player_${[indexTABI1]}_dots`],
            economy:
              response.data[`bowling_a_1_player_${[indexTABI1]}_economy`],
            extras: response.data[`bowling_a_1_player_${[indexTABI1]}_extras`],
            maiden_overs:
              response.data[`bowling_a_1_player_${[indexTABI1]}_maiden_overs`],
            name: response.data[`bowling_a_1_player_${[indexTABI1]}_name`],
            runs: response.data[`bowling_a_1_player_${[indexTABI1]}_runs`],
            wickets: response.data[`bowling_a_1_player_${[indexTABI1]}_wickets`]
          };

          if (individualPlayers.name === null) {
            break;
          }

          teamABallingInnings1.push(individualPlayers);
          indexTABI1++;
        }

        let indexTABI2 = 1;
        /*for (let i = 99; i < 198; i += 9) {*/
        for (let i = 0; i < 10; i++) {
          const individualPlayers = {
            balls: response.data[`bowling_a_2_player_${[indexTABI2]}_balls`],
            overs: response.data[`bowling_a_2_player_${[indexTABI2]}_overs`],
            dots: response.data[`bowling_a_2_player_${[indexTABI2]}_dots`],
            economy:
              response.data[`bowling_a_2_player_${[indexTABI2]}_economy`],
            extras: response.data[`bowling_a_2_player_${[indexTABI2]}_extras`],
            maiden_overs:
              response.data[`bowling_a_2_player_${[indexTABI2]}_maiden_overs`],
            name: response.data[`bowling_a_2_player_${[indexTABI2]}_name`],
            runs: response.data[`bowling_a_2_player_${[indexTABI2]}_runs`],
            wickets: response.data[`bowling_a_2_player_${[indexTABI2]}_wickets`]
          };

          if (individualPlayers.name === null) {
            break;
          }

          teamABallingInnings2.push(individualPlayers);
          indexTABI2++;
        }

        let indexTABatI1 = 1;
        /*for (let i = 396; i < 539; i += 13) {*/

        for (let i = 0; i < 11; i++) {
          const individualPlayerinn1 = {
            name:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_full_name`
              ],
            balls:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_balls`
              ],
            fours:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_fours`
              ],
            comment:
              response.data[
                `innings_A_1_batting_player_${[
                  indexTABatI1
                ]}_innings_1_out_comment`
              ],
            runs:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_runs`
              ],
            sixes:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_1_sixes`
              ],
            sr:
              response.data[
                `innings_A_1_batting_player_${[
                  indexTABatI1
                ]}_innings_1_strike_rate`
              ]
          };
          const individualPlayerinn2 = {
            name:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_full_name`
              ],
            balls:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_balls`
              ],
            fours:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_fours`
              ],
            comment:
              response.data[
                `innings_A_1_batting_player_${[
                  indexTABatI1
                ]}_innings_2_out_comment`
              ],
            runs:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_runs`
              ],
            sixes:
              response.data[
                `innings_A_1_batting_player_${[indexTABatI1]}_innings_2_sixes`
              ],
            sr:
              response.data[
                `innings_A_1_batting_player_${[
                  indexTABatI1
                ]}__innings_2_strike_rate`
              ]
          };

     /*     if (
            individualPlayerinn1.name === null ||
            individualPlayerinn2.name === null
          ) {
            break;
          }*/

          if(individualPlayerinn1.name !== null)
          {
              teamABattingInnings1.push(individualPlayerinn1);
          }

         if(individualPlayerinn2.name !== null)
         {
             teamABattingInnings2.push(individualPlayerinn2);
         }

          indexTABatI1++;
        }
        this.setState({
          teamABallingInnings1,
          teamABallingInnings2,
          teamABattingInnings1,
          teamABattingInnings2,
          loading: false
        });

        // ................. TEAM B ...............

        let indexTABIB1 = 1;
        /*for (let i = 198; i < 297; i += 9) {*/
        for (let i = 0; i < 10; i++) {
          const individualPlayers = {
            balls: response.data[`bowling_b_1_player_${[indexTABIB1]}_balls`],
            overs: response.data[`bowling_b_1_player_${[indexTABIB1]}_overs`],
            dots: response.data[`bowling_b_1_player_${[indexTABIB1]}_dots`],
            economy:
              response.data[`bowling_b_1_player_${[indexTABIB1]}_economy`],
            extras: response.data[`bowling_b_1_player_${[indexTABIB1]}_extras`],
            maiden_overs:
              response.data[`bowling_b_1_player_${[indexTABIB1]}_maiden_overs`],
            name: response.data[`bowling_b_1_player_${[indexTABIB1]}_name`],
            runs: response.data[`bowling_b_1_player_${[indexTABIB1]}_runs`],
            wickets:
              response.data[`bowling_b_1_player_${[indexTABIB1]}_wickets`]
          };

          if (individualPlayers.name === null) {
            break;
          }

          teamBBallingInnings1.push(individualPlayers);
          indexTABIB1++;
        }

        let indexTABIB2 = 1;
        /* for (let i = 297; i < 396; i += 9) {*/
        for (let i = 0; i < 10; i++) {
          const individualPlayers = {
            balls: response.data[`bowling_b_2_player_${[indexTABIB2]}_balls`],
            overs: response.data[`bowling_b_2_player_${[indexTABIB2]}_overs`],
            dots: response.data[`bowling_b_2_player_${[indexTABIB2]}_dots`],
            economy:
              response.data[`bowling_b_2_player_${[indexTABIB2]}_economy`],
            extras: response.data[`bowling_b_2_player_${[indexTABIB2]}_extras`],
            maiden_overs:
              response.data[`bowling_b_2_player_${[indexTABIB2]}_maiden_overs`],
            name: response.data[`bowling_b_2_player_${[indexTABIB2]}_name`],
            runs: response.data[`bowling_b_2_player_${[indexTABIB2]}_runs`],
            wickets:
              response.data[`bowling_b_2_player_${[indexTABIB2]}_wickets`]
          };

          if (individualPlayers.name === null) {
            break;
          }

          teamBBallingInnings2.push(individualPlayers);
          indexTABIB2++;
        }

        let indexTABatIB1 = 1;
        /*for (let i = 577; i < 720; i += 13) {*/

        for (let i = 0; i < 11; i++) {
          const individualPlayerinn1B = {
            name:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_full_name`
              ],
            balls:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_balls`
              ],
            fours:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_fours`
              ],
            comment:
              response.data[
                `innings_B_1_batting_player_${[
                  indexTABatIB1
                ]}_innings_1_out_comment`
              ],
            runs:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_runs`
              ],
            sixes:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_1_sixes`
              ],
            sr:
              response.data[
                `innings_B_1_batting_player_${[
                  indexTABatIB1
                ]}_innings_1_strike_rate`
              ]
          };
          const individualPlayerinn2B = {
            name:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_full_name`
              ],
            balls:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_balls`
              ],
            fours:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_fours`
              ],
            comment:
              response.data[
                `innings_B_1_batting_player_${[
                  indexTABatIB1
                ]}_innings_2_out_comment`
              ],
            runs:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_runs`
              ],
            sixes:
              response.data[
                `innings_B_1_batting_player_${[indexTABatIB1]}_innings_2_sixes`
              ],
            sr:
              response.data[
                `innings_B_1_batting_player_${[
                  indexTABatIB1
                ]}__innings_2_strike_rate`
              ]
          };

          //Batting list manipulation
       /*   if (
            individualPlayerinn1B.name === null ||
            individualPlayerinn2B.name === null
          ) {
            break;
          }*/

       if(individualPlayerinn1B.name !== null)
       {
           teamBBattingInnings1.push(individualPlayerinn1B);
       }
       if(individualPlayerinn2B.name !== null)
       {
           teamBBattingInnings2.push(individualPlayerinn2B);
       }
          indexTABatIB1++;
        }
        this.setState({
          teamBBallingInnings1,
          teamBBallingInnings2,
          teamBBattingInnings1,
          teamBBattingInnings2,
          loading: false
        });

        this.setState({
          matchObject: response.data
        });
      })
      .catch(error => {
        if (error.response.status === 401) {
          getuserdetails()
            .then(res => {
              console.log("ERROR ERROR", error);
              this.setState({}, () => {
                this.axiosGetRecentMatchDetails(
                  `http://206.189.159.149:8080/com-sweetitech-tigers-0.0.1-SNAPSHOT/api/match/showSummary?access_token=${
                    res.access_token
                  }&client_id=android-client&client_secret=android-secret&matchKey=iplt20_2018_final`
                );
              });
            })
            .catch(() => {
              this.setState({
                loading: false
              });
              Alert.alert(
                "you are being logged out for unavilability, Please log in again!"
              );
              this.props.nav.navigate("LoginPage");
            });
        } else {
          this.setState({
            loading: false
          });
          //alert(error.response.status);
          Alert.alert("A technical error occured!");
        }
      });
  };
  collapsibleLogic() {
    if (this.state.matchObject.format === "test") {
      return (
        <View>
          <Accordion
            initiallyActiveSection={0}
            sections={SECTIONS1}
            renderHeader={this.renderHeaderA}
            renderContent={this.renderX1stContent}
          />
          <Accordion
            sections={SECTIONS2}
            renderHeader={this.renderHeaderB}
            renderContent={this.renderY1stContent}
          />
          <Accordion
            sections={SECTIONS1}
            renderHeader={this.renderHeaderA2}
            renderContent={this.renderX1stContentTest2ndInn}
          />
          <Accordion
            sections={SECTIONS2}
            renderHeader={this.renderHeaderB2}
            renderContent={this.renderY1stContentTest2ndInn}
          />
        </View>
      );
    }
    return (
      <View>
        <Accordion
          initiallyActiveSection={0}
          sections={SECTIONS1}
          renderHeader={this.renderHeaderA}
          renderContent={this.renderX1stContent}
        />
        <Accordion
          sections={SECTIONS2}
          renderHeader={this.renderHeaderB}
          renderContent={this.renderY1stContent}
        />
      </View>
    );
  }
  renderHeaderA = (section, i, isActive) => (
    <View>
      <View style={{ width: "100%", height: 8, backgroundColor: "#DCDCDC" }} />
      <View style={styles.header}>
        <Text style={styles.headerTextnew}>
          {this.state.matchObject.team_a_full_name}
        </Text>
        {isActive ? (
          <LineIcon name="arrow-down" size={16} color="#23b06a" />
        ) : (
          <LineIcon name="arrow-right" size={16} color="#23b06a" />
        )}
      </View>
    </View>
  );
  renderHeaderA2 = (section, i, isActive) => (
    <View>
      <View style={{ width: "100%", height: 8, backgroundColor: "#DCDCDC" }} />
      <View style={styles.header}>
        <Text style={styles.headerTextnew}>
          {this.state.matchObject.team_a_full_name}
        </Text>
        {isActive ? (
          <LineIcon name="arrow-down" size={16} color="#23b06a" />
        ) : (
          <LineIcon name="arrow-right" size={16} color="#23b06a" />
        )}
      </View>
    </View>
  );
  renderHeaderB = (section, i, isActive) => (
    <View>
      <View style={{ width: "100%", height: 8, backgroundColor: "#DCDCDC" }} />
      <View style={styles.header}>
        <Text style={styles.headerTextnew}>
          {this.state.matchObject.team_b_full_name}
        </Text>
        {isActive ? (
          <LineIcon name="arrow-down" size={16} color="#23b06a" />
        ) : (
          <LineIcon name="arrow-right" size={16} color="#23b06a" />
        )}
      </View>
    </View>
  );
  renderHeaderB2 = (section, i, isActive) => (
    <View>
      <View style={{ width: "100%", height: 8, backgroundColor: "#DCDCDC" }} />
      <View style={styles.header}>
        <Text style={styles.headerTextnew}>
          {this.state.matchObject.team_b_full_name}
        </Text>
        {isActive ? (
          <LineIcon name="arrow-down" size={16} color="#23b06a" />
        ) : (
          <LineIcon name="arrow-right" size={16} color="#23b06a" />
        )}
      </View>
    </View>
  );

  renderX1stContent = () => (
    <ScrollView>
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>BATSMEN</Text>
            <Text style={styles.rowElementTextbold}>R</Text>
            <Text style={styles.rowElementTextbold}>B</Text>
            <Text style={styles.rowElementTextbold}>4S</Text>
            <Text style={styles.rowElementTextbold}>6S</Text>
            <Text style={styles.rowElementTextbold}>SR</Text>
          </View>

          {this.state.teamABattingInnings1.map(stat => (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  paddingTop: 6,
                  paddingBottom: 6,
                  borderTopWidth: 1,
                  borderColor: "#dcdcdc"
                }}
              >
                <Text style={styles.Row11}>{stat.name}</Text>
                <Text style={styles.rowElementText}>{stat.runs}</Text>
                <Text style={styles.rowElementText}>{stat.balls}</Text>
                <Text style={styles.rowElementText}>{stat.fours}</Text>
                <Text style={styles.rowElementText}>{stat.sixes}</Text>
                <Text style={styles.rowElementText}>{stat.sr}</Text>
              </View>
              {stat.comment ? (
                <View
                  style={{
                    flexDirection: "row",
                    width: "34%",
                    borderColor: "#dcdcdc",
                    paddingLeft: 13,
                    paddingBottom: 4
                  }}
                >
                  <Text>{stat.comment} </Text>
                </View>
              ) : (
                <View />
              )}
            </View>
          ))}
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>Extras</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementText}>
              {this.state.matchObject.innings_A_1_extras}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>TOTAL</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.totalText}>
              {this.state.matchObject.innings_A_1_run_str} (RR:{" "}
              {this.state.matchObject.innings_A_1_run_rate})
            </Text>
          </View>

          <View
            style={{ width: "100%", height: 1, backgroundColor: "#DCDCDC" }}
          />
        </View>
        <View style={styles.Highlights}>
          <View
            style={{
              paddingLeft: 12,
              paddingVertical: 10,
              paddingRight: 18
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 11 }}>
              Fall of wickets:
            </Text>
            <Text style={{ fontSize: 11 }}>
              {/*{this.state.matchObject.innings_A_1_fall_of_wickets}*/}
              {/*changed fall of wicket*/}

              {this.state.matchObject.innings_A_1_fall_of_wickets
                ? listStringArray(
                    this.state.matchObject.innings_A_1_fall_of_wickets
                  )
                : null}
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            paddingTop: 6,
            paddingBottom: 6,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#dcdcdc"
          }}
        >
          <Text style={styles.Row11bold}>BOWLERS</Text>
          <Text style={styles.rowElementBollingTextbold}>O</Text>
          <Text style={styles.rowElementBollingTextbold}>M</Text>
          <Text style={styles.rowElementBollingTextbold}>R</Text>
          <Text style={styles.rowElementBollingTextbold}>W</Text>
          <Text style={styles.rowElementBollingETextbold}>E</Text>
         {/* <Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
          <Text style={styles.rowElementBollingTextbold}>Ext</Text>
        </View>
        {this.state.teamBBallingInnings1.map(stat => (
          /*<View>
            {stat.balls !== null ? (*/
          <View>
            <View
              style={{
                flexDirection: "row",
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: 1,
                borderColor: "#dcdcdc"
              }}
            >
              <Text style={styles.Row11}>{stat.name}</Text>
              <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
              <Text style={styles.rowElementBollingText}>
                {stat.maiden_overs}
              </Text>
              <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
              <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
              {/*<Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
              <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
            </View>
            {stat.comment ? (
              <View
                style={{
                  flexDirection: "row",
                  width: "34%",
                  borderColor: "#dcdcdc",
                  paddingLeft: 13,
                  paddingBottom: 4
                }}
              >
                <Text>{stat.comment} </Text>
              </View>
            ) : (
              <View />
            )}
          </View>
          /*) : null}
          </View>*/
        ))}
      </View>
    </ScrollView>
  );
  renderX1stContentTest2ndInn = () => (
    <ScrollView>
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>BATSMEN</Text>
            <Text style={styles.rowElementTextbold}>R</Text>
            <Text style={styles.rowElementTextbold}>B</Text>
            <Text style={styles.rowElementTextbold}>4S</Text>
            <Text style={styles.rowElementTextbold}>6S</Text>
            <Text style={styles.rowElementTextbold}>SR</Text>
          </View>
          {this.state.teamABattingInnings2.map(stat => (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  paddingTop: 6,
                  paddingBottom: 6,
                  borderTopWidth: 1,
                  borderColor: "#dcdcdc"
                }}
              >
                <Text style={styles.Row11}>{stat.name}</Text>
                <Text style={styles.rowElementText}>{stat.runs}</Text>
                <Text style={styles.rowElementText}>{stat.balls}</Text>
                <Text style={styles.rowElementText}>{stat.fours}</Text>
                <Text style={styles.rowElementText}>{stat.sixes}</Text>
                <Text style={styles.rowElementText}>{stat.sr}</Text>
              </View>
              /*{" "}
              {stat.comment ? (
                <View
                  style={{
                    flexDirection: "row",
                    width: "34%",
                    borderColor: "#dcdcdc",
                    paddingLeft: 13,
                    paddingBottom: 4
                  }}
                >
                  <Text>{stat.comment} </Text>
                </View>
              ) : (
                <View />
              )}*/
              {stat.comment ? (
                <View
                  style={{
                    flexDirection: "row",
                    width: "34%",
                    borderColor: "#dcdcdc",
                    paddingLeft: 13,
                    paddingBottom: 4
                  }}
                >
                  <Text>{stat.comment} </Text>
                </View>
              ) : (
                <View />
              )}
            </View>
          ))}
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>Extras</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementText}>
              {this.state.matchObject.innings_A_2_extras}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>TOTAL</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.totalText}>
              {this.state.matchObject.innings_A_2_run_str}(RR:{" "}
              {this.state.matchObject.innings_A_2_run_rate})
            </Text>
          </View>

          <View
            style={{ width: "100%", height: 1, backgroundColor: "#DCDCDC" }}
          />
        </View>
        <View style={styles.Highlights}>
          <View
            style={{
              paddingLeft: 12,
              paddingVertical: 10,
              paddingRight: 18
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 11 }}>
              Fall of wickets:
            </Text>
            <Text style={{ fontSize: 11 }}>
              {/*{this.state.matchObject.innings_A_2_fall_of_wickets}*/}
              {/*changed fall of wicket*/}

              {this.state.matchObject.innings_A_2_fall_of_wickets
                ? listStringArray(
                    this.state.matchObject.innings_A_2_fall_of_wickets
                  )
                : null}
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            paddingTop: 6,
            paddingBottom: 6,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#dcdcdc"
          }}
        >
          <Text style={styles.Row11bold}>BOWLERS</Text>
          <Text style={styles.rowElementBollingTextbold}>O</Text>
          <Text style={styles.rowElementBollingTextbold}>M</Text>
          <Text style={styles.rowElementBollingTextbold}>R</Text>
          <Text style={styles.rowElementBollingTextbold}>W</Text>
          <Text style={styles.rowElementBollingETextbold}>E</Text>
         {/* <Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
          <Text style={styles.rowElementBollingTextbold}>Ext</Text>
        </View>
        {this.state.teamBBallingInnings2.map(stat => (
          /*<View>
            {stat.balls !== null ? (*/
          <View>
            <View
              style={{
                flexDirection: "row",
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: 1,
                borderColor: "#dcdcdc"
              }}
            >
              <Text style={styles.Row11}>{stat.name}</Text>
              <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.maiden_overs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
              <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
              {/*<Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
              <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
            </View>
            {stat.comment ? (
              <View
                style={{
                  flexDirection: "row",
                  width: "34%",
                  borderColor: "#dcdcdc",
                  paddingLeft: 13,
                  paddingBottom: 4
                }}
              >
                <Text>{stat.comment} </Text>
              </View>
            ) : (
              <View />
            )}
            {/*  </View>
            ) : null}*/}
          </View>
        ))}
      </View>
    </ScrollView>
  );
  renderY1stContent = () => (
    <ScrollView>
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>BATSMEN</Text>
            <Text style={styles.rowElementTextbold}>R</Text>
            <Text style={styles.rowElementTextbold}>B</Text>
            <Text style={styles.rowElementTextbold}>4S</Text>
            <Text style={styles.rowElementTextbold}>6S</Text>
            <Text style={styles.rowElementTextbold}>SR</Text>
          </View>
          {this.state.teamBBattingInnings1.map(stat => (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  paddingTop: 6,
                  paddingBottom: 6,
                  borderTopWidth: 1,
                  borderColor: "#dcdcdc"
                }}
              >
                <Text style={styles.Row11}>{stat.name}</Text>
                <Text style={styles.rowElementText}>{stat.runs}</Text>
                <Text style={styles.rowElementText}>{stat.balls}</Text>
                <Text style={styles.rowElementText}>{stat.fours}</Text>
                <Text style={styles.rowElementText}>{stat.sixes}</Text>
                <Text style={styles.rowElementText}>{stat.sr}</Text>
              </View>
              {stat.comment ? (
                <View
                  style={{
                    flexDirection: "row",
                    width: "34%",
                    borderColor: "#dcdcdc",
                    paddingLeft: 13,
                    paddingBottom: 4
                  }}
                >
                  <Text>{stat.comment} </Text>
                </View>
              ) : (
                <View />
              )}
            </View>
          ))}
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>Extras</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementText}>
              {this.state.matchObject.innings_B_1_extras}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>TOTAL</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.totalText}>
              {this.state.matchObject.innings_B_1_run_str}(RR:{" "}
              {this.state.matchObject.innings_B_1_run_rate})
            </Text>
          </View>

          <View
            style={{ width: "100%", height: 1, backgroundColor: "#DCDCDC" }}
          />
        </View>
        <View style={styles.Highlights}>
          <View
            style={{
              paddingLeft: 12,
              paddingVertical: 10,
              paddingRight: 18
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 11 }}>
              Fall of wickets:
            </Text>

            <Text style={{ fontSize: 11 }}>
              {this.state.matchObject.innings_B_1_fall_of_wickets
                ? listStringArray(
                    this.state.matchObject.innings_B_1_fall_of_wickets
                  )
                : null}
              {console.log(
                "List Object",
                this.state.matchObject.innings_B_1_fall_of_wickets
              )}
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            paddingTop: 6,
            paddingBottom: 6,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#dcdcdc"
          }}
        >
          <Text style={styles.Row11bold}>BOWLERS</Text>
          <Text style={styles.rowElementBollingTextbold}>O</Text>
          <Text style={styles.rowElementBollingTextbold}>M</Text>
          <Text style={styles.rowElementBollingTextbold}>R</Text>
          <Text style={styles.rowElementBollingTextbold}>W</Text>
          <Text style={styles.rowElementBollingETextbold}>E</Text>
         {/* <Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
          <Text style={styles.rowElementBollingTextbold}>Ext</Text>
        </View>
        {this.state.teamABallingInnings1.map(stat => (
          /*<View>
            {stat.name !== "" ? (*/
          <View>
            <View
              style={{
                flexDirection: "row",
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: 1,
                borderColor: "#dcdcdc"
              }}
            >
              <Text style={styles.Row11}>{stat.name}</Text>
              <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
              <Text style={styles.rowElementBollingText}>
                {stat.maiden_overs}
              </Text>
              <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
              <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
              {/*<Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
              <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
            </View>
            {stat.comment ? (
              <View
                style={{
                  flexDirection: "row",
                  width: "34%",
                  borderColor: "#dcdcdc",
                  paddingLeft: 13,
                  paddingBottom: 4
                }}
              >
                <Text>{stat.comment} </Text>
              </View>
            ) : (
              <View />
            )}
            {/*</View>
            ) : null}*/}
          </View>
        ))}
      </View>
    </ScrollView>
  );
  renderY1stContentTest2ndInn = () => (
    <ScrollView>
      <View style={{ flex: 1, width: "100%" }}>
        <View style={{ width: "100%" }}>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>BATSMEN</Text>
            <Text style={styles.rowElementTextbold}>R</Text>
            <Text style={styles.rowElementTextbold}>B</Text>
            <Text style={styles.rowElementTextbold}>4S</Text>
            <Text style={styles.rowElementTextbold}>6S</Text>
            <Text style={styles.rowElementTextbold}>SR</Text>
          </View>
          {this.state.teamBBattingInnings2.map(stat => (
            <View>
              <View
                style={{
                  flexDirection: "row",
                  paddingTop: 6,
                  paddingBottom: 6,
                  borderTopWidth: 1,
                  borderColor: "#dcdcdc"
                }}
              >
                <Text style={styles.Row11}>{stat.name}</Text>
                {/* <<<<<<< HEAD */}
                {/* <Text style={styles.rowElementBollingText}>{stat.balls}</Text>
                <Text style={styles.rowElementBollingText}>{stat.maiden_overs}</Text>
                <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
                <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
                <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
                <Text style={styles.rowElementBollingText}>{stat.dots}</Text>
                <Text style={styles.rowElementBollingText}>{stat.extras}</Text> */}
                {/* ======= */}
                <Text style={styles.rowElementText}>{stat.runs}</Text>
                <Text style={styles.rowElementText}>{stat.balls}</Text>
                <Text style={styles.rowElementText}>{stat.fours}</Text>
                <Text style={styles.rowElementText}>{stat.sixes}</Text>
                <Text style={styles.rowElementText}>{stat.sr}</Text>
                {/* >>>>>>> 348ffa05747e0dd37e51b0e5f37ef3c86384c15f */}
              </View>
              {stat.comment ? (
                <View
                  style={{
                    flexDirection: "row",
                    width: "34%",
                    borderColor: "#dcdcdc",
                    paddingLeft: 13,
                    paddingBottom: 4
                  }}
                >
                  <Text>{stat.comment} </Text>
                </View>
              ) : (
                <View />
              )}
            </View>
          ))}
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>Extras</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementText}>
              {this.state.matchObject.innings_A_1_extras}
            </Text>
          </View>
          <View
            style={{
              flexDirection: "row",
              paddingTop: 6,
              paddingBottom: 6,
              borderTopWidth: 1,
              borderBottomWidth: 1,
              borderColor: "#dcdcdc"
            }}
          >
            <Text style={styles.Row11bold}>TOTAL</Text>
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.rowElementTextbold} />
            <Text style={styles.totalText}>
              {this.state.matchObject.innings_B_2_run_str}(RR:{" "}
              {this.state.matchObject.innings_B_2_run_rate})
            </Text>
          </View>

          <View
            style={{ width: "100%", height: 1, backgroundColor: "#DCDCDC" }}
          />
        </View>
        <View style={styles.Highlights}>
          <View
            style={{
              paddingLeft: 12,
              paddingVertical: 10,
              paddingRight: 18
            }}
          >
            <Text style={{ fontWeight: "bold", fontSize: 11 }}>
              Fall of wickets:
            </Text>
            <Text style={{ fontSize: 11 }}>
              {/*{this.state.matchObject.innings_B_2_fall_of_wickets}*/}
              {/*changed fall of wicket*/}

              {this.state.matchObject.innings_B_2_fall_of_wickets
                ? listStringArray(
                    this.state.matchObject.innings_B_2_fall_of_wickets
                  )
                : null}
            </Text>
          </View>
        </View>

        <View
          style={{
            flexDirection: "row",
            paddingTop: 6,
            paddingBottom: 6,
            borderTopWidth: 1,
            borderBottomWidth: 1,
            borderColor: "#dcdcdc"
          }}
        >
          <Text style={styles.Row11bold}>BOWLERS</Text>
          <Text style={styles.rowElementBollingTextbold}>O</Text>
          <Text style={styles.rowElementBollingTextbold}>M</Text>
          <Text style={styles.rowElementBollingTextbold}>R</Text>
          <Text style={styles.rowElementBollingTextbold}>W</Text>
          <Text style={styles.rowElementBollingETextbold}>E</Text>
         {/* <Text style={styles.rowElementBollingTextbold}>0s</Text>*/}
          <Text style={styles.rowElementBollingTextbold}>Ext</Text>
        </View>
        {this.state.teamABallingInnings2.map(stat => (
          /*<View>
            {stat.balls !== null ? (*/
          <View>
            <View
              style={{
                flexDirection: "row",
                paddingTop: 6,
                paddingBottom: 6,
                borderTopWidth: 1,
                borderColor: "#dcdcdc"
              }}
            >
              <Text style={styles.Row11}>{stat.name}</Text>
              <Text style={styles.rowElementBollingText}>{stat.overs}</Text>
              <Text style={styles.rowElementBollingText}>
                {stat.maiden_overs}
              </Text>
              <Text style={styles.rowElementBollingText}>{stat.runs}</Text>
              <Text style={styles.rowElementBollingText}>{stat.wickets}</Text>
              <Text style={styles.rowElementBollingEText}>{stat.economy}</Text>
             {/* <Text style={styles.rowElementBollingText}>{stat.dots}</Text>*/}
              <Text style={styles.rowElementBollingText}>{stat.extras}</Text>
            </View>
            {stat.comment ? (
              <View
                style={{
                  flexDirection: "row",
                  width: "34%",
                  borderColor: "#dcdcdc",
                  paddingLeft: 13,
                  paddingBottom: 4
                }}
              >
                <Text>{stat.comment} </Text>
              </View>
            ) : (
              <View />
            )}
            {/*</View>
            ) : null}*/}
          </View>
        ))}
      </View>
    </ScrollView>
  );
  render() {
    //console.log(this.state.matchObject);
    if (this.state.loading) {
      return (
        <View
          style={{
            flex: 1,
            backgroundColor: "white",
            alignItems: "center",
            justifyContent: "center"
          }}
        >
          <ActivityIndicator size="large" color="#rgb(54,190,107)" />
        </View>
      );
    }
    return (
      <ScrollView>
        <View>
          <View style={styles.scoredisplaycontainer}>
            <View style={{ marginTop: 5, marginBottom: 5, paddingLeft: 18 }}>
              <Text style={{ fontSize: 12, color: "#9b9b9b" }}>
                {this.state.matchObject.related_name},{" "}
                {this.state.matchObject.short_name},{" "}
                {this.state.matchObject.venue},{" "}
                {this.state.matchObject.start_date_str}
              </Text>
            </View>
          </View>
          <View
            style={{
              width: "100%",
              height: 55,
              flexDirection: "column",
              paddingBottom: 18
            }}
          >
            <View
              style={{
                width: "100%",
                height: 25,
                flexDirection: "row",
                justifyContent: "space-between",
                paddingHorizontal: 12
              }}
            >
              <View
                style={{
                  width: "50%",
                  flexDirection: "row"
                }}
              >
                <Image
                  style={styles.flagimagesmall}
                  source={{ uri: this.state.matchObject.team_a_url }}
                />
                <Text
                  style={{
                    fontFamily: "BigNoodleTitling",
                    marginBottom: 2,
                    fontSize: 20,
                    color: "#2b2b2b"
                  }}
                >
                  {this.state.matchObject.team_a}
                </Text>
              </View>
              {this.state.matchObject.format !== "test" ? (
                <Text>
                  {this.state.matchObject.innings_A_1_runs}/{
                    this.state.matchObject.innings_A_1_wickets
                  }
                </Text>
              ) : (
                <Text>
                  {this.state.matchObject.innings_A_1_runs}/{
                    this.state.matchObject.innings_A_1_wickets
                  }{" "}
                  {"&"} {this.state.matchObject.innings_A_2_runs}/{
                    this.state.matchObject.innings_A_2_wickets
                  }
                </Text>
              )}
            </View>
            <View
              style={{
                width: "100%",
                height: 25,
                flexDirection: "row",
                justifyContent: "space-between",
                paddingHorizontal: 12
              }}
            >
              <View
                style={{
                  width: "50%",
                  flexDirection: "row",
                  alignItems: "center"
                }}
              >
                <Image
                  style={styles.flagimagesmall}
                  source={{ uri: this.state.matchObject.team_b_url }}
                />
                <Text
                  style={{
                    fontFamily: "BigNoodleTitling",
                    marginBottom: 2,
                    fontSize: 20,
                    color: "#2b2b2b"
                  }}
                >
                  {this.state.matchObject.team_b}
                </Text>
              </View>
              {this.state.matchObject.format !== "test" ? (
                <Text>
                  {this.state.matchObject.innings_B_1_runs}/{
                    this.state.matchObject.innings_B_1_wickets
                  }
                </Text>
              ) : (
                <Text>
                  {this.state.matchObject.innings_B_1_runs}/{
                    this.state.matchObject.innings_B_1_wickets
                  }{" "}
                  {"&"} {this.state.matchObject.innings_B_2_runs}/{
                    this.state.matchObject.innings_B_2_wickets
                  }
                </Text>
              )}
            </View>
          </View>
          <View style={{ flex: 1 }}>
            <View style={{ flex: 1 }}>
              {this.collapsibleLogic()}

                <View style={{ width: "100%", height: 8, backgroundColor: "#DCDCDC" }} />

              <View
                style={{
                  width: "100%",
                  backgroundColor: "white",
                  // flex: 1,
                  paddingBottom: "3%",
                  paddingTop: "3%",
                  // paddingLeft: 15,
                  // paddingRight: 15,
                  flexDirection: "row",
                  justifyContent: "center"
                }}
              >
                <Image
                  style={{ width: "80%", height: 60, resizeMode: "cover" }}
                  source={{
                    uri: `http://206.189.159.149:8080/abc/${
                      this.state.topBanner
                    }`
                  }}
                />
              </View>
              <View
                style={{
                  width: "100%",
                  paddingTop: 10,
                  paddingBottom: 24,
                  backgroundColor: "#f7f7f7"
                }}
              >
                <Text
                  style={{
                    paddingLeft: 18,
                    fontFamily: "Roboto",
                    fontWeight: "500",
                    color: "#4a4a4a",
                    fontSize: 16,
                    paddingTop: 16,
                    paddingBottom: 8
                  }}
                >
                  Match Info
                </Text>
                <View style={styles.matchinfoview}>
                  <Text style={styles.matchinfoLabel}>Series</Text>
                  {this.state.matchObject.short_name === null ? (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                    </View>
                  ) : (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>
                        {this.state.matchObject.short_name}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={styles.matchinfoview}>
                  <Text style={styles.matchinfoLabel}>Match Detail</Text>
                  {this.state.matchObject.related_name === null ? (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                    </View>
                  ) : (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>
                        {this.state.matchObject.related_name}
                      </Text>
                    </View>
                  )}
                </View>
                <View style={styles.matchinfoview}>
                  <Text style={styles.matchinfoLabel}>Toss</Text>
                  {this.state.matchObject.toss_str === null ? (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                    </View>
                  ) : (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>
                        {this.state.matchObject.toss_str}
                      </Text>
                    </View>
                  )}
                </View>


                {/*<View style={styles.matchinfoview}>
                  <Text style={styles.matchinfoLabel}>Man Of The Match</Text>
                  {this.state.matchObject.man_of_match === null ? (
                    <View style={styles.matchlblinfocontainer}>
                      <Image style={styles.flagimagesmall} />
                      <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                    </View>
                  ) : (
                    <View style={styles.matchlblinfocontainer}>
                      <Image style={styles.flagimagesmall} />
                      <Text style={styles.matchinfoLabelvalue}>
                        {this.state.matchObject.man_of_match}
                      </Text>
                    </View>
                  )}
                </View>*/}


                <View style={styles.matchinfoview}>
                  <Text style={styles.matchinfoLabel}>Series Result</Text>
                  {this.state.matchObject.msgs_result === null ? (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>TBA</Text>
                    </View>
                  ) : (
                    <View style={styles.matchlblinfocontainer}>
                      <Text style={styles.matchinfoLabelvalue}>
                        {this.state.matchObject.msgs_result}
                      </Text>
                    </View>
                  )}
                </View>
              </View>
            </View>
          </View>
        </View>
      </ScrollView>
    );
  }
}

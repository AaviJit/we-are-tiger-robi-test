import React, { Component } from "react";
import PropTypes from "prop-types";
import { StyleSheet, Text, View, TouchableOpacity } from "react-native";
// import Icon from "react-native-vector-icons/Ionicons";
import LineIcon from "react-native-vector-icons/SimpleLineIcons";
import { Header, Right } from "native-base";

const styles = StyleSheet.create({
  headertext: {
    fontSize: 17,
    fontWeight: "800",
    fontFamily: "Montserrat-Medium",
    color: "#ffffff",
    justifyContent: "center",
    textAlign: "center"
    // backgroundColor:'grey'
  },
  headerwearetigers: {
    // flexDirection: "row",
    // alignItems: "center",
    // justifyContent: "center",
    // height: "100%",
    // width: "70%",
    // marginLeft: 1,
    // flex: 1
    // backgroundColor:'maroon'
    width: "60%",
    justifyContent: "center",
    alignItems: "center"
  },

  headerRightPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    flex: 1,
    padding: 3,
    marginTop: 3
  },
  headerLeftPanel: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    padding: 10,
    flex: 1
    //  backgroundColor:'red',
  }
});

export default class HeaderGeneralComponent extends Component {
  static propTypes = {
    nav: PropTypes.object.isRequired,
    back: PropTypes.string,
    leftIcon: PropTypes.string.isRequired,
    HeaderTittle: PropTypes.string.isRequired,
    RightPanelicon1: PropTypes.string.isRequired
  };
  static defaultProps = {
    back: "HomePage"
  };

  toBack = () => {
    // alert(JSON.stringify(this.props));
    this.props.nav.navigate(this.props.back);
  };

  render() {
    return (
      <Header style={{ backgroundColor: "#36be6b", height: 60.6 }}>
        <View style={styles.headerLeftPanel}>
          <TouchableOpacity
            onPress={() => {
              this.toBack();
            }}
          >
            <LineIcon name={this.props.leftIcon} size={20} color="#ffffff" />
          </TouchableOpacity>
        </View>
        <View style={styles.headerwearetigers}>
          <Text style={styles.headertext}>{this.props.HeaderTittle}</Text>
        </View>
        <View style={styles.headerRightPanel}>
          <Right>
            <TouchableOpacity>
              <LineIcon
                name={this.props.RightPanelicon1}
                size={20}
                color="#ffffff"
              />
            </TouchableOpacity>
            <TouchableOpacity>
              <LineIcon name={this.RightPanelicon2} size={20} color="#ffffff" />
            </TouchableOpacity>
          </Right>
        </View>
      </Header>
    );
  }
}

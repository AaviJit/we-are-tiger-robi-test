import React, { Component } from "react";
import {
  View,
  StyleSheet,
  ScrollView,
  Text,
  TouchableOpacity
} from "react-native";

const styles = StyleSheet.create({
  Text1: {
    fontSize: 14,
    alignItems: "center",
    textAlign: "center",
    marginTop: 10,
    marginBottom: 10,
    fontFamily: "Roboto",
    fontWeight: "normal",
    fontStyle: "normal",
    letterSpacing: 0,
    color: "#d9d9d9"
  }
});

export default class Catagories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      color: "#d9d9d9",
      fontWeight: "normal"
    };
  }
  makeGray = () => {
    this.setState({
      color: "#4a4a4a",
      fontWeight: "bold"
    });
  };
  makeGray2 = () => {
    this.setState({
      color: "green",
      fontWeight: "bold"
    });
  };
  render() {
    return (
      <ScrollView>
        <View style={{ backgroundColor: "white", flex: 1 }}>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Blazers
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray2}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Boxer shorts
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Chinos
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Gilets
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Jackets and coats
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Jeans
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Neated vests
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Knitwear
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Long sleeve polos
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Polos
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
          <View>
            <TouchableOpacity activeOpacity={1} onPress={this.makeGray}>
              <Text
                style={[
                  styles.Text1,
                  {
                    color: this.state.color,
                    fontWeight: this.state.fontWeight
                  }
                ]}
              >
                Short Sleeve sweats
              </Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              height: 2,
              backgroundColor: "#eeeeee"
            }}
          />
        </View>
      </ScrollView>
    );
  }
}
